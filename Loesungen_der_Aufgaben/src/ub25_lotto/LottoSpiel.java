package ub25_lotto;

import java.util.Arrays;
import java.util.Random;

public class LottoSpiel {
	private int[] ziehung;
	private int anzahlKugelGesamt;
	
	public LottoSpiel(int anzahlKugel, int anzahlKugelGesamt) {
		ziehung = new int[anzahlKugel];
		this.anzahlKugelGesamt = anzahlKugelGesamt;
		}

	public String toString() { 
		int[] arr = ziehung;
		Arrays.sort( arr );  								// Methode "sort" der Klasse "java.util.Arrays" benutzen.
		return "Spiel 7 aus 49. " + Arrays.toString( arr ); // Spiel 7 aus 49. [3, 7, 11, 28, 35, 40, 48]
	}
	
	public void ziehen() {
		Random rand = new Random();
		int guess;
		for (int i = 0; i < ziehung.length; i++) {				// Die Anzahl der generierten Zahlen ist gleich anzahlKugel. 
			do {
				guess = rand.nextInt(anzahlKugelGesamt) + 1; 	// Dabei werden zufällige einzigartige Zahlen im Bereich 1 bis anzahlKugelGesamt generiert. 
			} while ( !isValid(guess) );
			ziehung[i] = guess;
		}
	}	
	
	private boolean isValid(int number) {
		if (number < 1 || number > 49) {
			return false;
		}
		for (int i = 0; i < ziehung.length ; i++) { 
			if (ziehung[i] == number) {
				return false;
			}
		}
		return true;		
	}
	
	int vergleichen(LottoTipp tipp) {
		int[] tip = tipp.getTipp();
		Arrays.sort( tip );
		int matches = 0;
		StringBuilder sb = new StringBuilder("");
		for (int i = 0; i < ziehung.length; i++) {
			if( Arrays.binarySearch(tip, ziehung[i]) >=0 ) {
				matches++;
				sb.append( ziehung[i] ).append(" ");
			}
		}
		int gewinn = 0;
		switch (matches) {
		case 0:
			gewinn = 0;
			System.out.println("      Niete.");
			break;
		case 1:
			gewinn = 1;
			System.out.println("      1 Treffer: " + sb.toString() );
			break;
		case 2:
			gewinn = 10;
			System.out.println("      2 Treffer: " + sb.toString() );
			break;
		case 3:
			gewinn = 100;
			System.out.println("      3 Treffer: " + sb.toString() );
			break;
		case 4:
			gewinn = 1000;
			System.out.println("      4 Treffer: " + sb.toString() );
			break;
		case 5:
			gewinn = 10_000;
			System.out.println("    * 5 Richtige! *  (" + sb.toString() + ")" );
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 6:
			gewinn = 100_000;
			System.out.println("   ** 6 Richtige! ** (" + sb.toString() + ")" );
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 7:
			gewinn = 1_000_000;
			System.out.println("  *** 7 Richtige! ***" );
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		default:
			break;
		}
		return gewinn;
	} 

//	public static void main(String[] args) {
//		/*
//		 * UnitTests
//		 */
//		
//		LottoSpiel lotto = new LottoSpiel(0, 2);
//		System.out.println( "Spiel 7 aus 49. []".equals( lotto.toString() ) ? "UnitTest toString: OK." : "UnitTest toString: Test has failed!");
//
//		lotto = new LottoSpiel(2, 4);
//		System.out.println( "Spiel 7 aus 49. [0, 0]".equals( lotto.toString() ) ? "UnitTest toString: OK." : "UnitTest toString: Test has failed!");
//
//		lotto.ziehen(); 
//		System.out.println( lotto );
//	} // end of main
}
