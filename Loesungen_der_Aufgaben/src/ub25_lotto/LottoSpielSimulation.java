package ub25_lotto;

public class LottoSpielSimulation {
	
	final static int ANZAHL_KUGEL = 7;
	final static int ANZAHL_KUGEL_GESAMT = 49;

	public static void main(String[] args) {

	    LottoSpiel lotto = new LottoSpiel(ANZAHL_KUGEL, ANZAHL_KUGEL_GESAMT);
	    
	    lotto.ziehen();	// Dabei werden zufällige einzigartige Zahlen im Bereich 1 bis anzahlKugelGesamt generiert. Die Anzahl der generierten Zahlen ist gleich anzahlKugel. 
	    System.out.println( lotto ); // Spiel 7 aus 49. [3, 7, 11, 28, 35, 40, 48] // Methode "sort" der Klasse "java.util.Arrays" benutzen.
	    
	    LottoTipp tipp = new LottoTipp(ANZAHL_KUGEL, ANZAHL_KUGEL_GESAMT);
	    tipp.abgeben(); // Dabei dürfen zufällige einzigartige Zahlen im Bereich 1 bis anzahlKugelGesamt generiert werden. Die Anzahl der generierten Zahlen ist gleich anzahlKugel.
	    System.out.println( tipp ); //  Tipp 7 aus 49. [6, 9, 17, 23, 35, 41, 42]
	    
	    // Optional: den Tipp von der Konsole einlesen.
	    
	    int gewinn = lotto.vergleichen(tipp);	    
	    System.out.println("Ihr Gewinn beträgt: " + gewinn +" Euro");    // Geben Sie den Gewinn aus.
	    
	    
	    // Optional: Spielen Sie mit demselben Tipp 100000 unterschiedlichen Lotto-Spiele. Ein Lotto-Spiel kostet 1 Euro. Ermitteln Sie den Gesamtgewinn.
	    playMultiple(100_000);

	
	}
	
	static void playMultiple(int repeats) {
		
	    LottoSpiel lotto = new LottoSpiel(ANZAHL_KUGEL, ANZAHL_KUGEL_GESAMT);

	    LottoTipp tipp = new LottoTipp(ANZAHL_KUGEL, ANZAHL_KUGEL_GESAMT); 	// Spielen Sie mit demselben Tipp 
	    tipp.abgeben(); 
	    System.out.println( tipp ); 
	    System.out.println();
	    
	    int gesamtGewinn = 0; 
	    int gesamtKosten = 0;

	    for (int i = 0; i < repeats; i++) {									// 100000 unterschiedlichen Lotto-Spiele
	    	
	    	gesamtKosten++; 												//Ein Lotto-Spiel kostet 1 Euro.
	    	
		    lotto.ziehen();	 												// 100000 unterschiedlichen Lotto-Spiele
		    System.out.print(i + 1 +". ");
		    System.out.println( lotto ); 

		    int gewinn = lotto.vergleichen(tipp);	    
		    System.out.printf("Ihr Gewinn beträgt:  %10d Euro %n", gewinn); 
		    System.out.println("-----------------------------------");
		    
		    gesamtGewinn += gewinn;
		}
	    System.out.println();
	    
	    System.out.println( tipp ); 

	    int saldo = gesamtGewinn - gesamtKosten;							// Ermitteln Sie den Gesamtgewinn.
	    System.out.println(); 					
	    System.out.printf("Summe Gewinne: %20d Euro %n", gesamtGewinn);
	    System.out.printf("Summe Kosten:  %20d Euro %n", -gesamtKosten);
	    System.out.printf("------------------------------------------------ %n", -gesamtKosten);
	    System.out.printf("      Saldo:   %20d Euro %n", saldo);
	    System.out.println();
	    double durchschnitt = (double) saldo / (double) repeats;
	    String s = saldo>=0 ? 
	    		String.format("Ihr durchschnittlicher Gewinn pro Spiel ist %.2f Euro.", durchschnitt) :
	    			String.format("Ihr durchschnittlicher Verlust pro Spiel ist %.2f Euro.", durchschnitt) ;
	    System.out.println(s);
	}

}
