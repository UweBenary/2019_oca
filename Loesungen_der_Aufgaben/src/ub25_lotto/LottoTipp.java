package ub25_lotto;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class LottoTipp  { //extends LottoSpiel

	private int[] tipp;
	private int anzahlKugelGesamt;

	public LottoTipp(int anzahlKugel, int anzahlKugelGesamt) {
		tipp = new int[anzahlKugel];
		this.anzahlKugelGesamt = anzahlKugelGesamt;
	}
	
	public int[] getTipp() {
		return tipp;
	}
	
	public String toString() { 
		int[] arr = tipp;
		Arrays.sort( arr );  								// Methode "sort" der Klasse "java.util.Arrays" benutzen.
		return "Ihr Tipp im Spiel 7 aus 49. " + Arrays.toString( arr ); // Spiel 7 aus 49. [3, 7, 11, 28, 35, 40, 48]
	}
	
	private void ziehen() {
		Random rand = new Random();
		int guess;
		for (int i = 0; i < tipp.length; i++) {				// Die Anzahl der generierten Zahlen ist gleich anzahlKugel. 
			do {
				guess = rand.nextInt(anzahlKugelGesamt) + 1; 	// Dabei werden zufällige einzigartige Zahlen im Bereich 1 bis anzahlKugelGesamt generiert. 
			} while ( !isValid(guess, false) );
			tipp[i] = guess;
		}
	}	
	
	
	public void abgeben() {	    // Optional: den Tipp von der Konsole einlesen.
		System.out.println("Möchten Sie einen eigenen Tipp eingeben (Y) oder soll ein zufälliger Tipp erzeugt werden (N)?");
		Scanner scanner = new Scanner( System.in );
		String response = scanner.next();
		if (response.equals("y") || response.equals("Y")) {
			this.getUserInput();
		} else {
			ziehen();
		}
//		scanner.close();
		System.out.println();
	}
	
	private void getUserInput() {
		int input;
		for (int i = 0; i < tipp.length; i++) {
			do {
				System.out.printf("Geben Sie ihre %s. Zahl ein: ", i + 1);
				input = new Scanner( System.in ).nextInt();
			} while ( !isValid(input, true) );
			
			tipp[i] = input;
			System.out.println();
		}
	}
	
	private boolean isValid(int number, boolean needUserOutput) {
		if (number < 1 || number > 49) {
			if (needUserOutput) {
				System.out.println("    Die Zahl muss größer 0 und kleiner 50 sein!");
			}
			return false;
		}
		for (int i = 0; i < tipp.length ; i++) { 
			if (tipp[i] == number) {
				if (needUserOutput) {
					System.out.println("    Die Zahl wurde schon gewählt! Bitte eine andere Zahl wählen:");
				}
				return false;
			}
		}
		return true;		
	}
	
//	public static void main(String[] args) {
//		
//		/*
//		 * UnitTests
//		 */
//		LottoTipp tipp = new LottoTipp(0, 2);
//		System.out.println( "Spiel 7 aus 49. []".equals( tipp.toString() ) ? "UnitTest toString: OK." : "UnitTest toString: Test has failed!");
//		
//		tipp = new LottoTipp(3, 4);
//		tipp.ziehen(); 
//		System.out.println( tipp );
//		
//		
//		tipp = new LottoTipp(3, 4);
//		tipp.abgeben(); 
//		System.out.println( tipp );
//	}
	
}
