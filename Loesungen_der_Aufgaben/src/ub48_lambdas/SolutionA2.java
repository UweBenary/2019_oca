package ub48_lambdas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SolutionA2 {

	public static void main(String[] args) {

		/*
		 * A2.
		 * Bilden Sie eine ArrayList mit 4-5 Strings und sortieren Sie sie mit `Collections.sort(List, Comparator)` nach Länge. 
		 * Verwenden Sie zum erzeugen vom Comparator eine Lambda-Funtion. 		
		 */
		ArrayList<String> list = new ArrayList<String>();
		list.add("Ene ");
		list.add("mene ");
		list.add("muh ");
		list.add("und ");
		list.add("raus bist du!");
		System.out.println(list);
		
		Comparator<String> c = (s1, s2) -> s1.length() - s2.length();
		Collections.sort(list, c);

		System.out.println(list);
	}
}
