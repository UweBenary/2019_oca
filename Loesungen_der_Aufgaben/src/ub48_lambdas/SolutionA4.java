package ub48_lambdas;

import java.util.ArrayList;
import java.util.function.Consumer;

public class SolutionA4 {

	static class CPU {
		int anzahlKerne;
		String hersteller;
		
		CPU(String hersteller) {
			this.hersteller = hersteller;
		}
	}
	
	public static void main(String[] args) {
		
		ArrayList<CPU> list = new ArrayList<CPU>();
		list.add( new CPU("Hersteller 1") );
		list.add( new CPU("Hersteller 2") );
		list.add( new CPU("Hersteller 1") );
		list.add( new CPU("Hersteller 3") );
		
		Consumer<CPU> c = x -> System.out.println(x.hersteller);
		list.forEach(c);
	}
}
