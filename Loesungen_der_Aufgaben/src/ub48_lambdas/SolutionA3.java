package ub48_lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SolutionA3 {
	
	public interface ListPair {
		List<Integer> accept(List<Integer> list1, List<Integer> list2);   
	}

	public static void main(String[] args) {

		List<Integer> list1 = new ArrayList<>();
		list1.add(1);
		list1.add(2);
		list1.add(3);		
		
		List<Integer> list2 = new ArrayList<>();
		list2.add(21);
		list2.add(22);
		list2.add(23);
		
		ListPair combiner = (l1, l2) -> {ArrayList<Integer> l3 = new ArrayList<Integer>(l1); l3.addAll(l2); return l3;}; // hier die Lambda-Funktion
		List<Integer> list3 = combiner.accept(list1, list2);
		//list3 hat alle Elemente aus list1 und list2
		System.out.println(list3);
		
		/*
		 * Alternative
		 */
		ListPair combiner2 = (l1, l2) -> Stream.of(l1, l2)
				.flatMap(x -> x.stream())
				.collect(Collectors.toList()); // hier die Lambda-Funktion
		List<Integer> list4 = combiner2.accept(list1, list2);
		System.out.println(list4);
	}
}
