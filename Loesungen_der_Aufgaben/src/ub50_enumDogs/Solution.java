package ub50_enumDogs;


public class Solution {

	enum Hunderasse {		
		DACKEL, COLLIE, DOGGE;
		
		double maxGroesse() {
			switch ( this ) {
			case DACKEL:
				return 0.5;
			case COLLIE:
				return 1.0;
			case DOGGE:
				return 1.5;
			default:
				System.out.println("unsupported");
				return -1;
			}
		}
		
	}
	
	
	public static void main(String[] args) {

		for (Hunderasse hund : Hunderasse.values()) {
			String s = hund.name().charAt(0) + hund.name().substring(1).toLowerCase();
			System.out.println( s + ", max. Größe: " + hund.maxGroesse() );
		}

	}

}
