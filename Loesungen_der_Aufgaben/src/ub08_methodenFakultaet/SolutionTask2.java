package ub08_methodenFakultaet;

public class SolutionTask2 {
	
	/*
	 * Task
	 * Definieren Sie eine statische Methode "getFakultaet", 
	 * die die Fakultät zu einem int-Wert berechnet. 
	 */

	
	public static void main(String[] args) {
		
		int erg = getFakultaet(3); // erg = 6
		System.out.println(erg);
	}
	
	static int getFakultaet(int n) {
		
		if (n<1) {
			System.out.println("False Eingabe für Fakultät!");
			return 0;
		}
		
		int res = 1;
		for (int i = 1; i <= n ; i++) {
			res *= i;
		}
		
		return res;
	}
}
