package ub08_methodenFakultaet;

public class SolutionTask1 {
	
	/*
	 * Task
	 * Definieren Sie eine statische Methode "getFakultaetRekursiv", 
	 * die die Fakultät zu einem int-Wert rekursiv berechnet. 
	 */

	
	public static void main(String[] args) {
		
		int erg = getFakultaetRekursiv(3); // erg = 6
		System.out.println(erg);
	}
	
	
	static int getFakultaetRekursiv(int n) {
		
		if(n<=1) {
			return 1;
		} else {
			return (n * getFakultaetRekursiv( --n ));
		}
		
	}
}
