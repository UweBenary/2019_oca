package ub40_lesenAusEinerTextdatei;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class LesenAusEinerTextdatei {

	public static void main(String[] args) {
				
		/*
		 * Benutzen Sie folgenden Code und erstellen Sie bitte damit eine Methode 'String loadFromFile(String path)'.
		 * 
		 *  Die Methode soll den Text aus einer Datei laden und als String zurückliefern.
		 *  
		 *  Verwenden Sie bitte auch den StringBuilder für die Lösung (also keine Strings-Konkatenationen)
		 */


		
	
		Path absolutePath = Paths.get(System.getProperty("user.dir"));// gleich mit: String s = new File("").getAbsolutePath();
		System.out.println(absolutePath);
		
		String relativePath = "Datei mit Text.txt";
		
		Path path2File = Paths.get(absolutePath.toString(),"bin","ub40_lesenAusEinerTextdatei", relativePath);
				
//		String absolutePath2 = LesenAusEinerTextdatei.class.getProtectionDomain().getCodeSource().getLocation().getPath();
//		System.out.println(absolutePath2);	

		
		try( BufferedReader in = new BufferedReader( new FileReader( /*relativePath*/ path2File.toString() ) ) ) {
			
			String line;
			while( (line = in.readLine()) != null ) {
				
				System.out.println("Zeile: " + line);
			}
			
		} catch (IOException e) {
			System.out.println("Fehler! Kann die Datei nicht lesen");
			e.printStackTrace();
		}
		
		
		String s = loadFromFile(path2File.toString());
		System.out.println(s);
		
		System.out.println("end of main");
		
	}
	
	static String loadFromFile(String path) {
		StringBuilder sb = new StringBuilder();
		
		try( BufferedReader in = new BufferedReader( new FileReader( path ) ) ) {
			
			String line;
			while( (line = in.readLine()) != null ) {
				
				sb.append(line);
				sb.append(System.lineSeparator());
			}
			
		} catch (IOException e) {
			System.out.println("Fehler! Kann die Datei nicht lesen");
			e.printStackTrace();
		}
		return sb.toString();
	}

}
