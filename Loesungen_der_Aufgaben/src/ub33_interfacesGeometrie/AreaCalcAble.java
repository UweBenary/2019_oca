package ub33_interfacesGeometrie;

public interface AreaCalcAble {
	
	double getArea ();

}
