package ub33_interfacesGeometrie;

import java.util.Random;

public class RandomRectangle extends Rectangle {

	public RandomRectangle() {
		Random rand = new Random();
		int r1 = rand.nextInt(6) + 1;
		int r2 = rand.nextInt(6) + 1;
		setHight(r1);
		setWidth(r2);
	}
}
