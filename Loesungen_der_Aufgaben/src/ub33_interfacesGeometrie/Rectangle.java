package ub33_interfacesGeometrie;

public class Rectangle 
	implements AreaCalcAble {
	
	private double width;
	private double hight;

	public void setWidth(double width) {
		this.width = width;
	}

	public void setHight(double hight) {
		this.hight = hight;
	}

	public double getWidth() {
		return width;
	}

	public double getHight() {
		return hight;
	}

	@Override
	public double getArea() {
		return getHight() * getWidth();
	}
	
	@Override
	public String toString() {
		return "Rectangle of " + getWidth() + "x" + getHight();
	}
}
