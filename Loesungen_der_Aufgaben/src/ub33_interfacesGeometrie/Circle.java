package ub33_interfacesGeometrie;

public class Circle 
	implements AreaCalcAble {
	
	private double radius;

	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}

	@Override
	public double getArea() {
		return Math.PI * getRadius() * getRadius();
	}
	
	@Override
	public String toString() {
		return "Circle of radius " + getRadius();
	}	
}
