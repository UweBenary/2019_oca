package ub33_interfacesGeometrie;

import java.util.Random;

public class RandomCircle extends Circle {


	public RandomCircle() {
		Random rand = new Random();
		int r1 = rand.nextInt(6) + 1;
		setRadius(r1);
	}

}
