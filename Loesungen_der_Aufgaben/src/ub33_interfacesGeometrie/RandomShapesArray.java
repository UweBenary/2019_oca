package ub33_interfacesGeometrie;

import java.util.Random;

public class RandomShapesArray {

	public static void main(String[] args) {

		AreaCalcAble[] shapes = new AreaCalcAble[100];
		Random rand = new Random();
		
		for (int i = 0; i < shapes.length; i++) {
			shapes[i] = rand.nextBoolean() ? new RandomRectangle() : new RandomCircle();
		}
		
		printAreas(shapes);		
	}
	
	
	static double[] applyGetArea(AreaCalcAble[] shapes) {
		
		double[] result = new double[shapes.length];
		
		for (int i = 0; i<shapes.length; i++) {
			result[i] = shapes[i].getArea();
		}

		return result;
	}
	
	static void printAreas(AreaCalcAble[] shapes) {
		
		double[] areaArray = applyGetArea(shapes);
		
		for (int i = 0; i<shapes.length; i++) {
			System.out.printf("%3d. %s has an area of %.2f.%n", i+1, shapes[i].toString(), areaArray[i]);
		}
	}

}
