package ub11_kleineAufgabeKlassen;

class Book {
	int numOfPages ;
}

public class Buecher {

	/*
	 * - Es soll Bücher geben können.
	 * - Jedes Buch hat nur eine Eigenschaft: Anzahl Seiten.
	 * - Bitte erzeugen Sie 2 Bücher.
	 * - Geben Sie zu jedem Buch die Anzahl seine Seiten.
	 */
	
	public static void main(String[] args) {

		Book book1 = new Book();
		book1.numOfPages = 276;
		
		printInfoBook(book1);
		
		Book book2 = new Book();
		book2.numOfPages = 42;
		
		printInfoBook(book2);
		
	}
	
	static void printInfoBook(Book book) {
		System.out.println("Buchseiten: " + book.numOfPages);
	}

}
