package ub51_exceptionsParseDate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Scanner;

public class Solution {

	public static void main(String[] args) {

		
		LocalDate d = LocalDate.now();
		System.out.println( d );
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern( "dd.mm.yyyy");
		String string =d.format(formatter);
		
		try {
			Date d2 = new SimpleDateFormat("yyyy-mm-dd").parse("2009-12-05");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		
//		
//		Date date = getDate();
////		System.out.println(date);
//		
//		// date -> String
//		DateFormat df = DateFormat.getDateInstance( );		
//		String dateAsText = df.format(date);
//		
//		System.out.println("German date: " + dateAsText );
//		
//		// String -> LocalDate
//		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
//		LocalDate date2 = LocalDate.parse(dateAsText, formatter);
//			
//		// date2 -> otherString
//		formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
//		String date2AsText = formatter.format(date2);
//			  
//		System.out.println("English date: " + date2AsText);

	}
	

	private static Date getDate() {
		
		DateFormat df = DateFormat.getDateInstance( DateFormat.SHORT );
		Date date;
		
		while (true){
			try {
				date = df.parse( getUserInput() );
				break;
				
			} catch (Exception e) {
				System.out.println(" -> Please use format dd.MM.JJJJ");
			}
		}
		return date;
	}

	private static String getUserInput() {
		
		System.out.println("Please enter a date!");
		String input = new Scanner( System.in ).nextLine();
		
		// default in case of pressing >enter<
		if (input.length()==0) {
			return "01.01.1977";
			
		} else {
			return input;
		}
	}

}
