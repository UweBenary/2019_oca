package ub42_arrayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Solutions {

	public static void main(String[] args) {
		int exercise = 0;
		/*
		 * A1. Erzeugen Sie eine `ArrayList` mit 20 zufälligen Integer aus dem Bereich [-100 ... 100]. 
		 * Geben Sie die Elemente der Liste durchnummeriert untereinander aus.
		 */
		System.out.println("*** Exercise " + ++exercise);
		ArrayList<Integer> intList = new ArrayList<Integer>();
		Random rand = new Random();
		for (int i = 0; i < 20; i++) {
			intList.add( rand.nextInt(201) - 100 );
		}
		for ( int i = 0; i < intList.size(); i++) {
			System.out.println( i+1 + ". " + intList.get(i) );
		}
		
		/*
		 * A2. Erzeugen Sie eine `ArrayList` mit folgenden Strings: "mo", "di", "mi", "o", "fr". 
		 * Geben Sie die Elemente der Liste mit der foreach-Schleife aus. 
		 */
		System.out.println("*** Exercise " + ++exercise);
		ArrayList<String> strList = new ArrayList<String>();
		strList.add("mo");
		strList.add("di");
		strList.add("mi");
		strList.add("o");
		strList.add("fr");
		for (String string : strList) {
			System.out.println( string );
		}
		
		/*
		 * A3. Erzeugen Sie eine `ArrayList` mit folgenden Werten: 10, 20, 30, 40, 50, 60, 70, 80. 
		 * Geben Sie jedes zweite Element der Liste aus.
		 */
		System.out.println("*** Exercise " + ++exercise);
		intList = new ArrayList<Integer>();
		for (int i = 0; i < 8; i++) {
			intList.add( (i+1)*10 );
		}
		System.out.println(intList);
		for (int i = 1; i < intList.size(); i+=2) {
			System.out.println( intList.get(i) );
		}
		
		/*
		 * A4. Erzeugen Sie eine `ArrayList` mit folgenden Werten: 10, 20, 30, 40, 50, 60, 70, 80. 
		 * Geben Sie die Elemete der Liste rückwärts aus.
		 */
		System.out.println("*** Exercise " + ++exercise);
		intList = new ArrayList<Integer>();
		for (int i = 0; i < 8; i++) {
			intList.add( (i+1)*10 );
		}
		for (int i = intList.size() - 1; i > -1  ; i--) {
			System.out.print( intList.get(i) + " ");
		}		
		System.out.println();
		
		/*
		 * A5. Erzeugen Sie eine `ArrayList` mit 20 zufälligen Integer aus dem Bereich [-100 ... 100]. 
		 * Sortieren Sie die Liste und geben Sie sie aus.
		 */
		System.out.println("*** Exercise " + ++exercise);
		intList = new ArrayList<Integer>();
//		rand = new Random();
		for (int i = 0; i < 20; i++) {
			intList.add( rand.nextInt(201) - 100 );
		}
		java.util.Collections.sort(intList);
		for ( int i = 0; i < intList.size(); i++) {
			System.out.println( i+1 + ". " + intList.get(i) );
		}
		
		/*
		 * A6. 
		 * Erzeugen Sie eine `ArrayList` mit 10 zufälligen Integer aus dem Bereich [10 ... 15]. 
		 * Suchen Sie mit `indexOf` nach dem Wert 12.
		 */
		System.out.println("*** Exercise " + ++exercise);
		intList = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			intList.add( rand.nextInt(15+1 - 10) + 10 );
		}
		int result = intList.indexOf(12);
		System.out.println("Position of 12 in " + intList + " is: " + result);
		
		/*
		 * A7. Erzeugen Sie eine `ArrayList` mit 10 zufälligen Integer aus dem Bereich [0 ... 50]. 
		 * Suchen Sie mit `Collections.binarySearch` nach dem Wert 12.
		 */
		System.out.println("*** Exercise " + ++exercise);
		intList = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			intList.add( rand.nextInt(51) );
		}
		java.util.Collections.sort(intList);
		result = Collections.binarySearch(intList, 12);
		System.out.println("Position of 12 in " + intList + " is: " + result);
		
		/*
		 * A8. Erzeugen Sie eine `ArrayList` mit 10 zufälligen Integer aus dem Bereich [0 ... 50]. 
		 * Entfernen Sie mit `remove(int)` die ungeraden Werte.
		 */
		System.out.println("*** Exercise " + ++exercise);
		intList = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			intList.add( rand.nextInt(51) );
		}
		System.out.println( intList );
		boolean isRemoved;
//		for (int i = 0; i < intList.size(); i+=2) {
//			do {
//				isRemoved = intList.remove( Integer.valueOf(i) );
//			} while (!isRemoved);			
//		}
		for (int i = 0; i < intList.size(); i++) {
			if (intList.get(i) % 2 == 0) {
				intList.remove(i);
			}		
		}
		System.out.println( intList );
		
		/*
		 * A9. Erzeugen Sie eine `ArrayList` mit 10 zufälligen Integer aus dem Bereich [0 ... 50]. 
		 * Ersetzen Sie in einer Schleife mit der Methode `set` alle ungeraden Werte durch 0.
		 */
		System.out.println("*** Exercise " + ++exercise);
		intList = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			intList.add( rand.nextInt(51) );
		}
		System.out.println( intList );
		for (int i = 0; i < intList.size(); i++) {
			if (intList.get(i) % 2 == 1) {
				intList.set(i, 0);
			}		
		}
		System.out.println( intList );
		
		/*
		 * A10. 
		 * Jedes Objekt vom Typ `Sportart` soll ein String-Attribut 'name' und ein boolean-Attribut 'olympisch' haben.
			- Erzeugen Sie eine `ArrayList` mit 5 Elementen vom Typ `Sportart`.
			- Überprüfen Sie mit der Methode `contains` ob die olympische Sportart 'Ringen' sich in der Liste befindet.
			- Suchen Sie mit `Collections.binarySearch` nach einer Sportart Ihrer Wahl.
		 */
		System.out.println("*** Exercise " + ++exercise);
		List<Sport> sportsList = new ArrayList<>();
		sportsList.add(new Sport("wrestling", true));
		sportsList.add(new Sport("crochet", false));
		sportsList.add(new Sport("badminton", true));
		sportsList.add(new Sport("pelota", false));
		sportsList.add(new Sport("golf", true));
		System.out.println(sportsList);
		boolean has = sportsList.contains(new Sport("wrestling", false));
		System.out.println("Does list include \"wrestling\"? " + (has? "Yes." : "No."));
		Collections.sort(sportsList);
		Sport sp =  new Sport("crochet", false);
		result = Collections.binarySearch(sportsList, sp);
		System.out.printf("Position of \"%s\" in %s is: %d %n", sp, sportsList, result);
		
		/*
		 * A11. Erstellen Sie folgende Liste mit Strings: mo, fr, di, fr, mi, fr, do, fr.
		 * Verwenden Sie bitte die Methode 'boolean remove(Object)' solange, 
		 * bis es kein String "fr" mehr gibt.
		 */
		System.out.println("*** Exercise " + ++exercise);
		strList = new ArrayList<String>();
		strList.add("mo");
		strList.add("fr");
		strList.add("di");
		strList.add("fr");
		strList.add("mi");
		strList.add("fr");
		strList.add("do");
		strList.add("fr");
		System.out.println( strList );
		isRemoved = false;
		do {
			isRemoved = strList.remove( "fr" );
		} while (isRemoved);			
		System.out.println( strList );
		
		/*
		 * A12. Speichern Sie bitte in einer ArrayList folgende drei Arrays:
	  			int[] a1 = { 1, 2, 3};
	  			int[] a2 = { -7, -5 };
	  			int[] a3 = { 101, 202, 303 };
		 * Geben Sie die Inhalte der ArrayList aus, so dass man die int-Werte der Arrays auf der Konsole auch sehen kann.
		 */
		System.out.println("*** Exercise " + ++exercise);
		int[] a1 = { 1, 2, 3};
		int[] a2 = { -7, -5 };
		int[] a3 = { 101, 202, 303 };
		List<int[]> arrList= new ArrayList<>();
		arrList.add(a1);
		arrList.add(a2);
		arrList.add(a3);
		for (int[] arr : arrList) {
			System.out.print( Arrays.toString(arr) + " ");
		}
		System.out.println();
	} // end of main
}
