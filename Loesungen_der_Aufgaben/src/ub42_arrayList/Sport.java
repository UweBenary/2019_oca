package ub42_arrayList;

public class Sport 
	implements Comparable<Sport> {

	private String name;
	private boolean olympic;
	
	public Sport(String name, boolean olympic) {
		this.name = name;
		this.olympic = olympic;
	}	
	
	public String getName() {
		return name;
	}

	public boolean isOlympic() {
		return olympic;
	}

	@Override
	public String toString() {
		return name + (olympic ? " (olympic)" : "");
	}
	
	@Override
	public boolean equals(Object obj) {
		Sport sp = (Sport) obj;
		return name.equals( sp.name );
	}

	@Override
	public int compareTo(Sport sp) {
		return name.compareTo( sp.getName() );
	}
}
