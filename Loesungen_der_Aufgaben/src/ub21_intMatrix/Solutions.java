package ub21_intMatrix;

import java.util.Random;

public class Solutions {


	public static void main(String[] args) {


		/*
		 * Erstellt eine IntMatrix mit 2 Zeilen und 3 Spalten:
  		 */
        IntMatrix m1 = new IntMatrix(2, 3);

        /*
         * Erstellt eine IntMatrix mit 5 Zeilen und 3 Spalten. Alle Elemente der IntMatrix sind mit dem Wert 100 initialisiert:
         */
        IntMatrix m2 = new IntMatrix(5, 3, 100);
		
        /*
         * System.out.println(m1) gibt auf der Konsole die IntMatrix m1 in folgender Form aus:
         *         0, 0, 0
         *         0, 0, 0
         */
        System.out.println(m1);
        
        /*
         * Liefert den Wert an der Stelle (1, 2) der IntMatrix (Zeile 1, Spalte 2):
         */
        int i = m1.get(1, 2);
        System.out.println(" i = " + i);
        
        /*
         * Erstellt eine IntMatrix mit 4 Zeilen und 6 Spalten. 
         * Alle Elemente werden zufällig initialisiert. 
         * Der erlaubte Wertebereich für die Elemente: 0 bis 200:        
         */
        IntMatrix m3 = IntMatrix.getRandomMatrix(4, 6, 200);
        System.out.println("\nRandom matrix:");
        System.out.println(m3);
        
        /*
         * Liefert true, falls die Matrizen die gleichen Werte an den entsprechenden Stellen gespeichert haben. 
         * Die Matrizen mit ungleichen Dimensionen sind ungleich:    
         */
        System.out.println("m1.equals(m2): " + m1.equals(m2));
        IntMatrix m4 = new IntMatrix(3, 2);
        System.out.println("Test m1.equals(m4) expect 'false': " + m1.equals(m4));
        IntMatrix m5 = new IntMatrix(5, 3, 100);    
        System.out.println("Test m2.equals(m5) expect 'true': " + m2.equals(m5));
        
        /*
         * Optional. 
         * Überlegen Sie sich weitere Operationen, die mit Matrizen möglich sind. 
         * Z.B.: Matrix transponieren, Matrizen addieren, Matrizen multiplizieren u.s.w.
         */
        IntMatrix m6 = new IntMatrix(2, 3, 1);
        System.out.println("m6: ");
        System.out.println(m6);
        System.out.println("m6 + m6: ");
        System.out.println(IntMatrix.add(m6, m6));
        System.out.println("m6 transposed: ");
        IntMatrix m6T = IntMatrix.transpose(m6);
        System.out.println(m6T);
//        System.out.println(IntMatrix.add(m6, m6T)); // throws exception as expected
        System.out.println("m6T x m6: ");
        System.out.println(IntMatrix.multiply(m6T, m6));
        
        /*
         * Optional.
         * Erzeugen Sie ein Array aus Matrizen mit unterschiedlichen Dimensionen
         */
        IntMatrix[] arr = new IntMatrix[3];
        for (int j = 0; j < arr.length; j++) {
        	Random rand = new Random();
			arr[j] = IntMatrix.getRandomMatrix(rand.nextInt(6)+2, rand.nextInt(6)+2, 6);
		}
        
        System.out.println("Three random matrixes: ");
        for (int j = 0; j < arr.length; j++) {
            System.out.println(arr[j]); 
		}
        
	}

}
