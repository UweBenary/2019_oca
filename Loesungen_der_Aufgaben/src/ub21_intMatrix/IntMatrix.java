package ub21_intMatrix;

import java.util.Arrays;
import java.util.Random;

public class IntMatrix {
	private int[][] values ;
//	private int numberOfRows;
//	private int numberOfColumns;
	
	
	public IntMatrix(int rows, int columns ) {
		this(rows, columns, 0 );
	}
	
	
	public IntMatrix(int rows, int columns, int value ) {
//		numberOfRows = rows;
//		numberOfColumns = columns;
		if (rows<1 || columns<1) {
			throw new IllegalArgumentException("Matrix dimensions must be >0.");
		}
		this.values = new int[rows][columns];
		for (int i = 0; i < numberOfRows(); i++) {
			for (int j = 0; j < numberOfColumns(); j++) {
				values[i][j] = value;
			}
		}		
	}
	
	
	@Override
//	public String toString() {
//		for (int i = 0; i < values.length; i++) {
//			for (int j = 0; j < values[i].length; j++) {
//				if (j < values[i].length-1) {
//					System.out.print(values[i][j] + ", ");
//				} else {
//					System.out.println(values[i][j]);
//				}
//			}
//		}		
//	return "";
//	}
	public String toString() {
		StringBuilder resultString = new StringBuilder(""); 
		for (int i = 0; i < numberOfRows(); i++) {
			for (int j = 0; j < numberOfColumns(); j++) {
				if (j < numberOfColumns()-1) {
					resultString.append(get(i, j)).append(", ");
				} else {
					resultString.append(get(i, j)).append( System.lineSeparator() );
				}
			}
		}		
	return new String( resultString.toString() );
	}


	public int get(int row, int column) {
		return values[row][column];
	}
	
	
	public void set(int row, int column, int value) {
		values[row][column] = value;
	}


	public int numberOfRows() {
//		return this.numberOfRows;
		return this.values.length;
	}
	
	
	public int numberOfColumns() {
//		return this.numberOfColumns;
		return this.values[0].length;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntMatrix other = (IntMatrix) obj;
		if (!Arrays.deepEquals(values, other.values))
			return false;
		return true;
	}


	public static IntMatrix getRandomMatrix(int rows, int columns, int upperBound) {
		Random rand = new Random();
		IntMatrix matrix = new IntMatrix(rows, columns);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				matrix.set(i, j, rand.nextInt(upperBound));
			}
		}
		return matrix;
	}
	
	
	public static IntMatrix transpose(IntMatrix m1) {
		IntMatrix result = new IntMatrix(m1.numberOfColumns(), m1.numberOfRows());
		for (int i = 0; i < result.numberOfRows(); i++) {
			for (int j = 0; j < result.numberOfColumns(); j++) {
				result.set(i, j, m1.get(j, i));
			}
		}
		return result;
	}
	
	
	public static IntMatrix add(IntMatrix m1, IntMatrix m2) {
		if (m1.numberOfRows()!=m2.numberOfRows()) {
			throw new IllegalArgumentException("Matrix dimension mismatch!");			
		}
		if (m1.numberOfColumns()!=m2.numberOfColumns()) {
			throw new IllegalArgumentException("Matrix dimension mismatch!");			
		}
		IntMatrix result = new IntMatrix(m1.numberOfRows(), m1.numberOfColumns());
		for (int i = 0; i < result.numberOfRows(); i++) {
			for (int j = 0; j < result.numberOfColumns(); j++) {
				result.set(i, j, m1.get(i, j) + m2.get(i, j));
			}
		}
		return result;
	}
	
	
	public static IntMatrix multiply(IntMatrix m1, IntMatrix m2) {
		if (m1.numberOfColumns()!=m2.numberOfRows()) {
			throw new IllegalArgumentException("Matrix dimension mismatch! (kXn)*(nXm)");			
		}

		IntMatrix result = new IntMatrix(m1.numberOfRows(), m2.numberOfColumns());
		for (int i = 0; i < result.numberOfRows(); i++) {
			for (int j = 0; j < result.numberOfColumns(); j++) {

				int res = 0;
				for (int k = 0; k < m1.numberOfColumns(); k++) {
					 res += ( m1.get(i, k) * m2.get(k, j) );
				}
				result.set(i, j, res);
				
			}
		}
		return result;
	}
}
	
	
	
	