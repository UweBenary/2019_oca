package ub14_klassenPersonen;

/*
 * Aufgaben A1-A8 siehe Person.java
 * 
 */
public class Solution {

	/*
	 * A8:
	 * Erzeugen Sie bitte eine statische Methode getMustermann 
	 * die eine neue Person 'Max Mustermann, Geburtsjahr: 1960, Plz: 12345' bildet 
	 * und zurück liefert. Testen Sie die Methode.
	 */
	public static Person getMustermann() {
		return new Person("Max", "Mustermann", 1960, "12345");
	}
	
	
	public static void main(String[] args) {
	
	/*
	 * A5:
	 * Erzeugen Sie 2 Beispielinstanzen der Klasse Person mit unterschiedlichen Konstruktoren.
	 */
	Person person1 = new Person("Klaus", "Dieter");
	Person person2 = new Person("Martina", "Mustermann", 1976, "01309");
	
	/*
	 * A6:
	 * Geben Sie die erstellten Personen auf der Konsole aus:
	 * Beispiel der Ausgabe: Hans Meyer, Geburtsjahr: 1950, Plz: 12345
	 */
	System.out.println("A6:");
	System.out.println(person1.show());
	System.out.println(person2.show());
	
	/*
	 * A7:
	 * Überschreiben Sie die Methode equals und testen Sie sie mit den erstellten Personen.
	 */
	System.out.println("A7;");
	System.out.println("person1.equals(person2): " + person1.equals(person2));
	
	Person person3 = new Person(person1.getVorname(), person1.getNachname());
	System.out.println("person1.equals(person3): " + person1.equals(person3));
	
	/*
	 * A8:
	 * Erzeugen Sie bitte eine statische Methode getMustermann 
	 * die eine neue Person 'Max Mustermann, Geburtsjahr: 1960, Plz: 12345' bildet 
	 * und zurück liefert. Testen Sie die Methode.
	 */
	Person person4 = getMustermann();
	System.out.println("A8:");
	System.out.println(person4.show());
	
	
	/*
	 * A9:
	 * Sorgen Sie dafür, dass auch folgender Code ein neues Person-Objekt erzeugt:
	 * 	
	Person p = new PersonBuilder()
			.forname("John")
			.nachname("Doe")
			.geburtsjahr(1987)
			.build();
	 */
	
	// adapted from: https://en.wikipedia.org/wiki/Builder_pattern#Java
	//ConcreteBuilder: 	Person
	//Builder:			PersonBuilder
	//Client:
	Person p = new PersonBuilder().setForname("John").setNachname("Doe").setGeburtsjahr(1987).build();
    System.out.println("A9:");
    System.out.println(p);

    
	}	
}
