package ub14_klassenPersonen;

/*
 * A1: 
 * Erstellen Sie eine neue Klasse Person mit folgenden Attributen: 
 * Vorname, Nachname, Geburtsjahr, Postleitzahl. 
 * Verwenden Sie für diese Attribute sinnvolle Datentypen und Namen.
 */
class Person {
	
	private String vorname;
	private String nachname;
	private int geburtsjahr;
	private String postleitzahl;
	
	/*
	 * A2:
	 * Erstellen Sie mindestens zwei Konstruktoren für die Klasse Person.
	 */
	
	public Person(String vorname, String nachname) {
		this(vorname, nachname, 0, "");
	}

	public Person(String vorname, String nachname, int geburtsjahr, String postleitzahl) {
		this.vorname = vorname;
		this.nachname = nachname;
		this.geburtsjahr = geburtsjahr;
		this.postleitzahl = postleitzahl;
	}
	
	/*
	 * A3:
	 * Überschreiben Sie die Methode toString() sinnvoll.
	 */
	@Override
	public String toString() {
		return "Person: " + this.vorname + " " + this.nachname +
				" (born: " + this.geburtsjahr +")" + " ZIP: " + this.postleitzahl;
	}
	
	/*
	 * A4: 
	 * Definieren Sie alle Getter- und Setter-Methoden nach JavaBeans Standard.
	 */
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	
	public String getVorname() {
		return this.vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public int getGeburtsjahr() {
		return geburtsjahr;
	}

	public void setGeburtsjahr(int geburtsjahr) {
		this.geburtsjahr = geburtsjahr;
	}

	public String getPostleitzahl() {
		return postleitzahl;
	}

	public void setPostleitzahl(String postleitzahl) {
		this.postleitzahl = postleitzahl;
	}
	
	/*
	 * A6:
	 * Geben Sie die erstellten Personen auf der Konsole aus:
	 * Beispiel der Ausgabe: Hans Meyer, Geburtsjahr: 1950, Plz: 12345
	 */
	public String show() {
		return "" + this.vorname + " " + this.nachname + ", Geburtsjahr: " + this.geburtsjahr 
				+ ", Plz: " + this.postleitzahl;
	}	
	
	/*
	 * A7:
	 * Überschreiben Sie die Methode equals und testen Sie sie mit den erstellten Personen.
	 */
	@Override
	public boolean equals(Object obj) {
		Person p = (Person) obj;
		if (this.vorname.equals(p.vorname) &&
			this.nachname.equals(p.nachname) &&
			this.geburtsjahr == p.geburtsjahr &&
			this.postleitzahl.equals(p.postleitzahl)) {
			return true;
		} else {
			return false;
		}
	}
}