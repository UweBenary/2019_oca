package ub14_klassenPersonen;

public class PersonBuilder {

	/*
	 * A9: 
	 * Sorgen Sie dafür, dass auch folgender Code ein neues Person-Objekt erzeugt:
	 * 	
	Person p = new PersonBuilder()
			.forname("John")
			.nachname("Doe")
			.geburtsjahr(1987)
			.build();
	 */
	
	private String forname;
	private String nachname;
	private int geburtsjahr;

    public PersonBuilder setForname(String forname) {
        this.forname = forname;
        return this;
    }
    public PersonBuilder setNachname(String nachname) {
        this.nachname = nachname;
        return this;
    }
    public PersonBuilder setGeburtsjahr(int geburtsjahr) {
        this.geburtsjahr = geburtsjahr;
        return this;
    }
    public Person build() {
        return new Person(this.forname, this.nachname, this.geburtsjahr, "");
    }
	
}
