package ub32_polymorphieBruteforce;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.util.Date;

public class PrintShortInLogFile extends Logger {

	@Override
	void writeLog(Test test) {
		int testCount = test.getTestCount();
		if ( testCount == 1 || test.isTestResult() ) {
	        Date date = new Date();
	        Timestamp ts = new Timestamp(date.getTime());
			String word = test.getPw2Try().toString();
			String testResult = test.isTestResult() ? "Correct password found." : "Password failed.";
			System.out.printf("Test #%d (%s): %s - %s %n", testCount, ts, word, testResult);
		}
	}
	
	private void write2File(String content) {
		//https://stackoverflow.com/questions/1625234/how-to-append-text-to-an-existing-file-in-java
		
		try{
            // Create new file
            
            String path="D:\\a\\hi.txt";
            File file = new File(path);

            // If file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);

            // Write in file
            bw.write(content);

            // Close connection
//            bw.close();
        }
        catch(Exception e){
            System.out.println(e);
        }
	}
}
