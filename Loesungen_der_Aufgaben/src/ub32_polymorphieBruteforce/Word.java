package ub32_polymorphieBruteforce;

import java.util.Arrays;

public class Word {

	private char[] word;
	
	Word(String word) {
		this( word.toCharArray() );
	}	
	
	Word(char[] word) {
		setWord(word);
	}
	
	private void setWord(char[] word) {
		if (word==null || word.length == 0) {
			throw new IllegalArgumentException("Null or length = 0 exception.");
		}
		for (char c : word) {
			if (c < 'a' || c > 'z') {
				throw new IllegalArgumentException("Only characters a-z are allowed.");
			}
		}
		this.word = word;
	}	
	
	char[] getWord() {
		return word;
	}
	
	void setCharAt(int x, char ch) {
		if ( ch >= 'a' && ch <= 'z') {
			word[x] = ch;
		} else {
			throw new IllegalArgumentException("Only characters a-z are allowed.");
		}
	}
	
	char getCharAt(int x) {
		return word[x];
	}
	
	int length() {
		return word.length;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
        for (int i = 0; i < word.length ; i++) {
            sb.append(word[i]);
        }
        return sb.toString();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof char[]) {
			return Arrays.equals( this.getWord(), (char[]) obj);
		} else if (obj instanceof Word) {
			Word w = (Word) obj;
			return Arrays.equals( this.getWord(), w.getWord());
		} else {
			return false;
		}
	}
	
	/*
	 * "UnitTests"
	 */
//	public static void main(String[] args) {		
//		Word w = new Word(new char[] {'a', 'b'});
//		System.out.println(w);
//		System.out.println(w.getWord());
//		
//		w = new Word("abba");
//		System.out.println(w.getWord());
//		
//		Word w2 = new Word(new char[] {'a', 'b', 'b', 'a'});
//		System.out.println(w.equals(w2));
//		
//		Word w3 = new Word(new char[] {98, 102});
//		System.out.println(w3);
//		
//		w.setCharAt(0, 'c');
//		System.out.println(w.getCharAt(0));
//		System.out.println(w);
//		
////		w.setWord(new char[] {});
////		w.setWord(new char[] {'a', '3'});
//	}
	
}
