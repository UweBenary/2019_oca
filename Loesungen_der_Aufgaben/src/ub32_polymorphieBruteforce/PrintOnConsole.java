package ub32_polymorphieBruteforce;

public class PrintOnConsole extends Logger {

	@Override
	void writeLog(Test test) {
		int testCount = test.getTestCount();
		String word = test.getPw2Try().toString();
		String testResult = test.isTestResult() ? "Correct password found." : "Password failed.";
		System.out.printf("Test #%d: %s - %s %n", testCount, word, testResult);
	}
}
