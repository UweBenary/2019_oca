package ub49_lambdasPredicateJavaTipp;


import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;


public class JavaTipp {

	private String text;
	private List<String> themen = new ArrayList<String>();
	
	
	public JavaTipp(String text, String... themen) {
	
		this.text = text;
		
		for (int i = 0; i < themen.length; i++) {
			this.themen.add( themen[i] );
		}
	}
	
	@Override
	public String toString() {
		return "Text: " + text + System.getProperty("line.separator") 
		+ "Themen: " + themen + System.getProperty("line.separator");
	}
	
	
	
	
	
	public static void main(String[] args) {
		
		/*
		 * A1
		 */
		List<JavaTipp> list = new ArrayList<JavaTipp>();
		list.add(
			new JavaTipp(
				"Mit dem Schlüsselwort 'implements' kann eine Klasse ein Interface realisieren.", 
				"Vererbung", 
				"Klassen", 
				"Interfaces"
				)
			);
		list.add(
			new JavaTipp(
				"Eine Klasse kann nur eine andere Klasse erweitern.", 
				"Vererbung", 
				"Klassen"
				)
			);
		list.add(
			new JavaTipp(
				"Statische Methoden werden mit dem Klassennamen aufgerufen.", 
				"Klassen",
				"Methoden",
				"static"
				)
			);
		list.add(
			new JavaTipp(
				"Alle Attribute in einem neuen Objekt werden im Konstruktor initialisiert.", 
				"Klassen", 
				"Konstruktoren", 
				"Attribute"
				)
			);
		list.add(
			new JavaTipp(
				"Beim Überschreiben darf die Sichtbarkeit nicht verschärft werden.", 
				"Klassen",
				"Methoden",
				"Überschreiben"
				)
			);
		list.add(
			new JavaTipp(
				"Alle Elemente in einem Interface sind immer 'public'", 
				"Interfaces",
				"Sichtbarkeiten",
				"static"
				)
			);

		/*
		 * A2
		 */
		System.out.println("**** filter and print list");
		
		Consumer< List<JavaTipp> > printer = new Consumer< List<JavaTipp> >() {

			@Override
			public void accept(List<JavaTipp> arrList) {
				for (JavaTipp javaTipp : arrList) {
					System.out.println(javaTipp);
				}				
			}
		};
			
		String searchString = "Klassen";
		Predicate<JavaTipp> pred = jt -> jt.themen.contains(searchString);
		
		System.out.println(
				"Anzahl gefundene Java-Tipps: " + filterJavaTipps(list, pred).size()
				+ " aus " + list.size() 
				);
		System.out.println();

		printer.accept( filterJavaTipps(list, pred) );
		
		System.out.println("**** filter and return list");
//		ArrayList<JavaTipp> resultList = filterJavaTipps(list, pred);
//		System.out.println(resultList);
		
		Function< ArrayList<JavaTipp>, ArrayList<JavaTipp> > func = x -> new ArrayList<JavaTipp>( x );
		List<JavaTipp> funcList = func.apply( filterJavaTipps(list, pred) );
		System.out.println(funcList);
		
		System.out.println("**** filter and provide list");
		Supplier< ArrayList<JavaTipp> > supplier = () -> filterJavaTipps(list, pred);
		printer.accept( supplier.get() );
		
		
		/*
		 * A3
		 */
		System.out.println("**** A3:");
		printer.accept( filterJavaTipps(list, x -> true) );
		
		/*
		 * A4
		 */
		System.out.println("**** A4:");
		printer.accept( filterJavaTipps(list, x -> x.text.contains("Klasse")) );
		
		/*
		 * A5
		 */
		System.out.println("**** A5:");
		printer.accept( filterJavaTipps(list, x -> x.themen.size() == 2) );
		
		/*
		 * A6
		 */
		System.out.println("**** A6:");
		printer.accept( filterJavaTipps(list, x -> x.themen.contains("Klassen")) );
		
		/*
		 * A7
		 */
		System.out.println("**** A7:");
		printer.accept( filterJavaTipps(list, x -> x.themen.contains("Klassen") & !x.themen.contains("Vererbung")) );
		
		/*
		 * A8
		 */
		System.out.println("**** A8:");
		printer.accept( filterJavaTipps(list, x -> !x.text.endsWith(".") ) );
		System.out.println("***");
		printer.accept( filterJavaTipps(list, x -> x.themen.add("dummy") ) );
		
	}
	

	 static ArrayList<JavaTipp> filterJavaTipps(List<JavaTipp> arrList, Predicate<JavaTipp> filter) {
		
		 ArrayList<JavaTipp> returnList = new ArrayList<JavaTipp>( );

		for (JavaTipp javaTipp : arrList) {
			if (filter.test(javaTipp)) {
				returnList.add(javaTipp);
			}
		}
		
		return returnList;
		
	 }
	 
}
