package ub47_innerClassCars;

public class Test {

	public static void main(String[] args) {

		SportsCar car = new SportsCar("Mercedes");
		
		SportsCar.Driver f = new SportsCar.Driver("M.", "Schuhmacher");
	    car.setDriver(f);
	
	    SportsCar.Engine m = car.getEngine();
	
	    System.out.println(car);		//    Rennwagen Mercedes. Fahrer: M. Schuhmacher
	    System.out.println(m);		//    Motor Type1 aus dem Rennwagen Mercedes
	}

}
