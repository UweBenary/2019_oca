package ub47_innerClassCars;

public class SportsCar {


	class Engine {
		private final String TYPE = "Type1";
		
		@Override
		public String toString() {
			return "Motor " + TYPE + " aus dem Rennwagen " + /*SportsCar.this.*/getBrandName();
		}
	}
	
	static class Driver {

		private String firstName;
		private String surName;
		
		public Driver(String firstName, String surName) {
			this.firstName = firstName;
			this.surName = surName;
		}

		@Override
		public String toString() {
			return "Fahrer: " + firstName + " " + surName;
		}
	}
	
	private String brandName;
	private Driver driver;
	private Engine engine = new Engine();
	
	public SportsCar(String brandName) {
		this.brandName = brandName;
	}

	public void setDriver(Driver d) {
		driver = d;		
	}

	public Engine getEngine() {
		return engine;
	}

	public String getBrandName() {
		return brandName;
	}
	
	@Override
	public String toString() {
		return "Rennwagen " + brandName + ". " + driver;
	}
	
}
