package ub05_whileDowhile;

public class Zahlenreihen_While {

		public static void main(String[] args) {
			
			/*
			 * Folgende Zahlenreihe in einer Zeile ausgeben: 
			 * 0 1 2 3 4 5 6 7 8 9
			 */
			zahlenreiheEinsBisZehn();
			
			/*
			 * 0 2 3 4 5 6 7 8 9 
			 */
			zahlenreiheEinsBisZehnOhneZwei();
			
			/*
			 * -4 -2 0 2 4 ... 50
			 */	
			zahlenreiheZweierschritte();
			
			/*
			 * Bitte die englischen Kleinbuchstaben von a bis z in einer Zeile ausgeben
			 */
			kleinbuchstabenreihe();
			
			/*
			 * Bitte die englischen Grossbuchstaben rueckwaerts in einer Zeile ausgeben
			 */
			grossbuchstabenreiheRueckwaerts();
			
			/*
			 * Bitte die Zahlen aus dem Bereich [1 ... 100] ausgeben, die durch 5 ohne Rest dividierbar sind
			 */
			zahlenreiheEinsBisHundertModuloFuenf();
			
		}
		
		

		
		static void zahlenreiheEinsBisZehn() {

			System.out.println("Aufgabe 1:");
			
			int i=0;
			while (i<10) {
				System.out.print(i + " ");
				i++;
			} 
			
			System.out.println();
			
		}

		
		static void zahlenreiheEinsBisZehnOhneZwei() {
			
			System.out.println("\nAufgabe 2:");
			int i = 0;
			while (i<10) {
				if(i!=1) { 
					System.out.print(i);
					System.out.print(' ');
				}
				i++;
			} 
			
			System.out.println();
		}
		
		
		static void zahlenreiheZweierschritte() {
			
			System.out.println("\nAufgabe 3:");
			
			int i = -4;
			while (i <= 50) {
				System.out.print(i + " ");
				i+=2;
			} 

			System.out.println();
			
		}

		
		static void kleinbuchstabenreihe() {

			System.out.println("\nAufgabe 4:");
			
			char ch = 'a';
			while ( ch<='z' ) {
				System.out.print(ch + " ");
				ch++;
			} 
			
			System.out.println();
		
		}
		
		
		static void grossbuchstabenreiheRueckwaerts() {

			System.out.println("\nAufgabe 5:");
			
			char ch='Z';
			while (ch>='A') {
				System.out.print(ch + " ");
				ch--;
			} 
			
			System.out.println();
		}
		
		
		static void zahlenreiheEinsBisHundertModuloFuenf() {

			System.out.println("\nAufgabe 6:");
			
			int i=1;
			while (i<=100) {
				if( i%5 == 0 ) {
					System.out.print(i + " ");
				}
				i++;
			} 

			System.out.println();
			
		}

	}

