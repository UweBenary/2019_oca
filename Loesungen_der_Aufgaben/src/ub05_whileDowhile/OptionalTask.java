package ub05_whileDowhile;

public class OptionalTask {

	public static void main(String[] args) {

	/*
	 * Gegeben ist x = 100. 
	 * Dekrementieren Sie x um einen Zufälligen Wert aus dem Bereich [1 .. 5] 
	 * solange x größer 0 ist. 
	 * Geben Sie bei jeder Dekrementierung den neuen Wert der x aus.
	 */
	
		/*
		 * set initial values
		 */
		double x = 100D; 					//Gegeben ist x = 100. 
		System.out.println( "Start: " + x ); 
	
		double r; 							//Zufälligen Wert
		int runCount = 0;
		String outputString; 
	
		
		while (x>0) {                     	//solange x größer 0 ist
			runCount++;
			r = Math.random() + 4; 	//Zufälligen Wert aus dem Bereich [1 .. 5] 
												// not exact since max value of random < 1
			x -= r;                       	//Dekrementieren Sie x um einen Zufälligen Wert
			
			
			/*
			 * Geben Sie bei jeder Dekrementierung den neuen Wert der x aus.
			 */
			if (x>0) {
				outputString = "Step " + runCount + ": " + x;
				System.out.println( outputString ); 
			} else {
				;
			}
		}
		
		
	}

}
