package ub45_stringBuilder;


public class Solutions {

	public static void main(String[] args) {

		/*
		 * Erzeugen und initialisieren Sie drei String Variablen:
			        String s1 = "Hello";
			        String s2 = " ";
			        String s3 = "Welt";
		 */
        String s1 = "Hello";
        String s2 = " ";
        String s3 = "Welt";
		
		/*
		 * In einer Schleife konkatienieren Sie wiederholt die Variablen zu einem Gesamtstring. 
		 * Die Anzahl der Schleifenwiederholungen wählen Sie selbst aus. 
		 * Messen Sie die Zeit vor und nach der Schleife und ermitteln Sie am Ende die Zeitdifferenz.
		 */
        final long start = System.nanoTime();
        
//        String[] list = new String[] {s1, s2, s3};
//
//        String finalString = "";
//        for (int i = 0; i < list.length; i++) {
//			finalString = finalString + list[i];
//		}
//        System.out.println(finalString);
        String finalString = "";
        
        for (int i = 0; i < 10; i++) {
			finalString = s1 + s2 + s3;
		}
        System.out.println(finalString);
        
        final long end = System.nanoTime();

        System.out.println("Took: " + (end - start)/ 1_000 + " micro seconds");
        System.out.println();

		/*
		 * Ersetzen Sie das Konkatenieren der Strings durch den Einsatz des StringBuilders. 
		 * Messen Sie die Zeit für dieselbe Anzahl der Wiederholungen. 
		 * Falls keine Unterschiede bemerkbar sind, erhöhen Sie die Anzahl der Wiederholungen für beide Varianten der Lösung.
		 */
		
        final long start2 = System.nanoTime();
        
//        String[] list2 = new String[] {s1, s2, s3};
//
//        StringBuilder finalString2 = new StringBuilder("");
//        for (int i = 0; i < list2.length; i++) {
//			finalString2.append( list2[i] );
//		}
//        System.out.println( finalString2.toString() );
        
        StringBuilder finalString2 = new StringBuilder("");
        
        for (int i = 0; i < 10; i++) {
        	finalString2 = new StringBuilder("");
        	finalString = finalString2.append(s1).append(s2).append(s3).toString();
		}
        System.out.println(finalString);
        
        final long end2 = System.nanoTime();

        System.out.println("Took: " + (end2 - start2)/ 1_000 + " micro seconds");
	}

}
