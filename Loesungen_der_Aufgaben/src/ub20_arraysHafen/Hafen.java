package ub20_arraysHafen;

public class Hafen {
	
/*
 * Definieren Sie bitte die Klasse 'Hafen' so, dass in einen Hafen zwischen 0 bis 100 Schiffe einlaufen können.
 */
	static final int MAX_NUMBER_OF_SCHIFF = 100;
	
	Schiff[] liegeplatz = new Schiff[MAX_NUMBER_OF_SCHIFF];

	@Override
	public String toString() {

		int occupiedPlaces = 0;
		for (int i = 0; i < this.liegeplatz.length; i++) {
			if (this.liegeplatz[i] != null) {
				occupiedPlaces++;
			}
		}		
		return "Hafen (Schiffe: " + Integer.toString(occupiedPlaces) + 
				". Freie Plätze: " + Integer.toString(MAX_NUMBER_OF_SCHIFF-occupiedPlaces) +")";
		
	}
	
}
