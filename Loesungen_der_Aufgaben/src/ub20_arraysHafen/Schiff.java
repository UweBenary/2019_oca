package ub20_arraysHafen;

public class Schiff {
	
	final String NAME;
	
	/*
	 * Optional. 
	 * Definieren Sie bitte die Klasse Schiff so, dass jedes Schiff einen eigenen Namen erhalten kann (nicht muss).
	 * Bei der Ausgabe eines Schiffes mit der `System.out.println` soll der Name auf der Konsole ausgegeben werden.
	 */
	
	public Schiff(){
		this("");
	}
	
	public Schiff(String name) {
		this. NAME = name;
	}

	@Override
	public String toString() {
		if (this.NAME == "") {
			return "Das Schiff ist noch nicht getauft worden.";
		} else {
			return "Schiff: " + NAME;
		}
	}

	
}
