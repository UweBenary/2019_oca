package ub20_arraysHafen;

public class Solutions {

	public static void main(String[] args) {

		/*
		 * Lassen Sie bitte in einem neuen Hafen 12 Schiffe ankern
		 */
	
		Hafen hafen1 = new Hafen();
		
		for (int i = 1; i <= 12; i++) {
			hafen1.liegeplatz[i-1] = new Schiff();
		}	
		
		/*
		 * Geben Sie den Hafen mit der `System.out.println` aus:
		 * 		System.out.println(h); //h ist der Hafen
		 * Die Konsolenausgabe soll dann so aussehen:
		 * 	    Hafen (Schiffe: 12. Freie Plätze: 88)
		 */
		System.out.println(hafen1);
		
		/*
		 * Optional. 
		 * Definieren Sie bitte die Klasse Schiff so, dass jedes Schiff einen eigenen Namen erhalten kann (nicht muss).
		 * Bei der Ausgabe eines Schiffes mit der `System.out.println` soll der Name auf der Konsole ausgegeben werden.
		 */
		Schiff schiff1 = new Schiff();
		System.out.println();
		System.out.println(schiff1);
		
		Schiff schiff2 = new Schiff("Titanic");
		System.out.println();
		System.out.println(schiff2);
		
	}

	
}
