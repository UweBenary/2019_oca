package ub41_personenSortFormat;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Person
	implements Comparable<Person> {

	private String firstName;
	private String familyName;
	private Date dateOfBirth;
	private final DateFormat FORMATTER = new SimpleDateFormat( "dd.MM.yyyy" );
	
	Person(String firstName, String familyName, String dateOfBirth) {
		setFirstName(firstName);
		setFamilyName(familyName);
		setDateOfBirth(dateOfBirth);
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	
	public String getDateOfBirthAsString() {
		return FORMATTER.format( getDateOfBirth() );
	}

	public void setDateOfBirth(String dateOfBirth) {
		try {
			this.dateOfBirth = FORMATTER.parse( dateOfBirth );
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public String toString() {
		String dateStr = getDateOfBirth()!=null ? " (" + FORMATTER.format( getDateOfBirth() ) + ")" : "";
		return firstName + " " + familyName + dateStr;
	}

	@Override
	public int compareTo(Person p) {
		int result = getFamilyName().compareTo( p.getFamilyName() );
		
		if (result == 0) {
			result = getFirstName().compareTo( p.getFirstName() );
		}
		
		if (result == 0 & p.getDateOfBirth()==null ) {
//			System.out.println(this.toString() + " " + p.toString() + " 0");
			return 0;
		}
		
		if (result == 0 & getDateOfBirth()==null ) {
//			System.out.println(this.toString() + " " + p.toString() + " -1");
			return -1;
		}
		
		if (result == 0) {
			result = getDateOfBirth().compareTo( p.getDateOfBirth() );
		} 
			
		return result;
	}
}
