package ub41_personenSortFormat;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

public class Solution {

	private static class PersonComperator 
		implements Comparator<Person> {
		
		@Override
		public int compare(Person p1, Person p2) {
			return p2.compareTo(p1);
		}
	}	
	
	public static void main(String[] args) {			
			
				
		String neuerName = PersonUtils.generiereName(PersonUtils.createSyllablesArray(), 4);
		System.out.println(neuerName);
		System.out.println();
		
		/*
		 * A4.
		 */
		Person[] personen = new Person[10];
		Random rand = new Random();
		String name1, name2, date;
		
		for (int i = 0; i < personen.length; i++) {
			name1 = PersonUtils.generiereName(PersonUtils.createSyllablesArray(), rand.nextInt(5)+1);
			name2 = PersonUtils.generiereName(PersonUtils.createSyllablesArray(), rand.nextInt(5)+1);
			date = "" + ( rand.nextInt(28)+1 ) + "." + ( rand.nextInt(12)+1 ) + "." + ( rand.nextInt(2020-1900)+1900 );
			personen[i] = new Person(name1, name2, date);					
		}
		
		/*
		 * A3.
		 */
		PersonUtils.printPersonen(personen);
		
		/*
		 * A5.
		 */
		Arrays.sort(personen);
		PersonUtils.printPersonen(personen);
		
		/*
		 * A6.
		 */
		Comparator<Person> cmp = new PersonComperator();
//		Comparator<Person> cmp = Comparator.reverseOrder();
		Arrays.sort(personen, cmp);
		PersonUtils.printPersonen(personen);
	}

}
