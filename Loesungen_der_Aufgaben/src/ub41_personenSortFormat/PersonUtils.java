package ub41_personenSortFormat;

import java.util.Arrays;
import java.util.Random;
	
public class PersonUtils {
	/*
	 * A1.
	 */
	private final static String[] SYLLABLES = {"pro", "gi", "idre", "to", "get", "her", "la", "le", "lu", "ene", "mene", "muh", "su", "ola", "im", "se", "fo", "aa", "ner", "yd"};
			

	static String[] createSyllablesArray() {
		return Arrays.copyOf(SYLLABLES, SYLLABLES.length);
	}


	static String generiereName(String[] syllables, int mumberOfSyllables) {
		StringBuilder sb = new StringBuilder("");
		Random rand = new Random();
		for (int i = 0; i < mumberOfSyllables; i++) {
			sb.append( syllables[ rand.nextInt(syllables.length) ] );
		}
		String firstLetter = sb.charAt(0) + "";		
		return firstLetter.toUpperCase() + sb.substring(1);
	}
		
	static void printPersonen(Person[] personen) {
		System.out.println(" |  Nr. |   		Vorname |  		Nachname |  		Geburtsjahr |  ");
		for (int i = 0; i < personen.length; i++) {
			System.out.printf(" |  %3d | %21s | %22s | %24s |  %n", 
					i+1, personen[i].getFirstName(), personen[i].getFamilyName(), personen[i].getDateOfBirthAsString());
		}
		System.out.println();
	}
	
}
