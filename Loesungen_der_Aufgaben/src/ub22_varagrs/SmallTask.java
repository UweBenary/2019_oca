package ub22_varagrs;

public class SmallTask {

	public static void main(String[] args) {

		printArgs("Ich", "Du", "ErSieEs");
		printArgs("Wir");
//		printArgs(); // test for min 1 argument
		
		// open issue: nullPointException
//		printArgs("Ihr", null);
	}
	
	static void printArgs(String string, String... strings) {
		System.out.print(string);
		
		for (String s : strings) {
			System.out.print(", " + s);
		}
		
		System.out.println();
	}

}
