package ub30_kleineAufgabePolymorphie;

public class MultiplyZero extends IntOperation {

	@Override
	public int execute(int x) {
		int result = 0;
		if (next != null) {
			result = next.execute(result);
		}
		return result;
	}

}
