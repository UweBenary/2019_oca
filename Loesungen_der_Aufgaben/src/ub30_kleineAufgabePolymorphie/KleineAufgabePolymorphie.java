package ub30_kleineAufgabePolymorphie;

import java.util.Random;

public class KleineAufgabePolymorphie {

	public static void main(String[] args) {
		
		
		/*
		 * A1.
		 */
		
		IntOperation op1 = new AddOne(); // Eine Einz addieren
		IntOperation op2 = new Negate(); // Vorzeichen umkehren
		
		int x = 5;
		
		x = op1.execute(x);
		System.out.println("x = " + x); // 6
		
		x = op2.execute(x);
		System.out.println("x = " + x); // -6
		
		/*
		 * A2.
		 * 
		 * Erzeugen Sie ein Array mit mindestens 4 (vier) unterschiedlichen IntOperation-Arten.
		 * 
		 * Jagen Sie einen int-Wert durch das Array so, dass alle Operationen sequentiell ausgeführt werden
		 * und jede nächste Operation als Eingangswert (input) das Ergebnis der vorherigen Operation erhält
		 */
		
		IntOperation[] array = new IntOperation[10];
		for (int i = 0; i < array.length; i++) {
			array[i] = getRandomIntOperation();
		}
		
		int y = 5;
		System.out.println("Startwert: y = " + y);
		for (int i = 0; i < array.length; i++) {
			y = array[i].execute(y);
			System.out.println( i+1 + ". " + array[i].getClass().getSimpleName() + " : " + y);
		}
		
		
		/*
		 * A3.
		 * 
		 * Nach folgendem Muster sollen Int-Operationen kombiniert werden können
		 */
		
		IntOperation op = new AddOne()
								.andThen( new Negate() )
								.andThen( new MultiplyZero() )
								.andThen( new AddOne() );
		
		int result = op.execute(2);
		System.out.println( "result = " + result ); // -19
		
					
		
	} // end of main
	
	static IntOperation getRandomIntOperation() {
		Random rand = new Random();
		switch (rand.nextInt(4)) {
		case 0:
			return new AddOne();
		case 1:
			return new Negate();
		case 2:
			return new Identity();
		case 3:
			return new MultiplyZero();
		}
		return null;
	}

}
