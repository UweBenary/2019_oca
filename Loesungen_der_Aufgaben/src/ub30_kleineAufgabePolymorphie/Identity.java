package ub30_kleineAufgabePolymorphie;

public class Identity extends IntOperation {

	@Override
	public int execute(int x) {
		int result = x;
		if (next != null) {
			result = next.execute(result);
		}
		return result;
	}

}
