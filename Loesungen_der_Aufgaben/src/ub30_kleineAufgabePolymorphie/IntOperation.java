package ub30_kleineAufgabePolymorphie;

public abstract class IntOperation {

	public IntOperation next;
	
	public abstract int execute(int x);
	
	public IntOperation andThen(IntOperation io) {
		next = io;
		return next;
	}
}
