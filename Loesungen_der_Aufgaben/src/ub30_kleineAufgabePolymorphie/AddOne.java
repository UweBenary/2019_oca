package ub30_kleineAufgabePolymorphie;

public class AddOne extends IntOperation {

	
	@Override
	public int execute(int x) {
		int result = x+1;
		if (next != null) {
			result = next.execute(result);
		}
		return result;
	}
	
}
