package ub02_kleineAufgabeZuSwitch;

public class UB4_Switch {

	public static void main(String[] args) {

		int day = -10;

		switch(day) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			System.out.println("3. Arbeitstag");
			break;
		case 6:
		case 7:
			System.out.println("3. Wochenende");
			break;
		default:
			System.out.println("3. Ungültige Tag-Eingaben: " + day);	
		
		}
		
	}

}
