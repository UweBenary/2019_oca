package ub16_arraysEquals;

public class TestArrays {
	
	static final int NUMBER_OF_TESTS = 20000;
	
	/*
	 * Definieren Sie eine statische Methode 'boolean equalsMitWhile(int[], int[])  
	 * die die Inhalte zwei int-Arrays gleicher Länge vergleicht und 
	 * dabei mit der while-Schleife durch die Arrays läuft
	 */
	static boolean equalsMitWhile(int[] arr1, int[] arr2) {
		boolean result = true;

		if (arr1.length == arr2.length) {
			int idx = 0;
			while (idx < arr1.length && result) {
				result = arr1[idx] == arr2[idx];
				idx++;
			}			
			return result;
		
		} else {
			throw new IllegalArgumentException("Error: array must have same length!");
		}
	}
	
	/*
	 * Definieren Sie eine statische Methode 'boolean equalsMitFor(int[], int[])  
	 * die die Inhalte zwei int-Arrays gleicher Länge vergleicht und 
	 * dabei mit der for-Schleife durch die Arrays läuft
	 */
	static boolean equalsMitFor(int[] arr1, int[] arr2) {
		boolean result = true;

		if (arr1.length == arr2.length) {
			for (int i = 0; i < arr1.length; i++) {
				result = arr1[i] == arr2[i];
			}
			
			return result;
		
		} else {
			throw new IllegalArgumentException("Error: array must have same length!");
		}
	}

	public static void main(String[] args) {
		
		System.out.println("Running...");

		/*
		 * Rufen Sie für zwei int-Arrays die Methode 'equalsMitWhile' mehrmals auf. 
		 * Die Anzahl der Aufrufe ist mit `NUMBER_OF_TESTS` definiert. 
		 * Messen Sie die Gesamtzeit aller Aufrufe. 
		 * Am Ende berechnen Sie den Mittelwert (Durchschnitt) der Zeit eines einzelnen Aufrufes.
		 */
		int[] arr = new int[1_000_000];
	
		long start = System.currentTimeMillis(); // start
//		System.out.println("Start time: " + start);
		for (int i = 0; i < NUMBER_OF_TESTS ; i++) {
			equalsMitWhile( arr, arr );
		}
		long end = System.currentTimeMillis();   // end
//		System.out.println("Final time: " + end);
		
		System.out.println("Average execution time of while-loop (ms): " + (end - start)/(double) NUMBER_OF_TESTS);
		

		/*
		 *  Rufen Sie für dieselben zwei int-Arrays die Methode 'equalsMitFor' mehrmals auf. 
		 *  Die Anzahl der Aufrufe ist mit `NUMBER_OF_TESTS` definiert. 
		 *  Messen Sie die Gesamtzeit aller Aufrufe. 
		 *  Am Ende berechnen Sie den Mittelwert (Durchschnitt) der Zeit eines einzelnen Aufrufes.
		 */
		start = System.currentTimeMillis(); // start

		for (int i = 0; i < NUMBER_OF_TESTS ; i++) {
			equalsMitWhile( arr, arr );
		}
		
		end = System.currentTimeMillis();   // end
		
		System.out.println("Average execution time of for-loop (ms): " + (end - start)/(double) NUMBER_OF_TESTS);

		System.out.println("\nTests done.");
	} // end of main

}
