package ub46_kleineAufgabeInnereKlasse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


interface AffeFormat {
	String format();
}

class Affe {

	private class FormatImpl implements AffeFormat {

		@Override
		public String format() {
			return "Affe: " + name + ", Größe: " + groesse;
		}
		
	}
		
		static class CmpGroesse implements Comparator<Affe> {

			@Override
			public int compare(Affe o1, Affe o2) {
				return o1.groesse - o2.groesse;
			}
			
		}
		
		private String name;
		private int groesse;
		
		
		public Affe(String name, int groesse) {
			this.name = name;
			this.groesse = groesse;
		}


		@Override
		public String toString() {
			return "Affe " + name + " (" + groesse + ")";
		}


		public AffeFormat getFormatter() {
			return new FormatImpl();
		}

	}

public class Aufgabe {


	public static void main(String[] args) {

		List<Affe> affen = new ArrayList<>();
		
		affen.add( new Affe("King Kong", 120) );
		affen.add( new Affe("Cheetah", 30) );
		affen.add( new Affe("Bobby", 22) );
		affen.add( new Affe("Bobby", 100) );
		
		for (int i = 0; i < affen.size(); i++) {
			System.out.println(i+1 + ". " + affen.get(i).toString());
		}
		System.out.println();
		
		Affe.CmpGroesse cmp1 = new Affe.CmpGroesse();
		Collections.sort(affen, cmp1);
		
		for (int i = 0; i < affen.size(); i++) {
			System.out.println(i+1 + ". " + affen.get(i).toString());
		}
		System.out.println();
		
		for (int i = 0; i < affen.size(); i++) {
			Affe a = affen.get(i);
			AffeFormat fmt = a.getFormatter();
			System.out.println( fmt.format() );
			
		}
	}

}
