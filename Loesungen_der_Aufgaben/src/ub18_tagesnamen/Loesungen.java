package ub18_tagesnamen;

public class Loesungen {
	
	public static void main(String[] args) {
		
		String[] wochenTage = {"Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag"};
		String[] wochenTageKurz = {"Mo", "Di", "Mi", "Do", "Fr"};
		
		String[] wochenEnde = {"Samstag", "Sonntag"};
		String[] wochenEndeKurz = {"Sa", "So"};
			
		String[][] woche = { wochenTage, wochenTageKurz};
		String[][] we = { wochenEnde, wochenEndeKurz};
		
		for (int i = 0; i < woche.length; i++) {
			for (int j = 0; j < woche[i].length; j++) {
				System.out.print(woche[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		
		for (int i = 0; i < we.length; i++) {
			for (int j = 0; j < we[i].length; j++) {
				System.out.print(we[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		
		String[][] arr = { wochenTage, wochenTageKurz, wochenEnde, wochenEndeKurz};
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
		
	}

}
