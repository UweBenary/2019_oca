package ub54_dateAndTime;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.util.Locale;

public class Calendar {

	public static void main(String[] args) {


		printCalendar(4, 2010);
//		printCalendar(2, 2000);
		
	}

	private static void printCalendar(int month, int year) {
		
		LocalDate date = LocalDate.of(year, month, 1);
		System.out.println("---------------------");
		System.out.println(date.getMonth().getDisplayName(TextStyle.FULL, Locale.GERMANY) + " " + date.getYear() );		
		System.out.println("---------------------");
		for (int i = 1; i < date.lengthOfMonth()+1; i++) {
			date = LocalDate.of(year, month, i);
			System.out.printf(" %02d |   %-10s  |%n", i, date.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.GERMANY));
//			DateTimeFormatter fmt = DateTimeFormatter.ofPattern(" dd |   EEEE  |");
//			System.out.println( fmt.format( date ));
		}
		System.out.println("---------------------");
/*
 * 	---------------------
		April 2010
		---------------------
		 01 |   Donnerstag  |
		 02 |   Freitag     |
		 03 |   Samstag     |
		...
		 20 |   Dienstag    |
		 21 |   Mittwoch    |
		...
		 30 |   Freitag     |
		---------------------
 */
		//Klassen `java.time.YearMonth` und `java.time.format.DateTimeFormatter`
		// if using DateTimeFormatter usage of getDisplayName() is unnecessary  
		
	}

}
