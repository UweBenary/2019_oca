package ub39_stringSilben;

import java.util.Random;

public class StringOfRandomSyllables {
	/*
	 * - Definieren Sie ein Array mit 10-20 unterschiedlichen Silben ihrer Wahl. Z.B.: "pro", "gi", "idre" ... 
	 * - Definieren Sie eine Methode 'generiere(String[], int)', die man so verwenden kann:
	 *       String neuerName = generiere(arrayMitSilben, 4);
	 *   Die Methode erhält das Array mit Silben und die gewünschte Anzahl der Silben und generiert einen neuen Namen. 
	 *   - Generieren Sie 50 neue Namen und geben Sie sie aus
	 */
	
	public static void main(String[] args) {
		/*
		 * - Definieren Sie ein Array mit 10-20 unterschiedlichen Silben ihrer Wahl. Z.B.: "pro", "gi", "idre" ... 
		 */
		String[] syllables = {"pro", "gi", "idre", "to", "get", "her", "la", "le", "lu", "ene", "mene", "muh"};
		
		/*
		 * - Definieren Sie eine Methode 'generiere(String[], int)', die man so verwenden kann:
		 *       String neuerName = generiere(arrayMitSilben, 4);
		 *   Die Methode erhält das Array mit Silben und die gewünschte Anzahl der Silben und generiert einen neuen Namen. 
		 */
		String neuerName = generiere(syllables, 4);
		System.out.println( neuerName );
		
		
		/*
		 *   - Generieren Sie 50 neue Namen und geben Sie sie aus
		 */
		for (int i = 0; i < 50; i++) {
			neuerName = generiere(syllables, 4);
			System.out.println( i+1 + ". " + neuerName );
		}
		
	}
	
	/*
	 * - Definieren Sie eine Methode 'generiere(String[], int)', die man so verwenden kann:
	 *       String neuerName = generiere(arrayMitSilben, 4);
	 *   Die Methode erhält das Array mit Silben und die gewünschte Anzahl der Silben und generiert einen neuen Namen. 
	 */
	static String generiere(String[] arrayOfSyllables, int mumberOfSyllables) {
		StringBuilder sb = new StringBuilder("");
		Random rand = new Random();
		for (int i = 0; i < mumberOfSyllables; i++) {
			sb.append( arrayOfSyllables[ rand.nextInt(arrayOfSyllables.length) ] );
		}
		return sb.toString();
	}

}
