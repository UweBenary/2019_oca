package ub03_switchIfAmpelfarben;

import java.util.Scanner;

public class Loesungen {

	public static void main(String[] args) {
		
		System.out.println(" Welche Farbe hat die Ampel (Rot/Gelb/Grün)? ");
		
		/*
		 * Get color from user
		 */
		Scanner scanner = new Scanner( System.in );
		String farbe =  scanner.nextLine();		
		System.out.println(""); //newline for visual reasons
		
		/*
		 * execute first subtask: switch
		 */
		boolean flag = ampelfarbeMitSwitch( farbe ); // returns true if input correct
		
		/*
		 * execute 2nd subtask: if-else
		 * 		but only if 1st task doe not throw error (flag) 
		 */
		if(flag) {			
			System.out.println("\n... immer noch ..."); // for visual reasons
			ampelfarbeMitIfElse( farbe );
		}
		
	}

	
	static boolean ampelfarbeMitSwitch(String farbe) {
		
		boolean flag = true;
		
		switch (farbe) {
		
		case "Rot":
			System.out.println("Rot. Bitte warten");
			break;
			
		case "Gelb":
			System.out.println("Gelb. Gleich get es los");
			break;
		
		case "Grün":
			System.out.println("Grün. Weg frei");
			break;

		default:
			System.out.println("Fehler! Diese Farbe gibt es nicht.");
			flag = false;
			break;
		}	
		
		return flag;
	}


	static void ampelfarbeMitIfElse(String farbe) {
	
		if (farbe.contentEquals( "Rot")) {
			System.out.println("Rot. Bitte warten");
	
		} else if (farbe.contentEquals( "Gelb" )) {
			System.out.println("Gelb. Gleich get es los");

		} else if (farbe.contentEquals( "Grün" )) {	
			System.out.println("Grün. Weg frei");

		} else { //default
			System.out.println("Fehler! Diese Farbe gibt es nicht.");
		}	
	}

}
