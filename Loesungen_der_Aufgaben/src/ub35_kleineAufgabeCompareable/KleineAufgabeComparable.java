package ub35_kleineAufgabeCompareable;

import java.util.Arrays;

public class KleineAufgabeComparable {

	public static void main(String[] args) {
		
		Kreis[] kreise = new Kreis[10];
		
		for (int i = 0; i < kreise.length; i++) {
			kreise[i] = new RandomKreis();
			kreise[i].setName( "Kreis" + Integer.toString(i + 1) );
		}
		
		System.out.println( Arrays.toString(kreise) );
		
		Arrays.sort(kreise);
		
		System.out.println( Arrays.toString(kreise) );
		
	
	}
}
