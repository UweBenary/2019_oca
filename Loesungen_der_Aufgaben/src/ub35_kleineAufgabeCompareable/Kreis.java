package ub35_kleineAufgabeCompareable;

public class Kreis 
	implements Comparable<Kreis> {
	
	private int radius;
	private String name;

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Kreis k) {
		return this.getRadius() - k.getRadius();
	}	

	@Override
	public String toString() {
		return "" + this.getClass().getSimpleName() + " " + this.getName() + " mit Radius " + this.getRadius();
	}
}
