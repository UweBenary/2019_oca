package ub35_kleineAufgabeCompareable;

public class Auto 
	implements Comparable<Auto> {

	private int baujahr;
	private String hersteller;

	public Auto(int i) {
		setBaujahr(i);
	}

	public int getBaujahr() {
		return baujahr;
	}
	
	public String getHersteller() {
		return hersteller;
	}

	public void setHersteller(String hersteller) {
		this.hersteller = hersteller;
	}

	public void setBaujahr(int baujahr) {
		this.baujahr = baujahr;
	}

	@Override
	public int compareTo(Auto a) {
		if ( getHersteller() == a.getHersteller() ) {
			return getBaujahr() - a.getBaujahr();
		}
		return getHersteller().compareTo( a.getHersteller() );
	}
	
	@Override
	public String toString() {
		return "Auto " +getHersteller() + " " + getBaujahr();
	}
	
}
