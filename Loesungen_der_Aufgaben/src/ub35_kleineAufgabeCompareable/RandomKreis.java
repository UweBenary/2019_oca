package ub35_kleineAufgabeCompareable;

import java.util.Random;

public class RandomKreis extends Kreis {

	public RandomKreis() {
		Random rand = new Random();
		setRadius( rand.nextInt(6) );
	}
}

