package ub35_kleineAufgabeCompareable;

import java.util.Arrays;
import java.util.Random;

public class AutosSortieren {

	public static void main(String[] args) {

		Auto[] autos = new Auto[6];
		
		Random rand = new Random();
		String[] hersteller = new String[] {"Opel", "Audi", "Ford"};
		
		for (int i = 0; i < autos.length; i++) {
			autos[i] = new Auto(rand.nextInt(2020 - 1900) + 1900);
			autos[i].setHersteller( hersteller[rand.nextInt(hersteller.length)] );
		}
		
		Arrays.sort(autos);
		
		System.out.println( Arrays.toString(autos) );

		
	}

}
