package ub01_Kontrollstrukturen_for;

public class Zahlenreihen {

	public static void main(String[] args) {
		
		zahlenreiheEinsBisZehn();
		
		zahlenreiheEinsBisZehnOhneZwei();
		
		zahlenreiheZweierschritte();
		
		kleinbuchstabenreihe();
		
		grossbuchstabenreiheRueckwaerts();
		
		zahlenreiheEinsBisHundertModuloFuenf();
		
		optional();

	}

	
	static void zahlenreiheEinsBisZehn() {

		System.out.println("Aufgabe 1:");
		
		for( int i=0 ; i<10 ; i++) {
			
			if(i<9) {
				System.out.print(" " + i);
			} else {
				System.out.println(" " + i);
			}
			
		}
	}

	
	static void zahlenreiheEinsBisZehnOhneZwei() {
		
		System.out.println("\nAufgabe 2:");
		
		for( int i=0 ; i<10 ; i++) {
			
			if( i<2 && i>0 ) {
				continue ;
			}
			
			if(i<9) {
				System.out.print(" " + i);
			} else {
				System.out.println(" " + i);
			}
			
		}
	}
	
	
	static void zahlenreiheZweierschritte() {
		
		System.out.println("\nAufgabe 3:");
		
		for( int i=-4 ; i<51 ; i+=2) {
			
			if(i<50) {
				System.out.print(" " + i);
			} else {
				System.out.println(" " + i);
			}
			
		}
		
		System.out.println("   Alternative:");
		
		for( int i=-4 ; i<51 ; i+=2) {
			
			switch (i) { // if you listen carefully you can hear this code suck. // I wanted to experiment with switch/case
			case -4:
			case -2:
			case 0:
			case 2:
			case 4:
				System.out.print(" " + i);
				break;
			case 6:
				System.out.print(" ...");
				break;
			case 50:
				System.out.println(" " + i);
				break;	
			default:
				break;
			}			
		}
		
	}

	
	static void kleinbuchstabenreihe() {

		System.out.println("\nAufgabe 4:");
		
		for( char i=97 ; i<97+26 ; i++) {
			
			if(i<97+26-1) {
				System.out.print(" " + i);
			} else {
				System.out.println(" " + i);
			}
			
		}
	}
	
	
	static void grossbuchstabenreiheRueckwaerts() {

		System.out.println("\nAufgabe 5:");
		
		for( char i=90 ; i>90-26 ; i--) {
			
			if(i>90-26+1) {
				System.out.print(" " + i);
			} else {
				System.out.println(" " + i);
			}
			
		}
	}
	
	
	static void zahlenreiheEinsBisHundertModuloFuenf() {

		System.out.println("\nAufgabe 6:");
		
		for( int i=1 ; i<101 ; i++) {
			
			if(i%5 != 0) {
				continue;
			} else if(101-i > 5) {
					System.out.print(" " + i);
			} else {
					System.out.println(" " + i);
			}
									
		}
		
	}


	static void optional() {

		System.out.println("\nOptionale Aufgabe :  ");
		
		for( int i=3 ; i>0 ; i--) {
			
			System.out.print(" " + i);
			
			for( int j =1 ; j<=i ; j++) {
				
				if( i>1 || j>1 ) {
					System.out.print(" " + j);
				
				} else {
					System.out.println(" " + j);
				}
									
			}
		}
	}

}
