package ub15_arrays;

import java.util.Random;

public class ArrayEinfach {
	
	public static void main(String[] args) {
		
		/*
		 * A1: 
		 * Die neue Methode soll aus der main-Methode folgendermassen aufgerufen werden koennen:
		 * 	    	int[] arr = createArray(2, 15, 30);
		 * 	> In diesem Beispiel wird ein Array der Laenge 30 erzeugt und 
		 *    mit den Zufallswerten aus dem Bereich [2 .. 15] belegt.
		 */
		System.out.println("\nA1:");
		int[] arr = createArray(2, 15, 30);
		
		/*
		 * A2:
		 * Bitte geben Sie das erzeugte Array aus. 
		 * Dafür definieren Sie eine weitere Methode `printArray`, an die Sie das Array übergeben.
		 */
		System.out.println("\nA2:");
		printArray(arr);
		
		/*
		 * A3: Definieren Sie eine statische Methode `deutscheUmlaute`,
		 * die ein Array mit den Zeichen 'ä', 'ö' und 'ü' zurück liefert.
		 */
		System.out.println("\nA3:");
		printArray(deutscheUmlaute());
		
		/*
		 * A4: 
		 * Definieren Sie eine statische Methode `deutscheBuchstaben`, 
		 * die ein Array mit allen deutschen Kleinbuchstaben inklusive Umlaute und 'ß' zurück liefert.
		 */
		System.out.println("\nA4:");
		printArray(deutscheBuchstaben());
		
	} // end of main

	
	
	/*
	 * A1: 
	 * Bitte definieren Sie eine Methode 'createArray', die ein int-Array erzeugt und 
	 * mit Zufallswerten belegt. 
	 * Die neue Methode soll aus der main-Methode folgendermassen aufgerufen werden koennen:
	 * 	    	int[] arr = createArray(2, 15, 30);
	 * 	> In diesem Beispiel wird ein Array der Laenge 30 erzeugt und 
	 *    mit den Zufallswerten aus dem Bereich [2 .. 15] belegt.
	 */
	
	static int[]  createArray(int from, int to, int length) {
		int[] arr = new int[length];
		Random rand = new java.util.Random(); 

		for (int i = 0; i < arr.length; i++) {
			arr[i] = rand.nextInt( (to+1)-from ) + from;
		}

		return arr;
	}
	
	/*
	 * A2:
	 * Bitte geben Sie das erzeugte Array aus. 
	 * Dafür definieren Sie eine weitere Methode `printArray`, an die Sie das Array übergeben.
	 */
	static void printArray(int[] arr) {
		for (int i : arr) {
			System.out.print(i + " ");
		}
		System.out.println();
	}
	
	/*
	 * A3: Definieren Sie eine statische Methode `deutscheUmlaute`,
	 * die ein Array mit den Zeichen 'ä', 'ö' und 'ü' zurück liefert.
	 */
	static String[] deutscheUmlaute() {
		return new String[] {"ä", "ö", "ü"};
	}
	
	//@Overloaded
	static void printArray(String[] arr) {
		for (String i : arr) {
			System.out.print(i + " ");
		}
		System.out.println();
	}
	
	/*
	 * A4: 
	 * Definieren Sie eine statische Methode `deutscheBuchstaben`, 
	 * die ein Array mit allen deutschen Kleinbuchstaben inklusive Umlaute und 'ß' zurück liefert.
	 */
	static char[] deutscheBuchstaben() {
		char[] arr = new char[26+4];
		for (char i = 0; i <= arr.length-4; i++) {
			arr[i] = (char) (97 + i) ;
			}
		arr[26] = 'ä';
		arr[27] = 'ö';
		arr[28] = 'ü';
		arr[29] = 'ß';
		
		return arr;
	}
	
	//@Overloaded
	static void printArray(char[] arr) {
		for (char i : arr) {
			System.out.print(i + " ");
		}
		System.out.println();
	}

}
