package ub53_germanCities;

public class City {

	private String name;
	private int inhabitants;
	private String state;
	
	public City(String name) {
		super();
		this.name = name;
	}

	public int getInhabitants() {
		return inhabitants;
	}

	public void setInhabitants(int inhabitants) {
		this.inhabitants = inhabitants;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}
	
	@Override
	public String toString() {
		return name + " (" + state + ") : " + inhabitants;
	}

}

