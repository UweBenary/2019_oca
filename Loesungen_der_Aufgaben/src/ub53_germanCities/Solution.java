package ub53_germanCities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UncheckedIOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Solution {

	public static void main(String[] args) {

		/*
		 * A2
		 */
		String html = "https://de.wikipedia.org/wiki/Liste_der_Gro%C3%9Fst%C3%A4dte_in_Deutschland";
		String pageContent = getPageContent(html);
		
		
		/*
		 * A3
		 */
		
		List<City> cities = parsePageContentForCities(html);
		
		
		for (City city : cities) {
			System.out.println(	city );
		}

		
	}

	static List<City> parsePageContentForCities(String html) {
		
		String pageContent = getPageContent(html);
				
		Document doc = Jsoup.parse(pageContent);
		
		Elements tables = doc.getElementsByTag("table");
		Element table1 = tables.get(0);
		Elements table1Entries = table1.getElementsByTag("tr");
		Elements cityEntries = new Elements();
		for (int i = 0 +2; i < table1Entries.size() -2; i++) { // w/o header and tail
			cityEntries.add( table1Entries.get(i) );
		}
	
		List<City> cities = new ArrayList<City>();
		
		for (Element element : cityEntries) {
			String name = element.getElementsByTag("td").get(1).selectFirst("a").ownText();
			City city = new City(name);
			city.setInhabitants( Integer.valueOf( element.getElementsByTag("td").get(11).ownText().replace(".", "") ) );
			city.setState( element.getElementsByTag("td").get(16).selectFirst("a").attributes().get("title") );
			cities.add(city);
			}
		
		return cities;
	}

	static String getPageContent(String html) {
		
		java.net.URL url = null;
		try {
			url = new java.net.URL( html );
		} catch (MalformedURLException e1) {
			System.out.println("URL falsch gebildet");
			e1.printStackTrace();
		}   
		
		InputStream is = null;
		try {
			is = url.openStream();
		} catch (IOException e1) {
			System.out.println("Kein InputStream");
			e1.printStackTrace();
		}   
		
		StringBuilder sb = new StringBuilder();
		Reader r = new InputStreamReader(is);   
		try(BufferedReader in = new BufferedReader( r )) {
			
			String line;
			while( (line = in.readLine()) != null ) {
				sb.append(line).append("\n");
			}
			
		} catch (IOException e) {
			throw new UncheckedIOException("Kann die Quelle nicht lesen", e);
		}
		
		return sb.toString();
	}

}
