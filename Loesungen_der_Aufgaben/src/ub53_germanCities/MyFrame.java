package ub53_germanCities;


import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class MyFrame extends JFrame {

	
	private JLabel label = new JLabel("");
	
	private JButton button = new JButton("Laden");
	private JButton button2 = new JButton("Parsen");
	
	private String html = "https://de.wikipedia.org/wiki/Liste_der_Gro%C3%9Fst%C3%A4dte_in_Deutschland";
	

	private class OnClickListener implements ActionListener {
//		private int clicksCount;
		
		@Override
		public void actionPerformed(ActionEvent e) {
//			System.out.println("click");
//			
			String text = "Laden... " ;
			label.setText(text);
//			
//			button.setText("Click (" + clicksCount + ")");
			
			String pageContent = Solution.getPageContent(html);
			System.out.println( pageContent );

		}
		
	}
		private class OnClickListener2 implements ActionListener {
		
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String text = "Parsen... " ;
				label.setText(text);
				
				List<City> cities = Solution.parsePageContentForCities(html);
			
				for (City city : cities) {
					System.out.println(	city );
				}

			}
	}
	
	public MyFrame() {
		super("Clicks Counter");
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	
		/*
		 * Anfangsposition
		 */
		int x = 10; // Negativ, damit die Test-Starts auf dem Dozentenrechner auf dem sekundären Monitor das Fenster zeigen
		int y = 30;
		
		setLocation(x, y);
		
		/*
		 * Maße
		 */
		int width = 300;
		int height = 200;
		setSize(width, height);
		
		/*
		 * Aktueller Layout-Manager: BorderLayout, der 5 Bereiche im Fenster definiert
		 */
		
		/*
		 * Label im Norden:
		 */
		add(label, BorderLayout.NORTH);
		
		/*
		 * Schaltfläche bilden und platzieren
		 */
		add(button, BorderLayout.EAST);

		add(button2, BorderLayout.WEST);
		/*
		 * Den Listener für Click-Events bilden
		 * und bei dem JButton registrieren.
		 */
		ActionListener onClick = new OnClickListener();
		button.addActionListener(onClick);
		
		ActionListener onClick2 = new OnClickListener2();
		button2.addActionListener(onClick2);
		
	}

	public static void main(String[] args) {

		JFrame frame = new MyFrame();
		
		frame.setVisible(true);
		
		System.out.println("end of main");
	}

}
