package ub29_polymorphieGeometrie;

import java.util.Random;

public class RandomCircle extends Circle {
	
	private Random rand = new Random();
	private int r1 = rand.nextInt(Shape.MAX_SIZE - Shape.MIN_SIZE) + Shape.MIN_SIZE;

	public RandomCircle() {
		setRadius(r1);
	}
	
	
}
