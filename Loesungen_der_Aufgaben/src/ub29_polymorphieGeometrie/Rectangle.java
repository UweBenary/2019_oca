package ub29_polymorphieGeometrie;

public class Rectangle extends Shape {

	private int width, hight;
	
	
	Rectangle() {	
		this(0,0);
	}
	
	Rectangle(int width, int hight) {
		setWidth(width);
		setHight(hight);
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHight() {
		return hight;
	}

	public void setHight(int hight) {
		this.hight = hight;
	}
	
	@Override
	public String toString() {
		return "Rectangle of " + getWidth() + "x" + getHight() + " at " + super.toString();
	}
	
	public double getArea() {
		return getHight() * getWidth();
	}
	
	/*
	 * UnitTests
	 */
	public static void main(String[] args) {
		
		Rectangle r1 = new Rectangle(1,2);
		System.out.println(r1.toString());
		
		r1.move(2, 1);
		System.out.println(r1.toString());
		
		System.out.println("Area: " + r1.getArea());
	}
}
