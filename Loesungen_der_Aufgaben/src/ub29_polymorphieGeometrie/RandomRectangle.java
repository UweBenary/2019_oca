package ub29_polymorphieGeometrie;

import java.util.Random;

public class RandomRectangle extends Rectangle {

	private Random rand = new Random();
	private int r1 = rand.nextInt(Shape.MAX_SIZE - Shape.MIN_SIZE) + Shape.MIN_SIZE;
	private int r2 = rand.nextInt(Shape.MAX_SIZE - Shape.MIN_SIZE) + Shape.MIN_SIZE;
	
	RandomRectangle() {
		setWidth(r1);
		setHight(r2);
	}	
}
