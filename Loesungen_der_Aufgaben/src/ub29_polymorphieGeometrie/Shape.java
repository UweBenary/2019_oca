package ub29_polymorphieGeometrie;

import java.awt.Point;

public class Shape {

	private Point coordinates;
	final static int MIN_SIZE = 1;	
	final static int MAX_SIZE = 6;
	
	public Shape() {
		setCoordinates(new Point(0, 0));
	}

	public Point getCoordinates() {
		return coordinates;
	}

	public void setCoordinates(Point coordinates) {
		this.coordinates = coordinates;
	}
			
	@Override
	public String toString() {
		return "position [" + coordinates.getX() + "," + coordinates.getY() + "]";
	}

	public void move(int x, int y) {
		setCoordinates(new Point(x, y));
	}
	
	
	/*
	 * UnitTests
	 */
	public static void main(String[] args) {
		
		Shape s = new Shape();
		System.out.println(s.toString());
		
		s.setCoordinates(new Point(2, 3));
		System.out.println(s.toString());
		
		s.move(0, 0);
		System.out.println(s.toString());
	}

	double getArea() {
		System.out.println("** getArea() has not yet been implemented! **");
		return 0;
	}
}

