package ub29_polymorphieGeometrie;

import java.util.Random;

public class Solution {

	public static void main(String[] args) {

		
		Shape[] shapes = new Shape[100];
		Random rand = new Random();
		
		for (int i = 0; i < shapes.length; i++) {
			shapes[i] = rand.nextBoolean() ? new RandomRectangle() : new RandomCircle();
		}
		
		double[] areaArray = applyGetArea(shapes);
		
		for (int i = 0; i<shapes.length; i++) {
			System.out.printf("%3d. %s has an area of %.2f.%n", i+1, shapes[i].toString(), areaArray[i]);
		}
	}

	static double[] applyGetArea(Shape[] shapes) {
	
		double[] result = new double[shapes.length];
		
		for (int i = 0; i<shapes.length; i++) {
			result[i] = shapes[i].getArea();
		}

		return result;
	}
}
