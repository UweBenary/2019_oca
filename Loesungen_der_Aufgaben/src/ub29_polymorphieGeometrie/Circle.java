package ub29_polymorphieGeometrie;

public class Circle extends Shape {

	private int radius;
	
	public Circle() {
		this(0);		
	}
	
	Circle(int radius) {
		setRadius(radius);
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	@Override
	public String toString() {
		return "Circle of radius " + getRadius() + " at " + super.toString();
	}	
	
	public double getArea() {
		return Math.PI * getRadius() * getRadius();
	}
	
	/*
	 * UnitTests
	 */
	public static void main(String[] args) {
		
		Circle c1 = new Circle(1);
		System.out.println(c1.toString());
		
		c1.move(2, 1);
		System.out.println(c1.toString());
		
		System.out.println("Area: " + c1.getArea());
	}
}
