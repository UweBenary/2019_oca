package ub06_kleineAufgabeZurPause;

public class Parameter {


		/*
		 * kleine Aufgabe:
		 * 
		 * Verbessern Sie bitte die Methode addAndPrint so, dass sie keine Summe berechnet, sondern eine Fehlermeldung
		 * ausgibt, falls das Addieren der Parameter int-Bereich sprengt.
		 */
		
//		static void addAndPrint(int a, int b) {
//			
//			int sum = a + b;
//			
//			System.out.println(a + " + " + b + " = " + sum);
//		}

		static void addAndPrint( long a, long b ) {		
		
			if (a<Integer.MIN_VALUE || a>Integer.MAX_VALUE || b<Integer.MIN_VALUE || b>Integer.MAX_VALUE) {
				System.err.println(("Mindestens ein Summand liegt außerhalb des int-Bereiches."));
			
			} else {
					long sum = a + b;
					if (sum<Integer.MIN_VALUE || sum>Integer.MAX_VALUE ) {
						System.err.println(("Summe liegt außerhalb des int-Bereiches."));
						
					} else {
						System.out.println((a + " + " + b + " = " + sum));
					}
			}
		}
		
	public static void main(String[] args) {

		
		addAndPrint(2, 4); // geht
		
		System.out.println("--1--");
		
//		addAndPrint(Integer.MAX_VALUE -2, 5); // geht, wirft gewollten Fehler
		
		System.out.println("--2--");
		
		addAndPrint(Integer.MAX_VALUE +2, 5);// wird nicht mit Long aufgerufen...

		System.out.println("--3--");
		
//		addAndPrint(Integer.MAX_VALUE +2L, 5);// geht, wirft gewollten Fehler

		System.out.println("--4--");
		
		addAndPrint(Integer.MAX_VALUE +0L, 5);// geht, wirft gewollten Fehler
		
		System.out.println("--5--");
		
//		addAndPrint( Integer.MAX_VALUE , 5);// geht, wirft gewollten Fehler
		
	}

}
