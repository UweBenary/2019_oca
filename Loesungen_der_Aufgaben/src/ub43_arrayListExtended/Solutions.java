package ub43_arrayListExtended;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class Solutions {
	
	public static void main(String[] args) {
		
		int exercise = 0;
		
		/*
		 * A1. Erzeugen Sie eine `ArrayList` mit folgenden Strings: "mo", "di", "mi", "o", "fr". 
		 * Verwenden Sie einen `Iterator` um die Elemente der Liste mit der while-Schleife auszugeben.
		 */
		System.out.println("\n*** Exercise " + ++exercise);

		List<String> strList = new ArrayList<String>();
		strList.add("mo");
		strList.add("di");
		strList.add("mi");
		strList.add("o");
		strList.add("fr");
		
		Iterable<String> iterable = strList;
		Iterator<String> it = iterable.iterator();
		
		while (it.hasNext()) {
			String s = it.next();
			System.out.println(s);
		}
		
		/*
		 * A2. Erzeugen Sie eine `ArrayList` mit 10 zufälligen Integer aus dem Bereich [0 ... 50]. 
		 * Entfernen Sie mit `removeIf` die ungeraden Werte.
		 */
		System.out.println("\n*** Exercise " + ++exercise);
		
		List<Integer> intList = new ArrayList<Integer>();
		Random rand = new Random();
		for (int i = 0; i < 10; i++) {
			intList.add( rand.nextInt(51) );
		}
		System.out.println(intList);
		intList.removeIf(n -> (n % 2 == 1));
		System.out.println(intList);
	
	
		/*
		 * A3. Optional
		 * Erzeugen Sie eine `ArrayList` mit 10 zufälligen Integer aus dem Bereich [0 ... 50]. 
		 * Ersetzen Sie mit der Methode `replaceAll` alle ungeraden Werte durch 0.
		 */
		System.out.println("\n*** Exercise " + ++exercise);
		
		intList.clear();
		for (int i = 0; i < 10; i++) {
			intList.add( rand.nextInt(51) );
		}
		System.out.println(intList);
		intList.replaceAll(n -> (n % 2 == 1) ? 0 : n);
		System.out.println(intList);
		
		/*
		 * A4. Erzeugen Sie eine `ArrayList` mit 10 zufälligen Integer aus dem Bereich [0 ... 50]. 
		 * Verwenden Sie eine Methode `toArray` um ein Array mit den Elementen der Liste zu erhalten.
		 */
		System.out.println("\n*** Exercise " + ++exercise);
		
		intList.clear();
		for (int i = 0; i < 10; i++) {
			intList.add( rand.nextInt(51) );
		}
		System.out.println(intList);
		Integer[] arr = intList.toArray(new Integer[0]);
		System.out.println( arr.getClass().getName() );
		System.out.println( Arrays.toString(arr) );
		
		/*
		 * A5. Optional 
		 * Erzeugen Sie ein Integer-Array. 
		 * Überlegen Sie sich mindestens zwei Möglichkteiten, eine ArrayList mit den Elementen des Arrays zu erstellen. 
		 */
		System.out.println("\n*** Exercise " + ++exercise);
		
		Integer[] arr2 = new Integer[] { 1, 2, 3, 4,};
		
		intList.clear();
		intList = new ArrayList<Integer>( );
		for (Integer integer : arr2) {
			intList.add(integer);			
		}
		System.out.println( intList.getClass().getName() );
		System.out.println(intList);
		
		List<Integer> intList2 = Arrays.asList( arr2 );
		System.out.println( intList2.getClass().getName() );
		System.out.println(intList2);
		
	}
}
