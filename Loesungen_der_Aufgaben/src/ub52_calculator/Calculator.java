package ub52_calculator;

import java.util.Scanner;


public class Calculator {

	public static void main(String[] args) {
		
		double result = 0;
		
		while (true) {
						
			String[] input = getInput();

			if (input[0].equals("q") || input[0].equals("Q")) {
				System.out.println("quit");
				System.exit(0);
			}
			
			double a, b;
			String operant;
			
			if ( canParseDouble(input[0]) ) {
				a = Double.parseDouble( input[0] );
				operant = input[1];
				b = Double.parseDouble( input[2] );
				 
			} else {
				a = result;
				operant = input[0];
				b = Double.parseDouble( input[1] );
			}
			
			switch (operant) {
			case "+":
				result = a + b;
				break;
			case "-":
				result = a - b;
				break;
			case "*":
				result = a * b;
				break;
			case "/":
				result = a / b;
				break;
			default:
				break;
			}
			
			System.out.println(a + " " + operant + " " + b + " = " + result);

		}
	}

	private static boolean canParseDouble(String string) {
		try {
			Double.parseDouble( string ); 
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private static String[]  getInput() {
		
		System.out.println("Use spaces to separate operants and numbers.");
		
		String[] stringList;
		boolean noValidInput = true;
		
		do {
			stringList =  new Scanner( System.in ).nextLine().trim().split(" ");
		
			if (stringList.length == 0 
					|| stringList.length > 3 
//					|| ! canParseDouble(stringList[0])
//					|| ! (stringList[0].length() == 1 && "qQ-+* / ".contains( "" + stringList[0].charAt(0) ) ) 
					) {
				System.out.println("Illegal arguments!");
				noValidInput = true;
			
			} else {
				noValidInput = false;
			}
			
			
		} while (noValidInput);
		
		return stringList;

	}


}
