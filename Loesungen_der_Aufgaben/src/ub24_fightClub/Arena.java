package ub24_fightClub;

import java.util.Random;

public class Arena {

	final private int maxNumberOfHeroes = 8;
	private Hero[] heroes;
	private Hero lastChosen;
	
	public Arena() {
		heroes = new Hero[maxNumberOfHeroes];
	}
	
	
	public Hero chooseHero() {
		Random rand = new Random();
		Hero h;
		do {
			h = heroes[rand.nextInt(maxNumberOfHeroes)];			
		} while (h==lastChosen || h==null); 	// implementation sucks! discard null from heroes! private void trim(){} 
		lastChosen = h;
		return h;	
	}

	public Hero getWinner(Hero figther1, Hero figther2) {

		System.out.printf("\n%s is fighting %s...", figther1.getName(), figther2.getName() );

		while (!figther1.isKO() || !figther2.isKO()) {
			figther1.hit();
			figther2.hit();			
		}		
		if( figther1.isKO() ) {
			System.out.printf("%s is KO. The winner is %s.\n", figther1.getName(), figther2.getName() );
			exciles(figther1); 
			return figther2;
		} else { 
			System.out.printf("%s is KO. The winner is %s.\n", figther2.getName(), figther1.getName() );
			exciles(figther2); 
			return figther1;}	
	}
	
	
	public boolean invites(Hero hero) {		
		for (int i = 0; i < heroes.length; i++) {
			if(heroes[i] == null) {
				heroes[i] = hero;
				return true;
			}
		}		
		return false;
	}
	
	
	public void exciles(Hero hero) {		
		for (int i = 0; i < heroes.length; i++) {
			if(heroes[i] == hero) {
				heroes[i] = null;
				System.out.println(hero.getName() + " is leaving the arena." );
				break;
			}
		}		
	}
	
	
	public boolean isLastAlive() {
		int countNulls = 0;
		for (int i = 0; i < heroes.length; i++) {
			if(heroes[i] == null) {
				countNulls++;
			}
		}
		return countNulls == heroes.length-1 ? true : false; 
	}
	
	
	void theGathering() {
		Hero figther1 = chooseHero();
		while(!isLastAlive() ) {
			Hero figther2 = chooseHero();
			Hero winner = getWinner(figther1, figther2);
			figther1 = winner;
		}
		System.out.println();
		System.out.println(figther1.getName() + " is the greatest!");
	}
	
	
	public static void main(String[] args) {
		
		//dreate arena
		Arena arena = new Arena();
		
		//invite 8 heros into arena 
		String[] heros = new String[] { "Batman", "Achill", "Wonder Woman", "Herakles", "Mini Mouse", "Oedipus", "The Hulk", "Popeye", "Superman"};
		for (String s : heros) {
			boolean isInvited = arena.invites(new Hero(s));
			if (!isInvited) {
				System.out.println("Arena is full! Let the games begin...");
				break;
			}
		}
		
		arena.theGathering();
	}
}
