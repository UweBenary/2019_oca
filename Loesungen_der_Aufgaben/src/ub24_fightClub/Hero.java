package ub24_fightClub;

import java.util.Random;

public class Hero {

	private final int minStrength = 5;
	private final int maxStrength = 10;
	private boolean isKO; 
	private int strength;
	private String name;
	
	
	public Hero(String name) {
		this.name = name;
		strength = new Random().nextInt(maxStrength+1 - minStrength) + minStrength;	
		isKO = false;
	}
	
		
	public void hit() {
		Random rand = new Random();		
		if (rand.nextInt(100) <= strength) {
			isKO = true;
		}
	}
	
	
	public String getName() {
		return name;
	}
	
	public boolean isKO() {
		return isKO;
	}
	

}
