package ub31_polymorphieTiere;

public class Katze extends Tier {

	public Katze(String name, int alter, boolean gesund) {
		this.setName(name);
		this.setAlter(alter);
		this.setGesund(gesund);
	}
	
	@Override
	public void laufen() {
		System.out.printf( "%s %s läuft%n", this.getClass().getSimpleName(), this.getName() );
	}
	
	/*
	 * UnitTest
	 */
	public static void main(String[] args) {
		
		Katze cat = new Katze("Tom", 6, true);
		cat.laufen();
	}

}
