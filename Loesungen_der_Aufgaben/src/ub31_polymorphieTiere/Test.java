package ub31_polymorphieTiere;

public class Test {

	public static void main(String[] args) {
		
		Hund dog = new Hund("Rex", 8, true);
		print(dog);
		
		Katze cat = new Katze("Tom", 6, true);
		print(cat);
	}
	
	private static void print(Tier tier) {
		System.out.printf( "%s. Name: %s%n", tier.getClass().getSimpleName(), tier.getName() );
	}
}
