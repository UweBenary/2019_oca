package ub31_polymorphieTiere;

public class Hund extends Tier {

	public Hund(String name, int alter, boolean gesund) {
		this.setName(name);
		this.setAlter(alter);
		this.setGesund(gesund);
	}
	
	@Override
	public void laufen() {
		System.out.printf( "%s %s läuft%n", this.getClass().getSimpleName(), this.getName() );
	}

	/*
	 * UnitTest
	 */
	public static void main(String[] args) {
		
		Hund dog = new Hund("Rex", 8, true);
		dog.laufen();
	}
}
