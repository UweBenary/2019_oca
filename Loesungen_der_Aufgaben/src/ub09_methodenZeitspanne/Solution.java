package ub09_methodenZeitspanne;


/*
 * I have no idea what I have been doing here! but it works apparently... 
 */

/*
 * example of how to format and parse a date
 * 
 * http://tutorials.jenkov.com/java-date-time/parsing-formatting-dates.html#simpledateformat-example
 *  * 
 */


/*
 * https://stackoverflow.com/questions/20165564/calculating-days-between-two-dates-with-java/29812532
 * 
 * 
 * import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class NewDateDifference {

    public static void main(String[] args) {

        System.out.print("Insert first date: ");
        Scanner s = new Scanner(System.in);
        String[] eingabe1 = new String[3];

        while (s.hasNext()) {
            int i = 0;
            insert1[i] = s.next();
            if (!s.hasNext()) {
                s.close();
                break;
            }
            i++;
        }

        System.out.print("Insert second date: ");
        Scanner t = new Scanner(System.in);
        String[] insert2 = new String[3];

        while (t.hasNext()) {
            int i = 0;
            insert2[i] = t.next();
            if (!t.hasNext()) {
                t.close();
                break;
            }
            i++;
        }


        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(insert1[0]));
        cal.set(Calendar.MONTH, Integer.parseInt(insert1[1]));
        cal.set(Calendar.YEAR, Integer.parseInt(insert1[2]));
        Date firstDate = cal.getTime();

        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(insert2[0]));
        cal.set(Calendar.MONTH, Integer.parseInt(insert2[1]));
        cal.set(Calendar.YEAR, Integer.parseInt(insert2[2]));
        Date secondDate = cal.getTime();


        long diff = secondDate.getTime() - firstDate.getTime();

        System.out.println ("Days: " + diff / 1000 / 60 / 60 / 24);
    }
}
 */

/*
 * https://stackoverflow.com/questions/7103064/java-calculate-the-number-of-days-between-two-dates
 * 
 * private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

public static long getDayCount(String start, String end) {
  long diff = -1;
  try {
    Date dateStart = simpleDateFormat.parse(start);
    Date dateEnd = simpleDateFormat.parse(end);

    //time is always 00:00:00, so rounding should help to ignore the missing hour when going from winter to summer time, as well as the extra hour in the other direction
    diff = Math.round((dateEnd.getTime() - dateStart.getTime()) / (double) 86400000);
  } catch (Exception e) {
    //handle the exception according to your own situation
  }
  return diff;
}
 */

/*
 * java.time-API (Java SE 8)
 * 
 * http://tutorials.jenkov.com/java-date-time/duration.html
 * 
 * Instant first = Instant.now();
 * //wait some time while something happens
 * Instant second = Instant.now();
 * Duration duration = Duration.between(first, second);
 * 
 */

/*
 * Java's java.util.Calendar class
 * 
 * http://tutorials.jenkov.com/java-date-time/java-util-calendar.html
 * 
 */


import java.text.SimpleDateFormat;
import java.util.Date;

public class Solution {

	/*
	 * Task
	 * Definieren Sie eine statische Methode "getZeitspanneInGanzenTagen", 
	 * an die man als Argumente zwei Datumsangaben (zu Tag 1 und Tag 2) übergibt. 
	 * Die Methode liefert dann die Anzahl der ganzen Tagen zwischen den zwei Daten.
	 * Tipp: bei der Lösung ist es sinnvoll weitere Hilfsmethoden zu definieren, z.B. "istSchaltjahr"
	 */

	
	public static void main(String[] args) {
		
		String date1 = "01.12.2017" ;
		String date2 = "01.12.2018" ;
		
		long numOfDays = getZeitspanneInGanzenTagen( date1, date2);
		System.out.printf("Es liegen %d Tage dazwischen.%n", numOfDays);
	}	
	
	
	/**
	 * getZeitspanneInGanzenTagen returns the number of days between two dates
	 * 
	 * @return int: number of days
	 */
	
//	static int getZeitspanneInGanzenTagen( date1, date2) {
//		return 0;
//	}
	
	/*
	 * https://stackoverflow.com/questions/7103064/java-calculate-the-number-of-days-between-two-dates
	 */
	
	private static SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");

	public static long getZeitspanneInGanzenTagen(String start, String end) {
	  long diff = -1;
	  try {
	    Date dateStart = simpleDateFormat.parse(start);
	    Date dateEnd = simpleDateFormat.parse(end);

	    //time is always 00:00:00, so rounding should help to ignore the missing hour when going from winter to summer time, as well as the extra hour in the other direction
	    diff = Math.round((dateEnd.getTime() - dateStart.getTime()) / (double) 86400000);
	  } catch (Exception e) {
	    //handle the exception according to your own situation
	  }
	  return diff;
	}
	 	
}
