package ub26_vererbungGeometrie;

public class Rectangle extends Shape {

	private int width;
	private int hight;
	
	public Rectangle(int width, int hight) {
		super(0,0);
		this.width = width;
		this.hight = hight;
	}

	
	@Override
	public String toString() {
		return "The rectangle " + hight + "x" + width + ", and " + super.toString();
	}
}
