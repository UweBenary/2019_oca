package ub26_vererbungGeometrie;

public class Circle extends Shape {

	private int radius;

	public Circle(int radius) {
		super(0,0);
		this.radius = radius;
	}
	
	@Override
	public String toString() {
		return "The circle with radius " + radius + " has " + super.toString();
	}
}
