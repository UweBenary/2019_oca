package ub26_vererbungGeometrie;

import java.awt.Point;

public class Shape {

	private Point position;
	
	public Shape(int x, int y) {
		this.position = new Point(x,y);
	}
	
//
//	public Point getPosition() {
//		return position;
//	}
	

	
	@Override
	public String toString() {
		return "(X,Y)-coordinates: (" + position.getX() + ", " + position.getY() + ")";
	}


	public void move(int x, int y) {
		this.position.x = x;
		this.position.y = y;
	}



	
	
}
