package ub26_vererbungGeometrie;

public class Solution {

	public static void main(String[] args) {


		Rectangle rec1 = new Rectangle(3, 4);
		System.out.println( rec1 );
		
		rec1.move(12, -7);
		System.out.println( rec1 );
		
		Circle circ1 = new Circle(4);
		System.out.println( circ1 );
		
		circ1.move(33, 1);
		System.out.println( circ1 );
	}

}
