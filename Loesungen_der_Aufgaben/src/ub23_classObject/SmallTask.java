package ub23_classObject;


class Zebra {
	
	private String name;
	
	public Zebra(String name) {	
		this.name = name;
	}
	
	@Override
	public String toString() {
//		return "Zebra " + name;
//		return Zebra.class.getSimpleName() + " " + name; // Class<?> c = Zebra.class;  c.getSimpleName();
		return getClass().getSimpleName() + " " + name;
	}
}


public class SmallTask {

	public static void main(String[] args) {
		
		Zebra z1 = new Zebra("Rosie");
		System.out.println(z1); 
	}
}
