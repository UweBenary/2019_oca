package ub38_stringAPImethodes;

public class StringMethods {

	public static void main(String[] args) {

		/*
		 * - Kennen Sie die Konstruktoren?
		 */
		 String s1 = new String();
		 s1 = "Foo";
		 String s2 = new String("bar");
		 
		 /*
		  * - Testen Sie folgende Methoden. Sehen Sie sich dabei die Methodendedklarationen an.
		  */
		String result = s1.concat(s2);
		System.out.println(result);
//		    Kann man concat durch den Konkatenationsoperator (+) ersetzen?
		result = s1 + s2;
		System.out.println(result); //yes
		
		char ch = s1.charAt(2);
		System.out.println(ch);
		
		int len = s2.length();
		System.out.println(len);
		
		boolean empty = s1.isEmpty();
		System.out.println(empty);
		empty = "".isEmpty();
		System.out.println(empty);
		
		result = s1.toUpperCase();
		System.out.println(s1 + " -> " + result);
		result = s1.toLowerCase();
		System.out.println(s1 + " -> " + result);
		
		boolean end = s1.endsWith("oo");
		System.out.println( end);
		boolean start = s1.startsWith("f");
		System.out.println( start); 
		start = s1.startsWith("F", 0);
		System.out.println( start); 
		
		boolean eq = s1.equals("Foo");
		System.out.println("equals " + eq);
//		    Kann man die equals durch den Vergleichsoperator (==) ersetzen?
		eq = s1 == "Foo";
		System.out.println("equals " + eq); //yes
		
		eq = s1.equalsIgnoreCase("fOo");
		System.out.println("equals " + eq); 
		
		int idx = s1.indexOf('o');
		System.out.println(idx);
		idx = s1.indexOf('o', 2);
		System.out.println(idx);
		idx = s2.indexOf("ar") ;
		System.out.println(idx);
		idx = s1.indexOf("ar") ;
		System.out.println(idx);
		
		idx = s1.lastIndexOf('o');
		System.out.println(idx);
		idx = s1.lastIndexOf('b', 1);
		System.out.println(idx);
		idx = s1.lastIndexOf("o");
		System.out.println(idx);
		idx = s1.lastIndexOf("Fo", 7); // schräg, dass 7 geht
		System.out.println(idx);
		
		result = s1.replace('o', '0');
		System.out.println(s1 + " -> " + result);
//		- auch die replace, die zwei Strings als Argumente akzeptiert 
		result = s2.replace("b", "Foob");
		System.out.println(s2 + " -> " + result);
		
		result = s1.substring(1) ;
		System.out.println(s1 + " -> " + result);
		result = s1.substring(1, 1)  ;
		System.out.println(s1 + " -> " + result);
		
		/*
		 * - Testen Sie folgende statische Methoden. Sehen Sie sich bitte die Methodendedklarationen an.
		 */
		
		System.out.println();
		result = s1.valueOf(true);
		System.out.println(result);
		result = String.valueOf(true);	
		System.out.println(result);

		result = String.valueOf('x');
		System.out.println(result);
		result = String.valueOf(new char[] {'d','a','t','a'});
		System.out.println(result);
		result = String. valueOf(2.3e1);
		System.out.println(result);
		result = String. valueOf(2.3e1f);
		System.out.println(result);
		result = String.valueOf(3);
		System.out.println(result);
		result = String.valueOf( 23790000000000000L);
		System.out.println(result);
		result = String.valueOf(new int[] {1,2});
		System.out.println(result);
	}

}
