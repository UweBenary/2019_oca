package ub07_methoden;

public class Task5 {
	
	/*
	 * Aufgabe 5.
	 * Definieren Sie eine statische Methode "zeichneRechteck", 
	 * die zwei int-Parameter "breite" und "hoehe" und einen boolean-Parameter "fuellen" hat. 
	 * Die Methode zeichnet ein Rechteck auf der Konsole. 
	 * Wenn der Parameter "fuellen" true ist, wird ein gefülltes Rechteck gezeichnet, 
	 * sonst ein leeres.
	 */
	
	public static void main(String[] args) {
		
		/*
		 * get UserInput
		 */
		System.out.println("Laß uns ein Rechteck zeichnen!");
		int width = Task1.getIntInput("Bitte gib die Breite an (Typ int): ");		
		int hight = Task1.getIntInput("Bitte gib die Höhe an (Typ int): ");
		int fill = Task1.getIntInput("Soll das Rechteck gefüllt sein (1) oder leer (0): ");

		zeichneRechteck( width, hight, fill==1 );
				
	}

	static void zeichneRechteck( int width, int hight, boolean fill ) {
		
		if (fill) {
			Task4.zeichneRechteck( width, hight );
			
		} else {
			for (int i = 1; i <= hight; i++) {
				for (int j = 1; j <= width; j++) {
					
					if (i==1||i==hight||j==1||j==width) {
						System.out.print("*");
					} else {
						System.out.print(" ");
					}					
				}
				System.out.println(); // for cosmetic reasons
			}
		}	
	}

}
