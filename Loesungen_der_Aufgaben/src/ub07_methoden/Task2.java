package ub07_methoden;

public class Task2 {

	/*
	 * Aufgabe 2.
	 * Definieren Sie eine statische Methode "sum", die zwei int-Parameter erhält, 
	 * die Summe berechnet und das Ergebnis zurück liefert. 
	 * Testen Sie Ihre Lösung. 
	 * Wenn die Aufgabe gelöst ist, überlegen Sie ob Ihre Methode immer die korrekten Ergebnisse liefert.
	 */
	
	public static void main(String[] args) {
		
//		System.out.println(Integer.MAX_VALUE); // for testing. cannot remember this number;)
		
		long a = Task1.getIntInput("Bitte gib einen Summanden ein (Typ int): ");
		long b = Task1.getIntInput("Bitte gib einen zweiten Summanden ein (Typ int): ");

		int sum = sum( a, b );
		
		System.out.println( a + " + " + b + " = " + sum );
		
	}

	static int sum( long a, long b ) {
		
		long summe = a + b;
		
		if (summe != (int) summe) {
			System.err.println("Achtung! Summe außerhalb des int-Wertebereichs.");
		}
		
		return (int)summe;
		
	}
}
