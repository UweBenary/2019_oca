package ub07_methoden;

public class Task3 {

	/*
	 * Aufgabe 3.
	 * Definieren Sie eine statische Methode "sumFromTo", die zwei int-Parameter "from" und "to" hat. 
	 * Die Methode berechnet die Summe aller int-Werte von "from" bis "to" und liefert das Ergebnis zurück. 
	 * Testen Sie Ihre Lösung.
	 */
	
	public static void main(String[] args) {
		
		int from = Task1.getIntInput("Bitte gib einen Startwert ein (Typ int): ");		
		int to = Task1.getIntInput("Bitte gib einen Endwert ein (Typ int): ");

		int sum = sumFromTo( from, to );
		
		System.out.printf("Die Summe aller int-Werte von %d bis %d ist %d", from, to, sum );
		
	}
	
	static int sumFromTo( long from, long to ) {
		
		long summe = 0;
		
		if (from <= to) {
			for (long i = from; i < to+1 ; i++) {
				summe += i;					
			}
			
		} else {
			for (long i = to; i < from+1 ; i++) {
				summe += i;					
			}
		}
		
		if (summe != (int) summe) {
			System.err.println("Achtung! Summe außerhalb des int-Wertebereichs.");
		}
				
		return (int)summe;
	}
}
