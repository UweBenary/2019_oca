package ub07_methoden;

import java.util.*;

public class Task6 {

	/*
	 * Aufgabe 6.
	 * Definieren Sie eine statische Methode "printRandom", 
	 * die die N int-Zufallszahlen aus einem Zahlenbereich [VON...BIS] generiert 
	 * und auf der Konsole ausgibt. 
	 * Die Anzahl der Zahlen und die Unter- und Obergrenze des Zahlenbereiches 
	 * sollen beim Aufruf der Methode als Argumente übergeben werden.
	 * Z.B.: printRandom(5, 10, 20) gibt 5 int-Zufallswerte aus dem Bereich [10 ... 20] aus. 
	 */
	
	public static void main(String[] args) {
		
		/*
		 * get UserInput
		 */
		System.out.println("Gott würfelt nicht!\n");
		int count = Task1.getIntInput("Wieviele Zufallszahlen brauchst du (Typ int, > 0)? ");
		
		System.out.println("In welchem Intervall sollen sie liegen?");
		int from = Task1.getIntInput("   von (Typ int): ");
		int to = Task1.getIntInput("   bis (Typ int): ");
		
		System.out.printf("Die %d Zufallszahlen sind:%n" , count);
		printRandom( count, from, to );
				
	}
	
	static void printRandom( int count, int from, int to ) {
		
		Random rand = new Random(); //java.util.Random rand = new java.util.Random();
		int intRand;
		
		for (int i = 1; i <= count; i++) {
			intRand = rand.nextInt((to+1)-from) + from;            //+1 includes upper bound
			System.out.print(" " + intRand); 
		}
		
		System.out.println();// for visual reasons
	}
}
