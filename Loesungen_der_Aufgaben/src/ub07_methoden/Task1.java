package ub07_methoden;

import java.util.Scanner;

public class Task1 {

	/*
	 * Aufgabe 1.
	 * Definieren Sie eine statische Methode "printFromTo" mit zwei int-Parameter "from" und "to". 
	 * Die Methode gibt alle int-Werte von "from" bis "to" auf der Konsole in einer Zeile aus. 
	 * Testen Sie Ihre Lösung.
	 */
	
	public static void main(String[] args) {
		
		int from = getIntInput("Bitte gib einen Startwert ein (Typ int): ");
		
		int to = getIntInput("Bitte gib einen Endwert ein (Typ int): ");
		
		printFromTo( from, to );

	}
	
	static int getIntInput(String outputStr) {
		
		/*
		 * Get color from user
		 */
		System.out.print( outputStr ); 
		Scanner scanner = new Scanner( System.in );
		int num =  scanner.nextInt();		
		System.out.println(""); //newline for visual reasons
		
		return num;
	}
	
	
	static void printFromTo( int from, int to ) {
		
		if (from <= to) {
			for (int i = from; i < to+1 ; i++) {
					System.out.print(i + " ");			
			}
			
		} else {
			for (int i = from; i > to-1 ; i--) {
				System.out.print(i + " ");			
			}		
		}
		
		System.out.println(); // newline for visual reasons
	}

}
