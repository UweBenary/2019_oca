package ub07_methoden;

public class Task4 {

	/*
	 * Aufgabe 4.
	 * Definieren Sie eine statische Methode "zeichneRechteck", 
	 * die zwei int-Parameter "breite" und "hoehe" hat. 
	 * Die Methode zeichnet ein gefülltes Rechteck auf der Konsole.
	 */
	
	public static void main(String[] args) {
		
		/*
		 * get UserInput
		 */
		System.out.println("Laß uns ein Rechteck zeichnen!");
		int width = Task1.getIntInput("Bitte gib die Breite an (Typ int): ");		
		int hight = Task1.getIntInput("Bitte gib die Höhe an (Typ int): ");

		zeichneRechteck( width, hight );
				
	}

	static void zeichneRechteck( int width, int hight ) {
		
		for (int i = 1; i < hight+1; i++) {
			for (int j = 1; j < width+1; j++) {
				System.out.print("*");
			}
			System.out.println();
		}
		
	}
}
