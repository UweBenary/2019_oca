package ub13_klassenGeometrie;

class Rectangle {
	
	// Attributes
	private double width;
	private double hight;
	
	// Constructor
	Rectangle( double width, double hight ) {
		this.width = width;
		this.hight = hight;
	}
	
	// toString
	public String toString() {
		return "This rectangle has width x hight: " + this.width + " x " + this.hight;
	}

	// getter & setter
	public double getRectangleWidth() {
		return this.width;		
	}
	
	public double getRectangleHight() {
		return this.hight;		
	}

	public void setDimensions(double width, double hight) {
		this.width = width;
		this.hight = hight;		
	}
	
	// static setter(?)
	public static Rectangle setRectangleDimensions(Rectangle r, double width, double hight) {
		r.width = width;
		r.hight = hight;	
		return r;
	}
}
