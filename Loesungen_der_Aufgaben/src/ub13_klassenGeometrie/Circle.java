package ub13_klassenGeometrie;

class Circle {
	
	// Attributes
	private double radius;
	
	// Constructor
	Circle( double radius) {
		this.radius = radius;
	}

	// toString
	public String toString() {
		return "This circles has a radius of " + this.radius;
	}

	// getter & setter
	public void setRadius(int radius) {
		this.radius = radius;		
	}
	
	public double getRadius() {
		return this.radius;		
	}
}
