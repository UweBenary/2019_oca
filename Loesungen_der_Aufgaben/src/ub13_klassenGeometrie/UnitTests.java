package ub13_klassenGeometrie;

public class UnitTests {

	static void testRectangle() {
		
		try {
			Rectangle r1 = new Rectangle(0, 0);			
			System.out.println(r1);
			
			System.out.println("Test bestanden.");	
			System.out.println();

		} catch (RuntimeException  e) {
			System.out.println("Fehler! Test NICHT bestanden.");
		}
		
	}

	static void testCircle() {
		
		try {
			Circle c1 = new Circle(0);			
			System.out.println(c1);
			
			System.out.println("Test bestanden.");		
			System.out.println();

		} catch (RuntimeException  e) {
			System.out.println("Fehler! Test NICHT bestanden.");
		}
	
		
	}

	static void testRectangleSetDimensions() {
		
		final double expectedWidth = 2D;
		final double expectedHight = 5D;
		
		Rectangle r1 = new Rectangle(0, 0);			
		
		r1.setDimensions(expectedWidth, expectedHight);
		
		double actualWidth = r1.getRectangleWidth();
		double actualHight = r1.getRectangleHight();
		
		if(actualWidth == expectedWidth && actualHight == expectedHight ) {
			System.out.println("Test bestanden.");	
		
		} else {
			System.out.println("Fehler! Test NICHT bestanden.");
		}	
	}
}
