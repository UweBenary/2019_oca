package ub13_klassenGeometrie;

public class Solution {

	public static void main(String[] args) {
		
		/*
		 * Führen Sie den Begriff "Rechteck" in einem Projekt ein. Ein Rechteck hat Breite und Höhe.
		 */
		UnitTests.testRectangle();
		
		/*
		 * Es soll auch Kreise geben können. Jeder Kreis soll ein Radius haben. 
		 */
		UnitTests.testCircle();
		
		/*
		 * Ein Rechteck soll es ermöglichen seine Maße auf der Konsole auszugeben.
		 */
		Rectangle r1 = new Rectangle(2, 4);
		System.out.println(r1);
		
		/*
		 * Ein Kreis soll eine bequeme Möglichkeit anbieten sein Radius auf der Konsole auszugeben.
		 */
		Circle c1 = new Circle(4D);
		System.out.println(c1);
		
		/*
		 * Erzeugen Sie 100 Rechtecke mit zufälligen Breiten und Höhen. 
		 * Die Werte für die Breite und Höhe müssen aber aus dem Bereich zwischen 1 und 20 gewählt werden. 
		 * Geben Sie auf der Konsole die Informationen zu den erzeugten Objekten in etwa so aus:
		 *     1. Rechteck (3 X 5)
		 *     2. Rechteck (7 X 15)
		 *         ...
		       100. Rechteck (19 X 11)
		 */
		printRandomRectangles(100);
		
		/*
		 * Erzeugen Sie einen Kreis und setzen Sie sein Radius auf 5. 
		 * Erzeugen Sie die Ausgabe in folgender Form:    Kreis. R = 5
		 */
		Circle c2 = new Circle(0);
		c2.setRadius(5);
		System.out.printf("%nKreis. R = %d %n%n", (int) c2.getRadius());

		/*
		 * Definieren Sie eine statische Methode, 
		 * mit der sich die beiden Dimensionen eines Rechteckes ändern lassen.
		 */

		Rectangle r2 = new Rectangle(0, 0);
		System.out.println(r2);
		
		r2 = Rectangle.setRectangleDimensions(r2, 3, 5);
		System.out.println(r2);
		System.out.println();
		
		/*
		 * Definieren Sie eine NICHT-statische Methode, 
		 * mit der sich die beiden Dimensionen eines Rechteckes ändern lassen.
		 */
		UnitTests.testRectangleSetDimensions();

	}
	
	
	public static void printRandomRectangles(int countMax) {
		
		for (int i = 1; i <= countMax; i++) {
			double rand1 = new java.util.Random().nextDouble() *(20-1) +1;
			double rand2 = new java.util.Random().nextDouble() *(20-1) +1;
			Rectangle r1 = new Rectangle( rand1, rand2);
			
			System.out.printf("%d. Rechteck (%d x %d) %n", i,  (int) r1.getRectangleWidth(), (int) r1.getRectangleHight());
		}
	}
}
