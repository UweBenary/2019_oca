package ub36_interfacesCompPerson;

import java.util.Comparator;

public class PersonComperator 
	implements Comparator<Person> {

	@Override
	public int compare(Person p1, Person p2) {
		return p2.compareTo(p1);
	}
}
