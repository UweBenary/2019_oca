package ub36_interfacesCompPerson;

import java.util.Arrays;
import java.util.Comparator;


public class Solution {

	public static void main(String[] args) {

//		DateFormat formatter = new SimpleDateFormat( "dd.MM.yyyy" );
//		Date date = null;
//		try {
//			date = formatter.parse( "12-03-2008" );
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		System.out.println(date);
		
		
		Person[] persons = new Person[4];
		persons[0] = new Person("Paul", "Smith");
		persons[1] = new Person("Paul", "Black");
		persons[2] = new Person("John", "Smith");
		persons[3] = new Person("John", "Black");
		
		System.out.println( Arrays.toString(persons) );

		Arrays.sort(persons);
		System.out.println( Arrays.toString(persons) );
		
		
		optional();
		
		System.out.println( "*** reverse" );
		Person[] personsAscending = Arrays.copyOf(persons, persons.length); // memorize sorted persons
//		System.out.println( Arrays.toString(personsAscending) );
		
		Comparator<Person> cmp = new PersonComperator();
		Arrays.sort(persons, cmp);
		System.out.println( Arrays.toString(persons) );
		
		System.out.println( "*** search" );
		Person wantedPerson = new Person("John", "Black");
		int position = Arrays.binarySearch(personsAscending, wantedPerson);
		System.out.println( Arrays.toString(personsAscending) );
		System.out.println(wantedPerson + " has position " + position);
		

		wantedPerson = new Person("John", "Smith");
		position = Arrays.binarySearch(persons, wantedPerson, cmp);
		System.out.println();
		System.out.println( Arrays.toString(persons) );
		System.out.println(wantedPerson + " has position " + position);
	}
	
	static private void optional() {
		
		System.out.println("*** optional:");
		
		Person[] persons = new Person[4];
		
		persons[0] = new Person("Paul", "Smith");
		persons[0].setDateOfBirth("24.12.2006");
		persons[1] = new Person("Paul", "Smith");
		persons[2] = new Person("Paul", "Smith");
		persons[2].setDateOfBirth("12.02.1906");	
		persons[3] = new Person("Paul", "Smith");
		
		System.out.println( Arrays.toString(persons) );

		Arrays.sort(persons);
		System.out.println( Arrays.toString(persons) );
	}

}
