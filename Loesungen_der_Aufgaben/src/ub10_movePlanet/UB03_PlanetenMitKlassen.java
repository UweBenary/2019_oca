package ub10_movePlanet;

class Planet {
	String name;
	int x, y;
}


public class UB03_PlanetenMitKlassen {

	public static void main(String[] args) {
		
		Planet p1 = new Planet();
		p1.name = "Erde";
		p1.x = 12;
		p1.y = 13;
		
		printPlanet(p1);
		
		Planet p2 = new Planet();
		printPlanet(p2);
//		System.out.println(p2);
		
		movePlanet(p1, 44, 30);
		printPlanet(p1);
		
	}
	
	static void printPlanet(Planet planet) {
		System.out.println("Planet: " + planet.name + ", Koordinaten: " + planet.x + ", " + planet.y);
	}
	
	static void movePlanet(Planet planet, int x, int y) {
		planet.x = x;
		planet.y = y;
	}
}
