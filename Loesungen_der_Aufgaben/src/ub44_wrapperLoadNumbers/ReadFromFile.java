package ub44_wrapperLoadNumbers;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;


public class ReadFromFile {

	public static void main(String[] args) {
		
		String fileName = "zahlen.txt";

		Path pathToFile = getPathToFile( fileName );
//		System.out.println(pathToFile.toString());
				
		ArrayList<Integer> zahlen = loadArrayFromFile(pathToFile);
		
		System.out.println( zahlen );
		
	}

	/**
	 * get absolute path to File
	 */
	private static Path getPathToFile(String fileName) {
		
		String packageName = (new ReadFromFile()).getClass().getPackageName();
		String pathString = Paths.get(System.getProperty("user.dir")).toString();

		return Paths.get(pathString,"src", packageName, fileName);
	}

	private static ArrayList<Integer> loadArrayFromFile(Path pathToFile) {

		StringBuilder sb = readFromFile( pathToFile.toString() );
		
		excludeComments(sb);		

		return getArray(sb);
	}

	/**
	 * Parses StringBuilder sb for Integer and adds them to ArrayList<Integer>
	 * @param sb
	 * @return
	 */
	private static ArrayList<Integer> getArray(StringBuilder sb) {
		
		ArrayList<Integer> resultArrayList = new ArrayList<Integer>();
		
		int idxStartLine = 0;
		int idxEndLine = 0; 
		String s;

		do {
			idxEndLine = sb.indexOf( "\n", idxStartLine );
			if (idxEndLine == 0) {
				idxStartLine = idxEndLine+1; // beginns with newline
			}
			s = sb.substring(idxStartLine, idxEndLine);
			resultArrayList.add( Integer.valueOf(s) );
			
			idxStartLine = idxEndLine+1;
		} while ( idxStartLine < sb.length() );
		
		return resultArrayList;
	}
	
	/**
	 * excludes comment lines "#   \n" in StringBuilder sb
	 */
	private static void excludeComments(StringBuilder sb) {
		
		int idxFirst, idxLast;
		do {
			idxFirst = sb.indexOf( "#" );			
			if (idxFirst < 0) {
				continue;
			}
			idxLast = sb.indexOf(  "\n", idxFirst+1 );		// System.getProperty("line.separator");
			sb.delete(idxFirst, idxLast+1);

		} while ( !(idxFirst < 0) );		
	}

	/**
	 * Reads from file 
	 * @param pathToFile absolute path to file
	 * @return
	 */
	static StringBuilder readFromFile(String pathToFile) {

		StringBuilder sb = new StringBuilder();
		
//		String pathToFile = "C:\\Path\\to\\zahlen.txt";		
		try(BufferedReader in = new BufferedReader(new FileReader(pathToFile))) {
							
//			StringBuilder sb = new StringBuilder();
			String line;
			while( (line = in.readLine()) != null ) {
				sb.append(line).append("\n");
			}
			
		} catch (IOException e) {
			throw new UncheckedIOException("Kann die Datei nicht lesen", e);
		}
		
		return sb;		
	}
}
