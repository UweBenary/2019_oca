package ub19_arrays2D;

public class Solutions {

	public static void main(String[] args) {

		/*
		 * A1:
		 * Bitte definieren Sie eine Methode 'createArray', die ein zweideminseonales int-Array erzeugt. 
		 * Die neue Methode soll aus der main-Methode folgendermassen aufgerufen werden koennen:
		 * 		int[][] arr = createArray(4, 5);
		 * > In diesem Beispiel wird ein 2D-Array 4 x 5 erzeugt.
		 */
		int[][] arr = createArray(4, 5);
		
		/*
		 * A2:
		 * Bitte geben Sie das erzeugte Array aus. 
		 * Dafür definieren Sie eine weitere Methode 'printArray', an die Sie das Array übergeben.
		 */
		System.out.println();
		printArray(arr);
		
		/*
		 * A3:
		 * Speichern Sie in dem Array auf der Stelle (1,1) den Wert 5 und geben Sie das Array erneut auf dem Bildschirm aus.
		 */
		arr[1][1] = 5;
		System.out.println();
		printArray(arr);
		
		/*
		 * A4:
		 * Belegen Sie alle Positionen in dem Array mit dem Wert 2. Benutzen Sie dazu die verschachtelten for-Schleifen.
		 */
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = 2;
			}
		}
		System.out.println();
		printArray(arr);
		
		/*
		 * A5: 
		 * Bitte überladen Sie die Methode 'createArray' durch eine weitere Methode, 
		 * die ein zweideminseonales int-Array erzeugt und mit dem gewünschten Wert belegt. 
		 * Die neue Methode soll aus der main-Methode folgendermassen aufgerufen werden koennen:
		 *         int[][] arr = createArray(4, 5, 2);
		 * > In diesem Beispiel wird ein 2D-Array 4 x 5 erzeugt, alle Elemente haben den Wert 2:
		 */
		int[][] arr2 = createArray(4, 5, 2);
		System.out.println();
		printArray(arr2);
	
		/*
		 * A6:
		 * Optional: ändern Sie die Methode 'printArray' so, dass die ausgegebenen Elemente durch Kommas getrennt sind.
		 *     Z. B. für ein Array 4x5, das mit 0-Werten belegt ist:
		 *             0, 0, 0, 0, 0
 							...
 			           0, 0, 0, 0, 0
		 */
		int[][] arr3 = createArray(4, 5);
		System.out.println();
		printArray(arr3, ",");
		
	}
	
 /*
	 * A1:
	 * Bitte definieren Sie eine Methode 'createArray', die ein zweideminseonales int-Array erzeugt. 
	 * Die neue Methode soll aus der main-Methode folgendermassen aufgerufen werden koennen:
	 * 		int[][] arr = createArray(4, 5);
	 * > In diesem Beispiel wird ein 2D-Array 4 x 5 erzeugt.
	 */
	static int[][] createArray(int dim1, int dim2) {
		
		return new int[dim1][dim2];
	}
	
	/*
	 * A2:
	 * Bitte geben Sie das erzeugte Array aus. 
	 * Dafür definieren Sie eine weitere Methode 'printArray', an die Sie das Array übergeben.
	 */
	static void printArray(int[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	/*
	 * A5: 
	 * Bitte überladen Sie die Methode 'createArray' durch eine weitere Methode, 
	 * die ein zweideminseonales int-Array erzeugt und mit dem gewünschten Wert belegt. 
	 * Die neue Methode soll aus der main-Methode folgendermassen aufgerufen werden koennen:
	 *         int[][] arr = createArray(4, 5, 2);
	 * > In diesem Beispiel wird ein 2D-Array 4 x 5 erzeugt, alle Elemente haben den Wert 2:
	 */
	static int[][] createArray(int dim1, int dim2, int value) {
		int[][] result = new int[dim1][dim2];
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				result[i][j] = 2;
			}
		}
		return result;
	}
	
	/*
	 * A6:
	 * Optional: ändern Sie die Methode 'printArray' so, dass die ausgegebenen Elemente durch Kommas getrennt sind.
	 *     Z. B. für ein Array 4x5, das mit 0-Werten belegt ist:
	 *             0, 0, 0, 0, 0
							...
			       0, 0, 0, 0, 0
	 */
	static void printArray(int[][] arr, String deliminator) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length-1; j++) {
				System.out.print(arr[i][j] + deliminator + " ");
			}
			System.out.println( arr[i][arr[i].length-1] );
		}
	}
}
