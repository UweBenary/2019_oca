package ub19_arrays2D;

public class OptionalBorderedArray {
	
	public static void main(String[] args) {

		/*
		 * Optional. 
		 * Definieren Sie bitte in einer neuen Klasse die Methode 'createBorderedArray', 
		 * die ein neues 2D-char-Array erzeugt, das die Dimensionen 'dim' X 'dim' hat. 
		 * Das Array ist mit Leerzeichen gefuellt, nur der Rand besteht aus dem Zeichen 'ch'.
		 * Wird ein Array z.B. durch den Aufruf createBorderedArray(5, 'X') erzeugt und dann ausgegeben, 
		 * sieht die Ausgabe etwa so aus:
//		        XXXXX
//		        X   X
//		        X   X
//		        X   X
//		        XXXXX
		 */
		
		char[][] arr = createBorderedArray(5, 'X');
		
		printArray(arr);
	}
	
	public static char[][] createBorderedArray(int dim, char ch) {
		
		char[][] result = new char[dim][dim];
		
		for (int i = 0; i < result.length; i++) {
			for (int j = 0; j < result[i].length; j++) {
				if (i==0||i==result.length-1||j==0||j==result[i].length-1) {
					result[i][j] = ch;
		
				} else {
					result[i][j] = ' ';
				}
			}
			System.out.println();
		}
		return result;
	}
	
	static void printArray(char[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j]);
			}
			System.out.println();
		}
	}

}
