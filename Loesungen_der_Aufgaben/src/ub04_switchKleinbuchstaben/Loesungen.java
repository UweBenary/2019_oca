package ub04_switchKleinbuchstaben;

import java.util.Scanner;

public class Loesungen {

	public static void main(String[] args) {
		
		char ch1 = userAbfrage();
		
		/*
		 * Check ch1 isEnglishCharacter
		 *    optional task I
		 */
		boolean check = (ch1>96 && ch1<96+26+1) ;
		if(check) {
			switchAbfrage(ch1);			
		} else {
			System.out.println(ch1 + " ist kein englischer Kleinbuchstabe!");
		}


		
		/*
		 * Check ch1 isUmlaut
		 *    optional task II
		 */
		boolean check2 = ch1==228  || //ä			
					 	 ch1==246  || //ö
						 ch1==252  ;  //ü
		boolean check3 = ch1==223  ;  //ß	
		
		if(check2) 		  {
			System.out.println(ch1 + " ist ein deutscher Umlaut!");
		} else if(check3) { 
			System.out.println(ch1 + " ist eine deutsche Ligatur!");
		}

	}

	static char userAbfrage() {
		
		System.out.println(" Geben Sie bitte einen kleinen englischen Buchstaben ein: ");
		
		/*
		 * Get character from user
		 */
		Scanner input = new Scanner( System.in );
		char ch1 = input.next(".").charAt(0); // needs catch/throw for exception if input > 1 character
		
		return ch1;
	}
	
	static void switchAbfrage(char ch1) {
		
		switch (ch1) {
		case 'a':
		case 'e':
		case 'i':
		case 'o':
		case 'u':
			System.out.println("Das ist ein Vokal.");
			break;

		default:
			System.out.println("Das ist ein Konsonant.");
		}
	}
	
}
