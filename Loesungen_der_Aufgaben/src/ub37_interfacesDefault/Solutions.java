package ub37_interfacesDefault;

public class Solutions {

	public interface Sum {
	   default int add(int a, int b) {
		   return a + b;
	   }
	}
	
	public interface Container {
	   /* 
	    * Speichert x im Container.
	    * Liefert die neue Containergröße zurück.
	    */
	   int add(int x);
	   
	   
	   /*
	    * Überladen. 
	    * Speichert im Container x, danach y.
	    * Liefert die neue Containergröße zurück.
	    */

	   default int add(int x, int y) {
		   System.out.println("Speichert im Container x, danach y.");
		   return x + y;		   
	   }
	}
	
	static class Calc 
		implements Sum {
	
	}	
	
	static class Calc2 
	implements Sum, Container {

	@Override
	public int add(int x) {
		return x;
	}

	@Override
	public int add(int x, int y) {
		return Container.super.add(x, y);
	}
	
}	
	
	public static void main(String[] args) {

		Calc calcSum = new Calc();
		int sum = calcSum.add(2, 3);
		System.out.println(sum);
		
		Calc2 calcSum2 = new Calc2();
		int sum2 = calcSum2.add(2, 3);
		System.out.println(sum2);

	}

}
