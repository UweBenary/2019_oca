package ub28_vererbungDozentTeilnehmer;

public class Person {

	private final String NAME;
	private int id;
	
	public Person(String name) {
		this(name, -1);
	}
	
	public Person(String name, int id) {
		this.NAME = name;
		setID( id );
	}

	public String getName() {
		return NAME;
	}
	public int getID() {
		return id;
	}

	public void setID(int id) {
		this.id = id;
	}	
}
