package ub28_vererbungDozentTeilnehmer;

public class Dozent extends Person{

	Dozent(String name){
		super(name);
	}
	
	public void leiten(JavaKurs kurs) {
		System.out.printf("Dozent %s leitet den Java-Kurs %d.%n", this.getName(), kurs.getNumber());
	}	
}
