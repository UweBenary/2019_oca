package ub28_vererbungDozentTeilnehmer;

public class Teilnehmer extends Person{

	public Teilnehmer(String name){
		super(name);
	}
	
	public void besuchen(JavaKurs kurs) {
		System.out.printf("Teilnehmer %s besucht den Java-Kurs %d.%n", this.getName(), kurs.getNumber());
	}
}
