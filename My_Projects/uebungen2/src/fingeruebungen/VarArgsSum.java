package fingeruebungen;

public class VarArgsSum {

	public static void main(String[] args) {

		System.out.println( sum(1, 2) );
		System.out.println( sum(1, 2, 3) );
		System.out.println( sum(1, 2, 3 ,-3) );

		System.out.println( sum(1, 2, 3 ,Integer.MAX_VALUE) );
		
	}

	static int sum(int arg1, int arg2, int... args) {
		long result = (long) arg1 + (long) arg2;
		for (int i = 0; i < args.length; i++) {
			result += (long) args[i];
		}		
		
		if (result > Integer.MAX_VALUE || result < Integer.MIN_VALUE ) {
			throw new ArithmeticException("Integer overflow exception.");
		}
		return (int) result;
	}
}
