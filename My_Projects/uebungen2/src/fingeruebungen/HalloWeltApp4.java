package fingeruebungen;

public class HalloWeltApp4 {

	public static void main(String[] args) {

		String str = grussZusammenbauen("Java");
		System.out.println(str);
	}

	static String grussZusammenbauen(String name) {
		return "Hallo " + name + "!";
	}
}
