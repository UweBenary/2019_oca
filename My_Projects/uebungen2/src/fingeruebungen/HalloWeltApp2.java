package fingeruebungen;

public class HalloWeltApp2 {

	public static void main(String[] args) {
		
		grussAusgeben();
	}
	
	static void grussAusgeben() {
		System.out.println("Hallo Welt!");
	}
	
	protected static void grussAusgeben(String person) {
		System.out.println("Hallo " + person + "!");
	}
}
