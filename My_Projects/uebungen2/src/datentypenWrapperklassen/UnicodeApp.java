package datentypenWrapperklassen;

public class UnicodeApp {

	public static void main(String[] args) {


		char ch = '\u0000';
		for (int i = 0; i < 256; i++, ch++) {

			if (i%20==0) {
				System.out.format(" %n %4d: ",i);
			
			} 
			System.out.print(ch + " ");			
		}
	}

}
