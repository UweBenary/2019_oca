package datentypenWrapperklassen;


public class BitZuIntegerApp {

	static class IntegerOverflowException extends RuntimeException { 
		public IntegerOverflowException(String message) {
			super(message);
		}
	};
	
	private BitZuIntegerApp() {	}

	public static int bitZuInteger(byte[] bits) {
		long result = 0;
		for (int i = bits.length-1, power = 0; i > -1 ; i--, power++) {			
			result += bits[i] * calcPower(power, 2);
		}
		if (result>Integer.MAX_VALUE) {
			throw new IntegerOverflowException("Result exceeds maximal integer value!");
		}
		return (int) result;
	}

	private static long calcPower(int power, int base) {
		long result = 1;
		for (int i = 1; i < power+1; i++) {
			result *= base;
		}
		return result;
	}
	
	public static void main(String[] args) {
		
		byte[] bits = {1,0,0,1,1,1,0,1,0};
		
		System.out.println( bitZuInteger(bits) );
		
		////////
		
		String bitWert = "100111010";
		int intWert = Integer.parseInt(bitWert, 2);
		System.out.println(intWert);
	}
}
