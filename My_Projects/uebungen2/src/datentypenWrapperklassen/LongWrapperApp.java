package datentypenWrapperklassen;

public class LongWrapperApp {

	public static void main(String[] args) {

		long l = 42L;
		Long longObj = Long.valueOf(l);

		long longVar = 42L;
		
		String trueString = "Die Werte sind gleich!";
		String falseString = "Die Werte sind ungleich!";
		String comparisonResult = longVar == longObj.longValue() ? trueString : falseString;
		System.out.println(comparisonResult);
	}

}
