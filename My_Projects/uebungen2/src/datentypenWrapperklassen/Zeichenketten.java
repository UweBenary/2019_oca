package datentypenWrapperklassen;

import java.util.Arrays;

public class Zeichenketten {

	public static void main(String[] args) {

		String string1 = "HalloWeltApp.java";
		String string2 = "halloweltapp.java";
		String string3 = "XaversAdresse.txt";
		
		if ( !string1.endsWith(".class") ) {
			System.out.println("Keine kompilierte Java-Datei!");
		}
		
		System.out.println( string1.contentEquals(string2) );
		

		System.out.println( string1.equalsIgnoreCase(string2) );
		
		String[] arr = {string2, string3};
		Arrays.sort(arr);
		
		System.out.println(Arrays.toString(arr));
		
		
		String string4 = "  Fischers Fritz fischt frische Fische.";
		
		System.out.println( string4.trim().toUpperCase() );
		
		int beginIndex = string4.indexOf(" ", string4.indexOf("Fischers"));
		int endIndex = string4.indexOf(" ", string4.indexOf("fischt"));
		System.out.println( string4.replace( string4.substring(beginIndex , endIndex), "").trim() );
		
		String string5 = string4.toLowerCase().trim();
		System.out.println( string5.matches(".*fisch.*"));
		System.out.println( string5.contains("fisch"));
	}

}
