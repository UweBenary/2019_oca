package datentypenWrapperklassen;

public class IntWerteBereicheApp {

	public static void main(String[] args) {

		Byte b = Byte.valueOf((byte) 0);
		Short s = Short.valueOf((short) 0);
		Integer i = Integer.valueOf(0);
		Long l = Long.valueOf( 0);
		
		Number[] nums = {b, s, i, l};
		for (Number number : nums) {
			System.out.format("%10s | %25s | %25s %n",
					number.getClass().getSimpleName(),
					getMin(number),
					getMax(number));
		}
	}

	private static long getMin(Number number) {
		if (number instanceof Byte) {
			return Byte.MIN_VALUE;
		} else if (number instanceof Short) {
			return Short.MIN_VALUE;
		} else if (number instanceof Integer) {
			return Integer.MIN_VALUE;
		} else {
			return Long.MIN_VALUE;
		}
		
		
	}

	private static Object getMax(Number number) {
		if (number instanceof Byte) {
			return Byte.MAX_VALUE;
		} else if (number instanceof Short) {
			return Short.MAX_VALUE;
		} else if (number instanceof Integer) {
			return Integer.MAX_VALUE;
		} else {
			return Long.MAX_VALUE;
		}
	}

}
