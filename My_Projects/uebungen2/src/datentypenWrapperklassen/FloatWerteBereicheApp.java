package datentypenWrapperklassen;

import java.lang.reflect.Field;

import datentypenWrapperklassen.IntWerteBereicheApp2.IntWert;
import datentypenWrapperklassen.IntWerteBereicheApp2.MaxValueSetter;
import datentypenWrapperklassen.IntWerteBereicheApp2.MinValueSetter;
import datentypenWrapperklassen.IntWerteBereicheApp2.Visitor;


public class FloatWerteBereicheApp {

	static class DoubleWert extends IntWert {
		private double minValue;
		private double maxValue;
		
		public DoubleWert(Number number) {
			super(number);
		}
		
		@Override
		public String toString() {
			return String.format("%10s | %15f | %25f %n",
					number.getClass().getSimpleName(),
					minValue,
					maxValue);
		}
	}
	
	static class DoubleMinValueSetter extends Visitor {

		void visit(DoubleWert doubleWert) {
			Field field;
			try {
				field = doubleWert.number.getClass().getField("MIN_VALUE");
				doubleWert.minValue = field.getDouble(field);
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		void visit(IntWert intWert) {
			visit((DoubleWert) intWert);
		}		
	}
	
	static class DoubleMaxValueSetter extends Visitor {

		void visit(DoubleWert doubleWert) {
			Field field;
			try {
				field = doubleWert.number.getClass().getField("MAX_VALUE");
				doubleWert.maxValue = field.getDouble(field);
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		void visit(IntWert intWert) {
			visit((DoubleWert) intWert);
		}		
	}
	
	public static void main(String[] args) {
		
		Float f = Float.valueOf(0.f);
		Double d = Double.valueOf( 0);
		
		DoubleMinValueSetter visitor1 = new DoubleMinValueSetter();
		DoubleMaxValueSetter visitor2 = new DoubleMaxValueSetter();
		DoubleWert[] nums = {new DoubleWert(f), new DoubleWert(d)};
		for (DoubleWert number : nums) {
			number.accept(visitor1);
			number.accept(visitor2);
			System.out.println( number.toString() );
		}
		
	}

}
