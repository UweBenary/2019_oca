package datentypenWrapperklassen;

import java.lang.reflect.Field;

public class IntWerteBereicheApp2 {
	
	static class IntWert {
		protected Number number;
		private long minValue;
		private long maxValue;
		
		public IntWert(Number number) {
			this.number = number;
		}
		
		@Override
		public String toString() {
			return String.format("%10s | %25d | %25d %n",
					number.getClass().getSimpleName(),
					minValue,
					maxValue);
		}
		
		public void accept(Visitor visitor) {
			visitor.visit(this);
		}
	}
	
	static abstract class Visitor {
		abstract void visit(IntWert intWert) ;
	}
	
	static class MinValueSetter extends Visitor {

		@Override
		void visit(IntWert intWert) {
			Field field;
			try {
				field = intWert.number.getClass().getField("MIN_VALUE");
				intWert.minValue = field.getLong(field);
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}
	
	static class MaxValueSetter extends Visitor {

		@Override
		void visit(IntWert intWert) {
			Field field;
			try {
				field = intWert.number.getClass().getField("MAX_VALUE");
				intWert.maxValue = field.getLong(field);
			} catch (NoSuchFieldException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}		
	}

	public static void main(String[] args) {

		Byte b = Byte.valueOf((byte) 0);
		Short s = Short.valueOf((short) 0);
		Integer i = Integer.valueOf(0);
		Long l = Long.valueOf( 0);
		
		MinValueSetter visitor1 = new MinValueSetter();
		MaxValueSetter visitor2 = new MaxValueSetter();
		IntWert[] nums = {new IntWert(b), new IntWert(s), new IntWert(i), new IntWert(l)};
		for (IntWert number : nums) {
			number.accept(visitor1);
			number.accept(visitor2);
			System.out.println( number.toString() );
		}
	}



}
