package datentypenWrapperklassen;

public class LogBitweiseApp {

	public static void main(String[] args) {

		System.out.println( Long.toBinaryString(2345678));
		System.out.println( Long.toBinaryString(1));
		System.out.println( Long.toBinaryString(0));
		System.out.println( Long.toBinaryString(1234985123417851235L));

	}

}
