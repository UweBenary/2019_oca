package pong;

import java.util.Scanner; 

public class UtilsLib {

static int getIntInput(String outputStr) {
		
		/**
		 * static int getIntInput(String outputStr)
		 *    Gets int input from user
		 * 
		 */
		System.out.print( outputStr ); 
		Scanner scanner = new Scanner( System.in );
		int num =  scanner.nextInt();		
//		System.out.println(""); //newline for visual reasons
		
		return num;
	}


}
