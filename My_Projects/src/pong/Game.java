package pong;

/*
 * checkBallPosition and checkBallDirction to be integrated in Ball constructor?
 * Attention: dependency on board instance!
 */

 
public class Game {

	public static void main(String[] args) {

		/*
		 * init board
		 */
		Board board1 = Board.createBoard();
		
		/*
		 * init ball
		 */
		
		Ball ball1 = Ball.createBallOnBoard(board1);
		
		checkBallPosition (ball1, board1);
		checkBallDirection(ball1);

		/*
		 * play game
		 */

		int repeats = 200; //length of game
		
		for (int i = 0; i <= repeats; i++) {
//			System.out.println(ball1.xPosition + " " + ball1.yPosition + " " + ball1.angle); // for debugging
//			System.out.println(ball1.xPosition < board1.xMax && ball1.xPosition > board1.xMin);
//			System.out.println(ball1.yPosition < board1.yMax && ball1.yPosition > board1.yMin);
			
			displayBoard(ball1, board1);
			moveBall(ball1, board1);
		}
		
	}
	
	
	static void checkBallPosition (Ball ball, Board board) {//implementation sucks
		int xDisplay = (int) Math.round(ball.getxPosition());
		int yDisplay = (int) Math.round(ball.getyPosition());
		
		if (xDisplay <= board.getBoardXMin()) {
			ball.setxPosition(board.getBoardXMin() +1);
		}
		if (yDisplay <= board.getBoardYMin()) {
			ball.setyPosition(board.getBoardYMin() +1);
		}
		if (xDisplay >= board.getBoardXMax()) {
			ball.setxPosition(board.getBoardXMax() -1);
		}
		if (yDisplay >= board.getBoardYMax()) {
			ball.setyPosition(board.getBoardYMax() -1);
		}
	}
	
	static void checkBallDirection (Ball ball) {
		while (ball.getAngle()<0) {
			ball.setAngle(ball.getAngle() +360D);
		}
		while (ball.getAngle()>360) {
			ball.setAngle(ball.getAngle() -360D);
		}
	}
	
	static void displayBoard(Ball ball, Board board) {

		int xDisplay = (int) Math.round(ball.getxPosition());
		int yDisplay = (int) Math.round(ball.getyPosition());
		
		for (int y = board.getBoardYMax(); y >= board.getBoardYMin() ; y--) {
			for (int x = board.getBoardXMin(); x <= board.getBoardXMax(); x++) {
				
				if (x==xDisplay && y==yDisplay){
					System.out.print("°");
				} else if ( x==board.getBoardXMin()||
							x==board.getBoardXMax()||
							y==board.getBoardYMin()||
							y==board.getBoardYMax()) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();			
		}
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static void moveBall(Ball ball, Board board) {
		
		double newX = ball.getxPosition() + 1 * Math.cos(ball.getAngle() * 3.1415 / 180);
		double newY = ball.getyPosition() + 1 * Math.sin(ball.getAngle() * 3.1415 / 180);	
		
		/*
		 * Check for hitting board boundaries
		 */
		
		int caseFlag = 11;//1st Digit: x, 2nd Digit: y
		
		if (newX >= board.getBoardXMax()) {
			caseFlag += 10; 
		} else if (newX <= board.getBoardXMin()) {
			caseFlag -= 10; 
		} 
		
		if (newY >= board.getBoardYMax()) {
			caseFlag += 1; 
		} else if (newY <= board.getBoardYMin()) {
			caseFlag -= 1; 
		}
		
		if (caseFlag!=11) {
			reflectBall(ball, board, caseFlag);
		} else {
			ball.setxPosition(newX); 
			ball.setyPosition(newY);
		}
	}
	
	static void reflectBall(Ball ball, Board board, int caseFlag) {
		// simpler code...
		
		double newX = ball.getxPosition() + 1 * Math.cos(ball.getAngle() * 3.1415 / 180);
		double newY = ball.getyPosition() + 1 * Math.sin(ball.getAngle() * 3.1415 / 180);	
		
		Ball dummyBall = new Ball(0, 0, 0);
		
		switch (caseFlag) {
		case 0: //newX <= board.xMin & newY <= board.yMin
			dummyBall.setxPosition(board.getBoardXMin());
			dummyBall.setyPosition(board.getBoardYMin());
			displayBoard( dummyBall, board);
			; // keep ball position
			ball.setAngle(ball.getAngle() +180D); //turn direction
			checkBallDirection(ball);
			break;

		case 2: //newX <= board.xMin & newY >= board.yMax
			dummyBall.setxPosition(board.getBoardXMin());
			dummyBall.setyPosition(board.getBoardYMax());
			displayBoard( dummyBall, board);
			; // keep ball position
			ball.setAngle(ball.getAngle() +180D); //turn direction
			checkBallDirection(ball);
			break;
			
		case 20://newX >= board.xMax & newY <= board.yMin
			dummyBall.setxPosition(board.getBoardXMax());
			dummyBall.setyPosition(board.getBoardYMin());
			displayBoard( dummyBall, board);
			; // keep ball position
			ball.setAngle(ball.getAngle() +180D); //turn direction
			checkBallDirection(ball);
			break;
			
		case 22://newX >= board.xMax & newY >= board.yMax
			dummyBall.setxPosition(board.getBoardXMax());
			dummyBall.setyPosition(board.getBoardYMax());
			displayBoard( dummyBall, board);
			; // keep ball position
			ball.setAngle(ball.getAngle() +180D); //turn direction
			checkBallDirection(ball);
			break;
			
		case 1: //newX <= board.xMin
//			System.out.println("marke1"); // debugging
			dummyBall.setxPosition(board.getBoardXMin());
			dummyBall.setyPosition(newY);
			displayBoard( dummyBall, board);
			
			ball.setxPosition(ball.getxPosition());
			ball.setyPosition(newY); 			
			ball.setAngle(180D - ball.getAngle()); 
			checkBallDirection(ball);
			break;
			
		case 10: //newY <= board.yMin
//			System.out.println("marke10"); // debugging
			dummyBall.setyPosition(board.getBoardYMin());
			dummyBall.setxPosition(newX);
			displayBoard( dummyBall, board);
			
			ball.setyPosition(ball.getyPosition());
			ball.setxPosition(newX); 		
			ball.setAngle(360D - ball.getAngle()); 
			checkBallDirection(ball);
			break;
			
		case 12: //newY >= board.yMax
//			System.out.println("marke12  "); // debugging
			dummyBall.setyPosition(board.getBoardYMax());
			dummyBall.setxPosition(newX);
			displayBoard( dummyBall, board);
			
			ball.setyPosition(ball.getyPosition()); 
			ball.setxPosition(newX); 			
			ball.setAngle(360D - ball.getAngle()); 
			checkBallDirection(ball);
			break;
			
		case 21: //newX >= board.xMax
//			System.out.println("marke21"); // debugging
			dummyBall.setxPosition(board.getBoardXMax());
			dummyBall.setyPosition(newY);
			displayBoard( dummyBall, board);
			
			ball.setxPosition(ball.getxPosition());
			ball.setyPosition(newY); 		
			ball.setAngle(180D - ball.getAngle()); 
			checkBallDirection(ball);
			break;

		default:
			break;
		}
	}
	
}
