package pong; 

public class Ball {
	
	// Attributes
	private	double xPosition;
	private double yPosition;
	private double angle;
	
	// Constructor
	Ball( double x, double y, double angle ) {
		this.xPosition = x;
		this.yPosition = y;
		this.angle = angle;
	}
	
	
	// toString
	public String toString() {
		return "This ball has coordinates [x,y] = [ " + 
				this.xPosition + " , " +	this.yPosition + "] " + 
				" and a direction of motion of " + this.angle + "°." ;
	}

	// getter
	public double getxPosition() {
		return xPosition;
	}


	public void setxPosition(double xPosition) {
		this.xPosition = xPosition;
	}


	public double getyPosition() {
		return yPosition;
	}


	public void setyPosition(double yPosition) {
		this.yPosition = yPosition;
	}


	public double getAngle() {
		return angle;
	}


	public void setAngle(double angle) {
		this.angle = angle;
	}
	
	
	//static methods
	public static Ball createBallOnBoard( double x, double y, double angle, Board board ) {
		Ball ball1 = new Ball( x, y, angle );
		return ball1;
	}
	

	public static Ball createBallOnBoard( Board board1 ) {
		double x = Math.random() * (board1.getBoardXMax() - board1.getBoardXMin())
				+ board1.getBoardXMin();
		double y = Math.random() * (board1.getBoardYMax() - board1.getBoardYMin()) 
				+ board1.getBoardYMin();
		double angle = Math.random() * 360D;
		
		Ball ball1 = new Ball( x, y, angle );
		return ball1;
	}
}
