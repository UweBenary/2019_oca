package pong; 

public class UnitTests {
	
	/**
	 * main
	 *    includes unit tests
	 */
	public static void main(String[] args) {
		
		/*
		 * Test class Board
		 */
//		testBoardConstructor();
//		System.out.println();
//		
//		testCheckBoardXDim();
//		System.out.println();
//
//		testCheckBoardYDim();
//		System.out.println();
//
//		testCreateBoard();
//		System.out.println();
//		
//		testBoardGetters();
//		System.out.println();
		
		/*
		 * Test class Ball
		 */
		testBallConstructor();
		System.out.println();
		
	}
	
	/**
	 * Test initialization of board
	 */
	static void testBoardConstructor() {
		
		try {
			Board b1 = new Board(0, 10, -3, 20);			
			System.out.println(b1);
			
			System.out.println("--> Test of Board constructor passed.");	


		} catch (RuntimeException  e) {
			System.out.println("--> Error! Test of Board constructor  NOT passed.");
		}		
	}
	
	/**
	 * Test checkBoardXDim()
	 */	
	static void testCheckBoardXDim() {
		if(true == Board.checkBoardXDim(-5,-1) && 
				true == Board.checkBoardXDim(0,5) &&
						false == Board.checkBoardXDim(-1,-5) &&
								false == Board.checkBoardXDim(5,1) &&
										false == Board.checkBoardXDim(0,2) &&
												false == Board.checkBoardXDim(-1,1)) {
			System.out.println("--> Test of 'testCheckBoardXDim' passed.");	
		
		} else {
			System.out.println("--> Error! Test of 'testCheckBoardXDim'  NOT passed.");
		}	
	}
	
	/**
	 * Test checkBoardYDim()
	 */	
	static void testCheckBoardYDim() {
		if(true == Board.checkBoardYDim(-5,-1) && 
				true == Board.checkBoardYDim(0,5) &&
						false == Board.checkBoardYDim(-1,-5) &&
								false == Board.checkBoardYDim(5,1) &&
										false == Board.checkBoardYDim(0,2) &&
												false == Board.checkBoardYDim(-1,1)) {
			System.out.println("--> Test of 'testCheckBoardXDim' passed.");	
		
		} else {
			System.out.println("--> Error! Test of 'testCheckBoardXDim'  NOT passed.");
		}	
	}	
	
	/**
	 * Test createBoard()
	 */
	static void testCreateBoard() {
		
		try {
			Board b1 = Board.createBoard();			
			System.out.println(b1);
			
			System.out.println("--> Test of 'createBoard()' passed.");	


		} catch (RuntimeException  e) {
			System.out.println("--> Error! Test of 'createBoard()'  NOT passed.");
		}		
	}
	
	/**
	 * Test Board getters
	 */
	static void testBoardGetters() {
		
		final int expectedXMin = 0;
		final int expectedXMax = 5;
		final int expectedYMin = -2;
		final int expectedYMax = 1;
		
		Board b1 = new Board(expectedXMin, expectedXMax, expectedYMin, expectedYMax);			
		
		
		int actualXMin = b1.getBoardXMin(); 
		int actualXMax = b1.getBoardXMax();
		int actualYMin = b1.getBoardYMin();
		int actualYMax = b1.getBoardYMax();
		
		System.out.println(actualXMin + " " + actualXMax + " " + actualYMin + " " + actualYMax);
		
		if(actualXMin == expectedXMin && 
				actualXMax == expectedXMax &&
						actualYMin == expectedYMin &&
								actualYMax == expectedYMax) {
			System.out.println("--> Test of 'testBoardGetters' passed.");	
		
		} else {
			System.out.println("--> Error! Test of 'testBoardGetters'  NOT passed.");
		}	
	}
	
	/**
	 * Test initialization of ball
	 */
	static void testBallConstructor() {
		
		try {
			Ball b1 = new Ball(1, 1, 20);			
			System.out.println(b1);
			
			System.out.println("--> Test of Ball constructor passed.");	


		} catch (RuntimeException  e) {
			System.out.println("--> Error! Test of Ball constructor  NOT passed.");
		}		
	}
	
}
