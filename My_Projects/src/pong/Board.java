package pong; 

public class Board {

	// Attributes
	private int xMin;
	private int yMin;
	
	private int xMax;
	private int yMax;
	
	// Constructor
	Board( int xMin, int xMax, int yMin, int yMax ) {
		this.xMin = xMin;
		this.xMax = xMax;
		this.yMin = yMin;
		this.yMax = yMax;
	}
	
	// toString
	public String toString() {
		return "This board has width x hight: [ " + 
				this.xMin + " , " +	this.xMax + "] x [" + 
				this.yMin + " , " +	this.yMax + "] " ;
	}
	
	//getter
	public int getBoardXMin() {
		return this.xMin;		
	}
	
	public int getBoardXMax() {
		return this.xMax;		
	}
	
	public int getBoardYMin() {
		return this.yMin;		
	}
	
	public int getBoardYMax() {
		return this.yMax;		
	}
	
	//Static method
	public static Board createBoard() {
		
		System.out.println("Let's create a game board.");
		System.out.println("  Define its dimensions left to right and bottom to top.");
		System.out.println();
		
		int left, right, bottom, top; 
		boolean check;
		
		do {
			left = UtilsLib.getIntInput("Left side (Typ int): ");
			right = UtilsLib.getIntInput("Right side (Typ int): ");
			check = checkBoardXDim( left, right);				
		} while ( !check ); 
		
		do {
			bottom = UtilsLib.getIntInput("Bottom (Typ int): ");
			top = UtilsLib.getIntInput("Top (Typ int): ");
			check = checkBoardYDim( bottom, top);		
		} while ( !check ); 

		Board board = new Board( left, right, bottom, top );
		
		return board;
	}
		
	public static boolean checkBoardXDim(int left, int right) {
		if ( left > right ) {
			System.out.println( " Input error: left side > right side");
			return false;
		} else if (right - left < 3) {
			System.out.println(" Input error: board is too small for the game (left to rigth > 3).");
			return false;
		} else {
			return true;
		}		
	}
	
	public static boolean checkBoardYDim(int bottom, int top) {
		if ( bottom > top ) {
			System.out.println( " Input error: bottom > top");
			return false;
		} else if (top - bottom < 3) {
			System.out.println(" Input error: board is too small for the game (bottom to top > 3).");
			return false;
		} else {
			return true;
		}		
	}

}
