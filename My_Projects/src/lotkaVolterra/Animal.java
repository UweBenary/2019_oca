package lotkaVolterra;

public abstract class Animal {

	final int CALORIES_OF_HARE = 5;
	final int CALORIES_OF_LYNX = 10;
	final int MIN_CALORIES_TO_LIVE = -1 * StartMain.board.getX_MIN() * StartMain.board.getX_MAX() /5;
	
	private final long ID = System.nanoTime();
	private int x;
	private int y;
	private int calories;

	protected void setX(int x) {
		this.x = x;
	}

	protected void setY(int y) {
		this.y = y;
	}

	protected Field getMyField() {
		return StartMain.board.getField(x,y);
	}
	
	/**
	 * 
	 * @return StartMain.board.getFreeRandomNeighborField(x, y)
	 */
	protected Field getDestination() {
		return StartMain.board.getFreeRandomNeighborField(x, y);
	}
	
	protected void addCalories(int cal) {
		calories = getCalories() + cal;
	}
	
	protected int getCalories() {
		return calories;
	}
	

	@Override
	public boolean equals(Object other) {
		if ( other instanceof Animal ) {
			return ID == ((Animal) other).ID ;
		} else {
			return false;
		}
	}
			
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + " (ID: " + ID +") @ [" + x + "," + y + "]";
	}
	

	protected boolean move() {
		System.out.println("Should not execute"); return false;
//	
//		Field myPlace = getMyField();
//		Field destination = getDestination();
//		
//		if ( destination == null ) {
//			return false;
//		} else {
//			myPlace.delete( this );
//			destination.add( this );
//			setX( destination.getX() );
//			setY( destination.getY() );
//			return true;
//		}
	}	
	
	protected void devour(Animal animal) {
		this.addCalories( Math.max( animal.getCalories(), CALORIES_OF_LYNX ) );
		animal.die();
	}
	
	/*
	 * abstract methods
	 */
	abstract protected void act();
	
	abstract protected Animal createOffspring();	
	
	abstract protected void die();

}
