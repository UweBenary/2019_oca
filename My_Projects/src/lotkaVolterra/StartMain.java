package lotkaVolterra;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class StartMain {

	static List<Hare> hares = new ArrayList<Hare>();
	static List<Lynx> lynxes = new ArrayList<Lynx>();
	static Board board = new Board();
	
//	static List<Integer> haresCount = new ArrayList<Integer>();
//	static List<Integer> lynxesCount = new ArrayList<Integer>();

	public static void main(String[] args) {
		
		final int RUNS = 10000; 
    	display(board);
    	
    	placeHares(15);
    	placeLynxes(15);

    	boolean moved;
    	int count = 0;
		
    	do {
    		Animal animal = chooseRandomAnimal();
    		
    		moved = animal.move();
    		
    		if (moved) {
    			animal.act();
    			
			} else /*if (animal instanceof Hare)*/ {
				animal.die();
			}
    		
    		display(board);
//    		haresCount.add( hares.size() );
//    		lynxesCount.add( lynxes.size() );
    		
    		count++;
		} while (count < RUNS && hares.size() > 1 && lynxes.size() > 1);
    	
//    	displayGraph(); // future project
    	
	}

	private static Animal chooseRandomAnimal() {
    	Random rand = new Random();
		int hSize = hares.size();
		int lSize = lynxes.size();
		
//		if ( rand.nextInt( hSize + lSize ) < hSize ) {
//			return lynxes.get( rand.nextInt( lSize ) );
//		} else {
//			return hares.get( rand.nextInt( hSize ) );
//		}
		
 		if ( rand.nextBoolean() ) {
			return hares.get( rand.nextInt( hSize ) );
		} else {
			return lynxes.get( rand.nextInt( lSize ) );
		}
	}

	private static int getRandomX () {
		Random rand = new Random();
		int min = board.getX_MIN() + 1; //excludes X_MIN
		int max = board.getX_MAX();
		return rand.nextInt(max - min) + min;
	}
	
	private static int getRandomY () {
		Random rand = new Random();
		int min = board.getY_MIN() + 1; //excludes X_MIN
		int max = board.getY_MAX();
		return rand.nextInt(max - min) + min;
	}
	
	private static void placeLynxes(int max) {
		boolean moved = false;
		int x, y;
    	for (int i = 0; i < max; i++) {
//    		do {
    			x = getRandomX();
    			y = getRandomY();
    			if ( board.getField(x, y) != null ) {
					Lynx l = new Lynx(x,y);
					moved = l.move();	
					if ( !moved ) {
						l.die();
					};
    			}
//			} while ( !moved );
		}
	}

	private static void placeHares(int max) {
		boolean moved = false;
		int x, y;
    	for (int i = 0; i < max; i++) {
//    		do {
    			x = getRandomX();
    			y = getRandomY();
    			if ( board.getField(x, y) != null ) {
					Hare h = new Hare(x,y);
					moved = h.move();	
					if ( !moved ) {
						h.die();
					};
    			}
//			} while ( !moved );
		}
	}

	static void display(Board board) {
    	
    	board.display();
    	
		System.out.printf("Hares: %d     Lynxes: %d%n", 
				StartMain.hares.size(), 
				StartMain.lynxes.size() );
		System.out.println();
		
		try {						// Pause shortly
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
