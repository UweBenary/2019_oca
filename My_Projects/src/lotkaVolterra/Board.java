package lotkaVolterra;

import java.util.ArrayList;
import java.util.Random;
import java.util.function.Predicate;

public class Board {

	private final int X_MIN = 0;
	private final int Y_MIN = 0;
	private final int X_MAX;
	private final int Y_MAX;
	private Field[][] fields;
	private final int OBSTACLE_FREQ = /* 1 in */ 20;
	
	Board() {
		X_MAX = getBoardwidth();
		Y_MAX = getBoardHight();
		fields = new Field[ X_MAX ][ Y_MAX ];
		initFields();
	}

	private int getBoardHight() {
		return 10;
	}

	private int getBoardwidth() {
		return 20;
	}
	
	private void initFields() {
		Random rand = new Random();
		
		for (int i = X_MIN+1; i < X_MAX; i++) { 		// exclude X_MIN
			for (int j = Y_MIN+1; j < Y_MAX; j++) {		// exclude Y_MIN
				
				if (rand.nextInt( OBSTACLE_FREQ ) > 0) {
					fields[i][j] = new Field(i,j);
				}
			}
		}
		
		/*
		 * set field(0,0) 
		 * is only used as initial field for initialisation of new animal
		 * is never a free neighborField 
		 */
		fields[0][0] = new Field(0,0);
	}
	
	public int getX_MIN() {
		return X_MIN;
	}

	public int getY_MIN() {
		return Y_MIN;
	}

	public int getX_MAX() {
		return X_MAX;
	}

	public int getY_MAX() {
		return Y_MAX;
	}

	Field getField(int x, int y) {
		return fields[x][y];
	}
	
	Field getFreeRandomNeighborField(int x, int y) {
		
		ArrayList<Field> neighbors = getNeighbors(x, y);		
		
		Predicate<Field> occupied = field -> field.countHares() + field.countLynxes() > 0;
		neighbors.removeIf( occupied );
	
		if ( neighbors.size() > 0) {

			int randomIdx = new Random().nextInt( neighbors.size() );
			return neighbors.get( randomIdx );
		} else {
			return null;
		}		
	}
		
	ArrayList<Field> getNeighbors(int x, int y) {
		
		ArrayList<Field> neighbors = new ArrayList<Field>();
		
		for (int i = Math.max(x-1, X_MIN); i < Math.min(x+2, X_MAX); i++) {
			for (int j = Math.max(y-1, Y_MIN); j < Math.min(y+2, Y_MAX); j++) {
				
				if ( ! (i == 0 & j == 0) ) { // initialisation field(0,0) is never a neighbor
					if ( ! (i == x & j == y) ) {
						if ( fields[i][j] != null) {
							neighbors.add( fields[i][j] );
						}
					}
				}				
			}
		}
		return neighbors;
	}

	void display() {
		
		for (int y = Y_MAX; y > -1; y--) {
			for (int x = 0; x < X_MAX+1; x++) {
				
				if (y == Y_MAX || y == 0 || x == 0 || x == X_MAX) {
					System.out.print("# ");
								
				} else if ( fields[x][y] == null ) {
					System.out.print("# ");
					
				} else {
					System.out.print( fields[x][y].getLabel() + " ");
				}
			}
			System.out.println();
		}
	}
	

	/*
	 * 
	 * 
	 * 
	 * 
	 * UnitTest
	 * 
	 * 
	 * 
	 * 
	 */
	public static void main(String[] args) {
				
		printStartScreen();
		
		Board b = new Board();
		b.display();
				
		testGetField();
		testGetFreeRandomNeighborField();
	}

	private static void printStartScreen() {	
		System.out.println();
		System.out.println("+++++++++++++++++++++++");
		System.out.println("+++    Unit Tests   +++");
		System.out.println("+++++++++++++++++++++++");
		System.out.println();
	}

	private static void printTestResults(Object expected, Object result) {
        String nameOfEnvokingMethod = Thread.currentThread().getStackTrace()[2].getMethodName(); 
        
		if( result.equals(expected) ) {
			System.out.println(nameOfEnvokingMethod + " -> Test passed.");
		
		} else {
			System.out.println(nameOfEnvokingMethod + " -> Test FAILED! Expected: " + expected + ", Result: " + result);
			System.err.println(nameOfEnvokingMethod + " -> Test FAILED!");
		}
	}
	
	private static void testGetField() {
		Board b = new Board();
		
		System.out.print("Test1 ");
		Field result = b.getField(0, 1);
		Field expected = null;
		
		if( result == expected ) {
			System.out.println("testGetField -> Test passed.");
		} else {
			System.out.println("testGetField -> Test FAILED! Expected: " + expected + ", Result: " + result);
		}
		
		System.out.print("Test2 ");
		String result2 = b.getField(2, 3).toString();
		String expected2 = "Field [2,3] with [null, null]";
		
		printTestResults(expected2, result2);
	}

	private static void testGetFreeRandomNeighborField() {
		Board b = new Board();
		
		String result = b.getFreeRandomNeighborField(0, 0).toString();
		String expected = "Field [1,1] with [null, null]";
		
		printTestResults(expected, result);
	}
}
