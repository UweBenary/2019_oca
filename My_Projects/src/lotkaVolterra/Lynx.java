package lotkaVolterra;

import java.util.ArrayList;
import java.util.Random;
import java.util.function.Predicate;

public class Lynx extends Animal {
	

	private Lynx() { // only for UnitTesting
		this(0,0);
	}
	
	Lynx(int x, int y) {
		setX(x);
		setY(y);
		addCalories( CALORIES_OF_LYNX );
		
		StartMain.lynxes.add( this );
		getMyField().add( this );
	}


	@Override
	protected void act() {

		addCalories(-1);

		if (getCalories() < MIN_CALORIES_TO_LIVE) {
			die();
		}
		Field myField = getMyField();
		
		if (myField.countHares() == 1) {
			devour( myField.getHares().get(0) );
		}
	
		if (myField.countLynxes() == 2) {
			int cal0 = myField.getLynxes().get(0).getCalories();
			int cal1 = myField.getLynxes().get(1).getCalories();
			
			if (cal0 > 0 & cal1 > 0) {
				createOffspring();
			} else {
//				fight( myField.getLynxes() );
			}			
		}		
	}
	
	private void fight(ArrayList<Lynx> lynxes) {
		boolean isLynx0Stronger = lynxes.get(0).getCalories() > lynxes.get(1).getCalories();
		if ( isLynx0Stronger ) {
			lynxes.get(0).devour( lynxes.get(1) );			
		} else {
			lynxes.get(1).devour( lynxes.get(0) );	
		}		
	}


	@Override
	protected Animal createOffspring() {		

		Field placeOfBirth = getDestination();
		
		if ( placeOfBirth == null ) {
			return null;
			
		} else {
			int x = placeOfBirth.getX();
			int y = placeOfBirth.getY();
			Lynx offspring = new Lynx(x, y);
			return offspring;
		}
	}
		
	@Override
	protected void die() {
		getMyField().delete( this );
		StartMain.lynxes.remove( this );	

	};
	
	@Override
	protected boolean move() {
		Field myPlace = getMyField();
		Field destination = getDestination4Lynx();
		
		if ( destination == null ) {
			return false;
		} else {
			myPlace.delete( this );
			destination.add( this );
			setX( destination.getX() );
			setY( destination.getY() );
			return true;
		}
	}
	
	private Field getDestination4Lynx() {
		int x = getMyField().getX();
		int y = getMyField().getY();
		ArrayList<Field> neighbors = StartMain.board.getNeighbors(x, y);		
		
		Predicate<Field> occupied = field -> field.countHares() + field.countLynxes() > 1;
		neighbors.removeIf( occupied );

		if ( neighbors.size() > 0) {
			
			Field result = null;
			
//			if ( getCalories() > 0 ) {
//				result = findLynx( new ArrayList<Field>(neighbors) );
//			}
//				
//			if (result==null) {
//				result = findHare( new ArrayList<Field>(neighbors) );
//			}
			
//			if (getCalories() < 0) {
//				result = findHare( new ArrayList<Field>(neighbors) );
//			}
			
			result = findHare( new ArrayList<Field>(neighbors) );
			
//			if (result==null) {
//				int randomIdx = new Random().nextInt( neighbors.size() );
//				result = neighbors.get( randomIdx );
//			}
			
			if (result==null && getCalories() < 0) {
				neighbors.removeIf( field -> field.countLynxes() > 0);
			}
			
			if (result==null) {
				int randomIdx = new Random().nextInt( neighbors.size() );
				result = neighbors.get( randomIdx );
			}
			
			return result;
			
		} else {
			return null;
		}			
	}

	private Field findLynx(ArrayList<Field> neighbors) {
		
		neighbors.removeIf( field -> field.countLynxes() != 1);
		
		if (neighbors.size() > 0) {
			int randomIdx = new Random().nextInt( neighbors.size() );
			return neighbors.get( randomIdx );
		} else {
			return null;
		}
	}
	
	private Field findHare(ArrayList<Field> neighbors) {
		
		neighbors.removeIf( field -> field.countHares() > 0);
		
		if (neighbors.size() > 0) {
			int randomIdx = new Random().nextInt( neighbors.size() );
			return neighbors.get( randomIdx );
		} else {
			return null;
		}
	}

	/*
	 * 
	 * 
	 * 
	 * 
	 * UnitTest
	 * 
	 * 
	 * 
	 * 
	 */
	public static void main(String[] args) {
		
		printStartScreen();
		
		testConstructor();

		callToString();
		
		testIdenticalLynxWithEquals();
		testDifferentLynxesWithEquals();

		testDie();
		
		testDevour();
		
		testMove();
		testAct();
		testCreateOffspring();
	}
	
	private static void printStartScreen() {	
		System.out.println();
		System.out.println("+++++++++++++++++++++++");
		System.out.println("+++    Unit Tests   +++");
		System.out.println("+++++++++++++++++++++++");
		System.out.println();
	}
	
	private static void printTestResults(Object expected, Object result) {
        String nameOfEnvokingMethod = Thread.currentThread().getStackTrace()[2].getMethodName(); 
        
		if( result.equals(expected) ) {
			System.out.println(nameOfEnvokingMethod + " -> Test passed.");
		
		} else {
			System.out.println(nameOfEnvokingMethod + " -> Test FAILED! Expected: " + expected + ", Result: " + result);
			System.err.println(nameOfEnvokingMethod + " -> Test FAILED!");
		}
	}
	
	private static void testConstructor() {
		Lynx l = new Lynx();
		int result = StartMain.lynxes.size();		
		int expected = 1;
				
		printTestResults(expected, result);	
		l.die();
	}
	
	private static void callToString() {
		System.out.print("callToString -> ");
		Lynx l = new Lynx(); 
		System.out.println( l );
		l.die();
	}
	
	private static void testIdenticalLynxWithEquals() {
		
		Lynx l = new Lynx();
		boolean expected = true;
		boolean result = l.equals(l);

		printTestResults(expected, result);
		l.die();
	}
	
	private static void testDifferentLynxesWithEquals() {
		Lynx l = new Lynx();
		Lynx l2 = new Lynx();
		
		boolean expected = false;
		boolean result = l.equals(l2);

		printTestResults(expected, result);	
		l.die();
		l2.die();
	}

	private static void testDie() { 
		Lynx l = new Lynx();
		l.die();	
			
		int expected = 0;
		int result = StartMain.lynxes.size();		
        
		printTestResults(expected, result);
		l.die();
	}

	static void testDevour() {
		Hare h = new Hare(0, 0);
		Lynx l = new Lynx();
		l.devour(h);	
		
		System.out.print("Test1 ");
		int expected = 0;
		int result = StartMain.hares.size();		
        
		printTestResults(expected, result);
		
		System.out.print("Test2 ");
		String result2 = l.getMyField().toString();
		String expected2 = "Field [0,0] with [null, " + l.toString() + "]";			
        
		printTestResults(expected2, result2);
		l.die();
		h.die();
	}
	
	private static void testCreateOffspring() {
		Lynx l = new Lynx();
		Lynx l2 = (Lynx) l.createOffspring();		
		
		System.out.print("Test1 ");
		int expected = 2;
		int result = StartMain.lynxes.size();		
        
		printTestResults(expected, result);
		
		System.out.print("Test2 ");
		String result2 = l2.getMyField().toString();
		String expected2 = "Field [1,1] with [" + l2.toString() + ", null]";			
        
		printTestResults(expected2, result2);
		
		l.die();
		l2.die();
	}

	private static void testAct() {
		Lynx l = new Lynx();
		Lynx l2 = new Lynx();
		l.act();			
		
		System.out.print("Test1 ");
		int result = StartMain.lynxes.size();
		int expected = 3;
	        
		printTestResults(expected, result);

		System.out.print("Call field(1,1).toString() ");
		System.out.println( StartMain.board.getField(1, 1).toString() );
		l.getDestination().getLynxes().get(0).die(); //removes offspring
		
		l.die();
		l = new Lynx();
		l2.addCalories(-10);
		l.act();
		
		System.out.print("Test2 ");
		String result2 = l.getMyField().toString();
		String expected2 = "Field [0,0] with [" + l.toString() + ", null]";
	        
		printTestResults(expected2, result2);
		System.out.print("Call field(1,1).toString() ");
		System.out.println( StartMain.board.getField(1, 1).toString() );
		
		System.out.print("Test3 ");
		int result3 = l.getCalories();
		int expected3 = l.CALORIES_OF_LYNX * 2 - 1;
	        
		printTestResults(expected3, result3);
		l2.die();
		
		System.out.print("Test4 ");
		Hare h = new Hare(0, 0);
		l.act();
		
		int result4 = l.getCalories();
		int expected4 = l.CALORIES_OF_HARE + expected3 - 1;
	  	        
		printTestResults(expected4, result4);
		
		h.die();
		l.die();
	}

	private static void testMove() {
		Lynx l = new Lynx();
		Field f = l.getMyField();
		l.move( );	
				
		System.out.print("Test1 ");		
		String result = l.getMyField().toString();
		String expected = "Field [1,1] with [" + l.toString() + ", null]";		

		printTestResults(expected, result);

		System.out.print("Test2 ");
		String result2 = f.toString();
		String expected2 = "Field [0,0] with [null, null]";		

		printTestResults(expected2, result2);	
		l.die();
	}
}
