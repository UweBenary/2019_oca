package lotkaVolterra;

import java.util.ArrayList;
import java.util.Random;
import java.util.function.Predicate;

public class Hare extends Animal {

	
	private Hare() { // only for UnitTesting
		this(0,0);
	}
	
	Hare(int x, int y) {
		setX(x);
		setY(y);
		addCalories(CALORIES_OF_HARE);
		
		StartMain.hares.add( this );
		getMyField().add( this );
	}
	
	@Override
	protected void act() {
		
		Field myField = getMyField();
		
		if (myField.countHares() == 2) {
			createOffspring();
		}
		
		if (myField.countLynxes() == 1) {
			Lynx lynx = myField.getLynxes().get(0);
			lynx.devour( this );
		}
	}

	@Override
	protected Animal createOffspring() {	
		
		Field placeOfBirth = getDestination();
		
		if ( placeOfBirth == null ) {
			return null;
			
		} else {
			int x = placeOfBirth.getX();
			int y = placeOfBirth.getY();
			Hare offspring = new Hare(x, y);
			return offspring;
		}
	}

	@Override
	protected void devour(Animal animal) {
		throw new UnsupportedOperationException("Hare cannot devour!"); // Hare cannot devour 
	}

	@Override
	protected void die() {
		getMyField().delete( this );
		StartMain.hares.remove( this );	
	};

	@Override
	protected boolean move() {
		Field myPlace = getMyField();
		Field destination = getDestination4Hare();
		
		if ( destination == null ) {
			return false;
		} else {
			myPlace.delete( this );
			destination.add( this );
			setX( destination.getX() );
			setY( destination.getY() );
			return true;
		}
	}
	
	private Field getDestination4Hare() {
		int x = getMyField().getX();
		int y = getMyField().getY();
		ArrayList<Field> neighbors = StartMain.board.getNeighbors(x, y);		
				
		Predicate<Field> occupied = field -> field.countHares() + field.countLynxes() > 1;
		neighbors.removeIf( occupied );
		
		if ( neighbors.size() > 0) {
			neighbors.removeIf( field -> field.countLynxes() > 0);
		}
		
		if ( neighbors.size() > 0) {
			
			ArrayList<Field> n = new ArrayList<Field> (neighbors);
			n.removeIf( field -> field.countHares() > 0 );
			if (n.size() > 0 && n.size() < 3) {
					int randomIdx = new Random().nextInt( n.size() );
					return n.get( randomIdx );
			}			
			
			int randomIdx = new Random().nextInt( neighbors.size() );
			return neighbors.get( randomIdx );

		} else {
			return null;
		}			
	}
	
	

	/*
	 * 
	 * 
	 * 
	 * 
	 * UnitTest
	 * 
	 * 
	 * 
	 * 
	 */
	public static void main(String[] args) {
		
		printStartScreen();
		
		testConstructor();
		
		callToString();

		testIdenticalHareWithEquals();
		testDifferentHaresWithEquals();
		testDevourShouldFail();
		
		testDie();
		
		testMove();
		testAct();
		testCreateOffspring();
	}

	private static void printStartScreen() {	
		System.out.println();
		System.out.println("+++++++++++++++++++++++");
		System.out.println("+++    Unit Tests   +++");
		System.out.println("+++++++++++++++++++++++");
		System.out.println();
	}

	private static void printTestResults(Object expected, Object result) {
        String nameOfEnvokingMethod = Thread.currentThread().getStackTrace()[2].getMethodName(); 
        
		if( result.equals(expected) ) {
			System.out.println(nameOfEnvokingMethod + " -> Test passed.");
		} else {
			System.out.println(nameOfEnvokingMethod + " -> Test FAILED! Expected: " + expected + ", Result: " + result);
			System.err.println(nameOfEnvokingMethod + " -> Test FAILED!");
		}
	}

	private static void testConstructor() {		
		Hare h = new Hare();
		int result = StartMain.hares.size();		
		int expected = 1;
				
		printTestResults(expected, result);
		h.die();
	}
	
	private static void callToString() {
		System.out.print("callToString -> ");
		Hare h = new Hare(); 
		System.out.println( h );
		h.die();
	}
	
	private static void testIdenticalHareWithEquals() {		
		Hare h = new Hare();
		boolean expected = true;
		boolean result = h.equals(h);

		printTestResults(expected, result);
		h.die();
	}
	
	private static void testDifferentHaresWithEquals() {
		Hare h = new Hare();
		Hare h2 = new Hare();
		
		boolean expected = false;
		boolean result = h.equals(h2);

		printTestResults(expected, result);	
		h.die();
		h2.die();
	}

	static void testDevourShouldFail() {
		Hare h = new Hare();
		Hare h2 = new Hare();
		try {			
			h.devour( h2 );
			
			System.out.println("testDevourShouldFail -> Test FAILED! Expected: UnsupportedOperationException");
			System.err.println("testDevourShouldFail -> Test FAILED!");
			
		} catch (UnsupportedOperationException e) {
			System.out.println("testDevourShouldFail -> Test passed.");
		}
		h.die();
		h2.die();
	}

	private static void testDie() {
        Hare h = new Hare();
		h.die();	
			
		int expected = 0;
		int result = StartMain.hares.size();		
        
		printTestResults(expected, result);
	}
	
	private static void testCreateOffspring() {
		Hare h = new Hare();
		Animal a = h.createOffspring();			
		
		System.out.print("Test1 ");
		String result = a.getMyField().toString();
		String expected = "Field [1,1] with [" + a.toString() + ", null]";			
        
		printTestResults(expected, result);
	
		
		h.die();
		a.die();
		
		System.out.print("Test2 ");
		int result2 = StartMain.hares.size();
		int expected2 = 0;			
        
		printTestResults(expected2, result2);
	}

	private static void testAct() {
		Hare h = new Hare();
		Hare h2 = new Hare();
		h.act();	
		
		System.out.print("Test1 ");
		int result = StartMain.hares.size();
		int expected = 3;
	        
		printTestResults(expected, result);

		System.out.print("Call field(1,1).toString() ");
		System.out.println( StartMain.board.getField(1, 1).toString() );
		h2.die();
		h.getDestination().getHares().get(0).die();//removes offspring
		
		System.out.print("Test2 ");

		Lynx l = new Lynx(0, 0);
		h.act();
		
		String result2 = h.getMyField().toString();
		String expected2 = "Field [0,0] with [null, " + l.toString() + "]";
	        
		printTestResults(expected2, result2);
		
		h.die();
		l.die();
	}

	private static void testMove() {
		Hare h = new Hare();
		Field f = h.getMyField();
		
		System.out.print("Test1 ");
		h.move( );			
		String result = h.getMyField().toString();
		String expected = "Field [1,1] with [" + h.toString() + ", null]";		

		printTestResults(expected, result);

		System.out.print("Test2 ");
		String result2 = f.toString();
		String expected2 = "Field [0,0] with [null, null]";		

		printTestResults(expected2, result2);		
		h.die();
	}
}