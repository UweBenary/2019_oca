package lotkaVolterra;

import java.util.ArrayList;
import java.util.Arrays;

public class Field {

	private final int X;
	private final int Y;
	private Animal[] inhabitants = new Animal[2];
	
	Field(int x, int y) {
		X = x;
		Y = y;
	}

	public int getX() {
		return X;
	}

	public int getY() {
		return Y;
	}

	void add(Animal animal) {
		if ( countHares() + countLynxes() < 2 ) {
			for (int i = 0; i < inhabitants.length; i++) {
				if (inhabitants[i] == null) {
					inhabitants[i] = animal;
					break;
				}
			}
		}
	}
	
	void delete(Animal animal) {
		for (int i = 0; i < inhabitants.length; i++) {
			if ( inhabitants[i]!=null && inhabitants[i].equals(animal) ) {
				inhabitants[i] = null;
				break;
			}
		}
	}
	
	ArrayList<Hare> getHares() {
		ArrayList<Hare> result = new ArrayList<Hare>();
		for (int i = 0; i < inhabitants.length; i++) {
			if ( inhabitants[i] != null && inhabitants[i] instanceof Hare ) {
				result.add( (Hare) inhabitants[i] );
			}
		}
		return result;
	}
	
	ArrayList<Lynx> getLynxes() {
		ArrayList<Lynx> result = new ArrayList<Lynx>();
		for (int i = 0; i < inhabitants.length; i++) {
			if ( inhabitants[i] != null && inhabitants[i] instanceof Lynx ) {
				result.add( (Lynx) inhabitants[i] );
			}
		}
		return result;
	}

	String getLabel() {				
		int haresCount = countHares();
		int lynxesCount = countLynxes();
		
		if (lynxesCount == 0) {
			switch ( haresCount ) {
			case 0:
				return " ";
			case 1:
				return "h";
			case 2:
				return "H";
			default:
				break;
			}
		}		
		if (lynxesCount == 1 ) {
			return "L";
		}
		if (lynxesCount == 2 ) {
			return "U";
		}
		
		/*
		 * should not be reachable
		 */
		throw new UnsupportedOperationException("Code problem: This point in LotkaVolterra.Field.getLabel() should not be reachable!"); 
	}

	int countHares() {
		int count = 0;
		for (int i = 0; i < inhabitants.length; i++) {
			if ( inhabitants[i] != null && inhabitants[i] instanceof Hare ) {
				count++;
			}
		}
		return count;
	}
	
	int countLynxes() {
		int count = 0;
		for (int i = 0; i < inhabitants.length; i++) {
			if ( inhabitants[i] != null && inhabitants[i] instanceof Lynx ) {
				count++;
			}
		}
		return count;
	}
	
	@Override
	public String toString() {
		return "Field [" + X + "," + Y + "] with " + Arrays.toString(inhabitants);
	}
	
	/*
	 * 
	 * 
	 * 
	 * 
	 * UnitTest
	 * 
	 * 
	 * 
	 * 
	 */
	public static void main(String[] args) {
		
		printStartScreen();
		
		callToString();

		
		testAdd();
		testDelete();
		testCountHares();
		testCountLynxes();
		testGetLabel();
		testGetHares();
		testGetLynxes();
	
	}

	private static void printStartScreen() {	
		System.out.println();
		System.out.println("+++++++++++++++++++++++");
		System.out.println("+++    Unit Tests   +++");
		System.out.println("+++++++++++++++++++++++");
		System.out.println();
	}

	private static void printTestResults(Object expected, Object result) {
        String nameOfEnvokingMethod = Thread.currentThread().getStackTrace()[2].getMethodName(); 
        
		if( result.equals(expected) ) {
			System.out.println(nameOfEnvokingMethod + " -> Test passed.");
		
		} else {
			System.out.println(nameOfEnvokingMethod + " -> Test FAILED! Expected: " + expected + ", Result: " + result);
			System.err.println(nameOfEnvokingMethod + " -> Test FAILED!");
		}
	}
	
	private static void callToString() {
		System.out.print("Call1 callToString -> ");
		Field f = new Field(1, 2);
		System.out.println(f);

		System.out.print("Call2 callToString -> ");
		Hare h = new Hare(0, 0);
		f.add( h );
		Lynx l = new Lynx(0, 0);
		f.add( l );
		System.out.println(f);
		h.die();
		l.die();
	}
	
	private static void testAdd() {
		Field f = new Field(1, 2);
		
		System.out.print("Test1 ");	
		Hare h = new Hare(0, 0);
		f.add( h );
		Animal result = f.inhabitants[0];
		Animal expected = h;
		

		printTestResults(expected, result);		
		
		System.out.print("Test2 ");
		Lynx l = new Lynx(0, 0);
		f.add(l);
		result = f.inhabitants[1];
		expected = l;
		
		printTestResults(expected, result);
		h.die();
		l.die();
	}

	private static void testDelete() {
		Field f = new Field(1, 2);
		
		System.out.print("Test1 ");	
		Hare h = new Hare(0, 0);
		Lynx l = new Lynx(0, 0);
		f.add(h);
		f.add(l);
		f.delete(l);
		
		Animal result = f.inhabitants[1];
		Animal expected = null;		

      	if( result == expected ) {
			System.out.println("testDelete -> Test passed.");
		} else {
			System.out.println("testDelete -> Test FAILED! Expected: " + expected + ", Result: " + result);
		}	
		
		System.out.print("Test2 ");

		result = f.inhabitants[0];
		expected = h;
		
		printTestResults(expected, result);	
		h.die();
		l.die();
	}

	private static void testCountHares() {
		Field f = new Field(1, 2);
		Hare h = new Hare(0, 0);
		Lynx l = new Lynx(0, 0);
		f.add(l);
		f.add(h);
				
		int result = f.countLynxes();
		int expected = 1;		
		
		printTestResults(expected, result);
		h.die();
		l.die();
	}

	private static void testCountLynxes() {
		Field f = new Field(1, 2);
		Lynx l = new Lynx(0, 0);
		f.add(l);
		f.add(l);
				
		int result = f.countLynxes();
		int expected = 2;		
		
		printTestResults(expected, result);
		l.die();
	}

	private static void testGetLabel() {
		Field f = new Field(1, 2);
		
		System.out.print("Test1 ");	
		Hare h = new Hare(0, 0);
		Lynx l = new Lynx(0, 0);
		f.add(h);
		f.add(l);
				
		String result = f.getLabel();
		String expected = "L";		
		
		printTestResults(expected, result);
		
		System.out.print("Test2 ");	
		f.delete(l);
		f.add(h);
		
		result = f.getLabel();
		expected = "H";		

		printTestResults(expected, result);
		h.die();
		l.die();
	}

	private static void testGetHares() {
		Field f = new Field(1, 2);
		Hare h = new Hare(0, 0);
		f.add(h);
		f.add(h);
		System.out.print("Test1 ");
				
		ArrayList<Hare> result = f.getHares();
		ArrayList<Hare> expected = new ArrayList<Hare>();	
		expected.add(h);
		expected.add(h);
		
		printTestResults(expected, result);
		
		System.out.print("Test2 ");
		f.delete(h);
				
		ArrayList<Hare> result2 = f.getHares();
		ArrayList<Hare> expected2 = new ArrayList<Hare>();	
		expected2.add(h);
		
		printTestResults(expected2, result2);
		h.die();
	}

	private static void testGetLynxes() {
		Field f = new Field(1, 2);
		Hare h = new Hare(0, 0);
		f.add(h);
		Lynx l = new Lynx(0, 0);
		f.add(l);
				
		ArrayList<Lynx> result = f.getLynxes();
		ArrayList<Lynx> expected = new ArrayList<Lynx>();	
		expected.add(l);
		
		printTestResults(expected, result);
		h.die();
		l.die();
	}
}
