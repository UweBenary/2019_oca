package battleship;

public interface BuildShipAble {
	
	enum ShipTypes {
		BATTLESHIP {			
			public BattleShip build() {
				return (BattleShip) BattleShip.createShip();
			}
		}, 
		CRUISER {
			public Cruiser build() {
				return (Cruiser) Cruiser.createShip();
			}
		},
		DESTROYER {
			public Destroyer build() {
				return (Destroyer) Destroyer.createShip();
			}
		},
		SUBMARINE {
			public Submarine build() {
				return (Submarine) Submarine.createShip();
			}
		};
		
		private int count;
		
		public void setCount(int count) {
			this.count = count;
		}
		
		public int getCount() {
			return count;
		}
		

	}
	
	
	static Ship createShip();	

}
