package battleship;

import java.util.ArrayList;

abstract class Ship implements BuildShipAble {
	
	/**
	 * Inner Class ShipBody
	 * Prerequisite to build a Ship.
	 * Constructor throws AssertionError upon illegal arguments. 
	 */
	class ShipBody {
		private final int X_MIN; 
		private final int X_MAX; 
		private final int Y_MIN; 
		private final int Y_MAX;

		
		/**
		 * Sole and only constructor of ShipBody.
		 * @param x_MIN
		 * @param x_MAX
		 * @param y_MIN
		 * @param y_MAX
		 * @throws AssertionError
		 */
		ShipBody(int x_MIN, int x_MAX, int y_MIN, int y_MAX) throws AssertionError {
			if ( isValidDimensions(x_MIN, x_MAX, y_MIN, y_MAX) ) {
				X_MIN = x_MIN;
				X_MAX = x_MAX;
				Y_MIN = y_MIN;
				Y_MAX = y_MAX;
				
			} else {
				throw new AssertionError(
							"ShipBody is ill defined by Arguments [" 
							+ x_MIN + ", " + x_MAX + ", " + y_MIN + ", " + y_MAX + "]."
						  );
			}
		}

		/**
		 * evaluates that ShipBody is at least 1 unit long and 
		 * that Ship is stretched out either horizontally or vertically 
		 * @param x_MIN2
		 * @param x_MAX2
		 * @param y_MIN2
		 * @param y_MAX2
		 * @return
		 */
		private boolean isValidDimensions(int x_MIN2, int x_MAX2, int y_MIN2, int y_MAX2) {
			boolean validCase1 = x_MIN2 == x_MAX2 && y_MIN2 < y_MAX2;
			boolean validCase2 = x_MIN2 < x_MAX2 && y_MIN2 == y_MAX2;
			if ( !(validCase1 || validCase2) ) {
				return false;
			} else {
				return true;
			}			
		} 
	}
	
	/**
	 * Inner Class ShipPart
	 * Defines a part of a Ship, 
	 * providing information of its location ({@code x}, {@code y} and 
	 * whether it has been {@code destroyed} ({@value true}) or not ({@value false}). 
	 */
	class ShipPart {

		private final int X;
		private final int Y;
		private boolean destroyed;
		
		public ShipPart(int x, int y) {
			this.X = x;
			this.Y = y;
			this.destroyed = false;
		}
		
		public boolean isDestroyed() {
			return destroyed;
		}

		public void destroy() {
			this.destroyed = true;
		}

		public int getX() {
			return X;
		}

		public int getY() {
			return Y;
		}
	}
	
	
	
	/*
	 * class variables
	 */
	
	/*
	 * object variables
	 */
	private final String NAME = nameShip();
	private final ArrayList<ShipPart> SHIP_PARTS = setSHIPPARTS();
	private boolean destroyed;

	
	/*
	 * constructors
	 */
	// abstract class Ship
	
	/*
	 * class methods
	 */
	
	/*
	 * abstract methods
	 */	
	abstract String nameShip();
	abstract ShipBody createShipBody(); 
	
	/*
	 * getters & setters
	 */
	/**
	 * depends on: abstract ShipBody createShipBody()
	 * @return ArrayList<ShipPart>
	 */
	private ArrayList<ShipPart> setSHIPPARTS() {
		ShipBody shipBody = createShipBody();
		ArrayList<ShipPart> shipParts = new ArrayList<ShipPart>();
		for (int x = shipBody.X_MIN; x < shipBody.X_MAX + 1; x++) {
			for (int y = shipBody.Y_MIN; y < shipBody.Y_MAX + 1; y++) {
				shipParts.add( new ShipPart(x, y) );
			}
		}
		return shipParts;
	}
	
	/**
	 * evaluates {@code destroyed} using ShipParts 
	 * unless {@code destroyed} has already been evaluated as {@value true} once.
	 * @return
	 */
	boolean isDestroyed() {
		if ( !destroyed) {
			updateDestroyed();
		}
		return destroyed;
	}
	
	/**
	 * 
	 * @return {@code NAME} of Ship
	 * @return {@value "Anonymous Ship"} if {@code NAME == null || NAME.isEmpty()}
	 */
	String getNAME()  {
		return NAME == null || NAME.isEmpty() ? "Anonymous Ship" : NAME;
	}
	
	public ShipPart getSHIP_PART(int x, int y) {
		for (ShipPart shipPart : SHIP_PARTS) {
			if (shipPart.getX() == x && shipPart.getY() == y) {
				return shipPart;
			}
		}
		return null;
	}
	
	/*
	 * object methods
	 */	

	@Override
	public String toString() {
		int percentDestroyed = (100 * countDestroyedShipParts()) / SHIP_PARTS.size();
		return getNAME() + " (Damage: " + percentDestroyed + "%)";
	}
	
	private void updateDestroyed() {
		if ( countDestroyedShipParts() == SHIP_PARTS.size() ) {
				destroyed = true;
		}
	}
	
	int countDestroyedShipParts() {
		int destroyedParts = 0;
		for (ShipPart shipPart : SHIP_PARTS) {
			if ( shipPart.isDestroyed() ) {
				destroyedParts++;
			}
		}
		return destroyedParts;
	}
}
