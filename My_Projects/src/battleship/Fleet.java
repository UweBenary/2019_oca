package battleship;

import java.util.ArrayList;
import java.util.List;

class Fleet implements BuildShipAble {
	
	

//	private static final byte BATTLESHIP_COUNT = 1;
//	private static final byte CRUISER_COUNT = 1;
//	private static final byte DESTROYER_COUNT = 3;
//	private static final byte SUBMARINE_COUNT = 4;
	
	private boolean destroyed;
	private List<Ship> ships = new ArrayList<Ship>();
	
	Fleet () {
		destroyed = false;
		createShips();
		
	}

	private void createShips() {
		
		for (ShipTypes shipType : ShipTypes.values()) {
			for (byte i = 0; i < shipType.count; i++) {
				ships.add( createShip(shipType) );			
			}
		}
	}

	private Ship createShip(ShipTypes shipType) {
		
		Ship newShip;
		
		switch (shipType) {
		case BATTLESHIP:
			newShip = BattleShip().;
			break;

		default:
			break;
		}

		return shipType.create;
	}

	/**
	 * not realized in Fleet
	 * @return null
	 */
	@Override
	public Ship createShip() {
		return null;
	}
	
	
}
