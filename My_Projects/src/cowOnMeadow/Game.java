package cowOnMeadow;

public class Game {

	public static void main(String[] args) {
		/*
		 * create meadow
		 */
		Meadow meadow1 = new Meadow();
		meadow1.plantWeed();    		// random placement of weed (true) on meadow
		
		/*
		 * place cow
		 */
		Cow cow = new Cow(meadow1);		// placed at random position on meadow
		
		/*
		 * repeat until no more weed
		 */
		int count = 0;
		while (findWeed(meadow1) && count<1000) {
			count++;
			displayGame(meadow1, cow);
			cow.move();
			cow.guzzle(meadow1);	
			
			try {						// Pause shortly
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//			if (userIntervention()) {	// interruption by user at any time
//				break;
//			}
		}
		displayGame(meadow1, cow);
		
		/*
		 * game over statement
		 */
		System.out.println("- Game Over -");
		System.out.println("Cow has guzzled all weeds.");
		
	} // end of main
	
	public static boolean findWeed(Meadow meadow) {
		boolean result = false;
		boolean [][] weed = meadow.weed();
		breakAll:
		for (int i = 0; i < meadow.getWidth(); i++) {
			for (int j = 0; j < meadow.getLength(); j++) {
				if (weed[i][j]) {
					result = true;
					break breakAll;
				}
			}
		}
		return result;
	}
	
	public static void displayGame(Meadow meadow, Cow cow) {
		boolean[][] weed = meadow.weed();
		for (int y = meadow.getLength(); y >=-1 ; y--) {
			for (int x = -1; x <= meadow.getWidth(); x++) {
		
				if (x==-1||x==meadow.getWidth()) {
					System.out.print("|");	
				} else if (y==-1||y==meadow.getLength()) {
					System.out.print("-");	
				} else if(x==cow.getxPosition() && y==cow.getyPosition()) {
					System.out.print("Q");	
				} else if (weed[x][y]) {
					System.out.print("Y");				
				} else {
					System.out.print(" ");	
				}
			}
			System.out.println();
		}
		System.out.println();
	}
	
//	public static boolean userIntervention() {
//		java.util.Scanner scanner = new java.util.Scanner( System.in );
//		return scanner.next().equals("q");
//	}

}
