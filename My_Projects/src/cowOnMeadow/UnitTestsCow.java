package cowOnMeadow;

public class UnitTestsCow {

	public static void main(String[] args) {
		
		
		test_DefaultConstructor_expectPass();	
		test_RandomConstructor_expectPass();
		test_move_expectPass();
		test_guzzle_expectPass();
	}
	
	
	static void test_DefaultConstructor_expectPass() {
		
		final int expectedxPosition = 20; 
		final int expectedyPosition = 10; 

		Meadow meadow = new Meadow( expectedxPosition*2, expectedyPosition*2);
		Cow cow = new Cow( meadow, expectedxPosition, expectedyPosition);
		
		int actualxPosition = cow.getxPosition();
		int actualyPosition = cow.getyPosition();
	
		if (actualxPosition == expectedxPosition &&
				actualyPosition == expectedyPosition ) {
		
			System.out.println("--> Test 'test_DefaultConstructor_expectTrue' passed!" );
			
		} else {
			System.out.println("--> Error: Test 'test_DefaultConstructor_expectTrue' did NOT pass!" );
		}		
	}
	
	static void test_RandomConstructor_expectPass() {
		
		final int width = 3; 
		final int length = 2; 

		Meadow meadow = new Meadow( width, length );
		
		boolean test = true; //expected 
		for (int i = 0; i < 100; i++) {
		
			Cow cow = new Cow( meadow );
			
			int actualxPosition = cow.getxPosition();
			int actualyPosition = cow.getyPosition();
			
			if (actualxPosition>width ||actualxPosition<0||
					actualyPosition>length ||actualyPosition<0) {
				test = false;
				break;
			}
		}
	
		if ( test ) {		
			System.out.println("--> Test 'test_RandomConstructor_expectPass' passed!" );
			
		} else {
			System.out.println("--> Error: Test 'test_RandomConstructor_expectPass' did NOT pass!" );
		}		
	}
	
	static void test_move_expectPass() {
			
			final int width = 3; 
			final int length = 2; 
	
			Meadow meadow = new Meadow( width, length );
			
			boolean test = true; //expected 
			for (int i = 0; i < 100; i++) {
			
				Cow cow = new Cow( meadow );
				
				for (int j = 0; j < 100; j++) {
					
					cow.move();
					int actualxPosition = cow.getxPosition();
					int actualyPosition = cow.getyPosition();
					
					if (actualxPosition>width ||actualxPosition<0||
							actualyPosition>length ||actualyPosition<0) {
						test = false;
						break;
					}
				}				
			}
		
			if ( test ) {		
				System.out.println("--> Test 'test_move_expectPass' passed!" );
				
			} else {
				System.out.println("--> Error: Test 'test_move_expectPass' did NOT pass!" );
			}		
		}

	static void test_guzzle_expectPass() {
					
			final int width = 20; 
			final int length = 10; 
			final int x = 5;
			final int y = 3;
			final boolean expectedVal = false;

			Meadow meadow = new Meadow( width, length);
			boolean[][] w = meadow.weed();
			if (w[x][y] != false) {
				
				System.out.println("--> Error: Test 'test_setWeed_expectPass' did NOT pass!" );
				
			} else {

					meadow.setWeed( x, y, false);
					Cow cow = new Cow(meadow, x, y);
					cow.guzzle(meadow);
					
					w = meadow.weed();
					
					if (w[x][y] == expectedVal) {
						System.out.println("--> Test 'test_setWeed_expectPass' passed!" );
						
					} else {
						System.out.println("--> Error: Test 'test_setWeed_expectPass' did NOT pass!" );
					}
			}		
	}

}
