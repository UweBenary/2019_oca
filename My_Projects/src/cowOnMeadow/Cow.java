package cowOnMeadow;

import java.util.Random;

public class Cow {

	private int xPosition; 
	private int yPosition;
	private int xMin = 0; 
	private int xMax; 
	private int yMin = 0; 
	private int yMax;
	
	public Cow (Meadow m, int x, int y) {
		this.xMax = m.getWidth() -1;
		this.yMax = m.getLength() -1;
		if (this.xMin <= x && x <= this.xMax &&
				this.yMin <= y && y <= this.yMax) {
			this.xPosition = x;
			this.yPosition = y;
		} else {
			throw new IllegalArgumentException("Cow must be placed on meadow!");
		}
	}

	public Cow (Meadow m) {
		this.xMax = m.getWidth() -1;
		this.yMax = m.getLength() -1;
		Random ran = new java.util.Random(); 
		this.xPosition = ran.nextInt( this.xMax +1 -this.xMin) +this.xMin;
		this.yPosition = ran.nextInt( this.yMax +1 -this.yMin) +this.yMin;
	}
	
	public int getxPosition() {
		return xPosition;
	}

	public int getyPosition() {
		return yPosition;
	}
	
	public void move() {
		int x;
		do {
			x = this.xPosition;
			Random ran = new java.util.Random(); 
			x += ran.nextInt(3) -1;
		} while (x < this.xMin || x > this.xMax);
		this.xPosition = x;
		
		int y;
		do {
			y = this.yPosition;
			Random ran = new java.util.Random(); 
			y += ran.nextInt(3) -1;
		} while (y < this.yMin || y > this.yMax);
		this.yPosition = y;
	}
	
	public void guzzle( Meadow m ) {
		int x = this.xPosition;
		int y = this.yPosition;
		m.setWeed(x, y, false);
	}
	
	
}
