package cowOnMeadow;

import java.util.Arrays;

public class UnitTestsMeadow {
	
	public static void main(String[] args) {
		
		test_DefaultConstructor_expectPass();		
		test_plantWeed_expectPass();
		test_setWeed_expectPass();
		
	} // end of main
	
	static void test_DefaultConstructor_expectPass() {
		
		final int expectedWidth = 20; 
		final int expectedLength = 10; 
		final boolean[][] expectedWeed = new boolean[expectedWidth][expectedLength];

		Meadow meadow = new Meadow( expectedWidth, expectedLength);
		
		boolean[][] actualWeed = meadow.weed();
		int actualWidth = meadow.getWidth();
		int actualLength = meadow.getLength();
	
		if (actualWidth == expectedWidth &&
				actualLength == expectedLength &&
				Arrays.deepEquals(actualWeed, expectedWeed)) {
		
			System.out.println("--> Test 'testDefaultConstructor_expectTrue' passed!" );
			
		} else {
			System.out.println("--> Error: Test 'testDefaultConstructor_expectTrue' did NOT pass!" );
		}		
	}

	static void test_plantWeed_expectPass() {
		
		final double expectedWeedRate = 0.5; 

		Meadow meadow = new Meadow(1000,1000);
		meadow.plantWeed();
		boolean[][] weed = meadow.weed();
		
		double count = 0;
		double weedCount = 0;
		for (int i = 0; i < weed.length; i++) {
			for (int j = 0; j < weed[i].length; j++) {
				count++;
				if (weed[i][j]) {
					weedCount++;
				}
			}
		}
		
		double actualWeedRate = count==0 ? -1 : weedCount/count;

		if (actualWeedRate < expectedWeedRate *(1.+1./100.) &&
				actualWeedRate > expectedWeedRate *(1.-1./100.)) {		
			System.out.println("--> Test 'test_plantWeed_expectPass' passed!" );
			
		} else {
			System.out.println("--> Error: Test 'test_plantWeed_expectPass' did NOT pass!" );
		}		
		
//		System.out.println("expectedWeedRate: " + expectedWeedRate + " actualWeedRate: " + actualWeedRate);
				
	}
	
	static void test_setWeed_expectPass() {
		
		final int width = 20; 
		final int length = 10; 
		final int x = 5;
		final int y = 3;
		final boolean expectedVal = true;

		Meadow meadow = new Meadow( width, length);
		boolean[][] w = meadow.weed();
		if (w[x][y] != false) {
			
			System.out.println("--> Error: Test 'test_setWeed_expectPass' did NOT pass!" );
			
		} else {

				meadow.setWeed( x, y, expectedVal);
				w = meadow.weed();
				
				if (w[x][y] == expectedVal) {
					System.out.println("--> Test 'test_setWeed_expectPass' passed!" );
					
				} else {
					System.out.println("--> Error: Test 'test_setWeed_expectPass' did NOT pass!" );
				}
		}
	}
	
}