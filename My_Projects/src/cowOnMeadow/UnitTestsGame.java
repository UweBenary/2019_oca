package cowOnMeadow;

public class UnitTestsGame {

	public static void main(String[] args) {


		test_findWeed_expectPass();
//		test_userIntervention_expectPassUponPressingQ();

	}
	static void test_findWeed_expectPass() {
		
		final int width = 20; 
		final int length = 10; 
		final int x = 5;
		final int y = 3;
		Meadow meadow = new Meadow( width, length);
		
		final boolean expectedVal1 = false;
		final boolean expectedVal2 = true;
		
		if (expectedVal1 == Game.findWeed(meadow)) {
		
			meadow.setWeed( x, y, expectedVal2);
			if (expectedVal2 == Game.findWeed(meadow)) {
				System.out.println("--> Test 'test_setWeed_expectPass' passed!" );
				
			} else {
				System.out.println("--> Error: Test 'test_setWeed_expectPass' did NOT pass!" );
			}
		
		} else {
			System.out.println("--> Error: Test 'test_setWeed_expectPass' did NOT pass!" );
		}
	}	
	
//	static void test_userIntervention_expectPassUponPressingQ() {
//		int i = 1000;
//		while ( i >= 0) {
//			System.out.print(".");
//			if (Game.userIntervention()) {
//				break;
//			}
//			i--;
//		}
//		System.out.println();
//		if (i>0) {				
//			System.out.println("--> Test 'test_userIntervention_expectPassUponPressingQ' passed!" );			
//		} else {
//			System.out.println("--> Error: Test 'test_userIntervention_expectPassUponPressingQ' did NOT pass!" );
//		}
//	}

}
