package cowOnMeadow;

import java.util.Random;

public class Meadow {	
	
	/*
	 * Attributes
	 */
	private int width; 
	private int length;
	private boolean[][] weed; 

	/*
	 * Constructors
	 */
	public Meadow (int width, int length) {
		this.width = width;
		this.length = length;
		this.weed = new boolean[width][length]; // default is false
	}
	
	public Meadow () {
		this(10, 5);
	}
	
	/*
	 * getters
	 */
	public boolean[][] weed() {
		return weed;
	}
	
	public int getWidth() {
		return width;
	}

	public int getLength() {
		return length;
	}
	
	/*
	 * setter
	 */
	public void setWeed(int x, int y, boolean b) {
		this.weed[x][y] = b;
	}
	
	/*
	 * methods
	 */
	public void plantWeed() {
		Random rand = new java.util.Random(); 
		for (int i = 0; i < this.weed.length; i++) {
			for (int j = 0; j < this.weed[i].length; j++) {
				this.weed[i][j] = rand.nextBoolean();
			}
		}
	}
	



}
