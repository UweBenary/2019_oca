package brakeCodeBruteforce;

public class Safe {
	
	private final Word PASSWORD; //aus unterschiedlichen englischen Kleinbuchstaben der Länge 4
	
	public Safe(Word password) {
		PASSWORD = password;
	}
	
	/*
	 * Die Methode `open` vergleicht das Argument `passwordToTry` mit dem Attribut `password` 
	 * und liefert `true` wenn die beiden Arrays nach ihren Inhalten gleich sind. 
	 */
	boolean open(Word passwordToTry) { 
		if (PASSWORD.equals(passwordToTry)) {
//			System.out.println("Safe is open.");
			return true;
		} else {
//			System.out.println("Access denied!");
			return false;
		}
	}
	
	/*
	 * "UnitTests"
	 */
//	public static void main(String[] args) {
//		Word pword = new Word(new char[] {'a','b'});
//		Safe s = new Safe(pword);	
//		System.out.println(s.open(pword) ? "TestOK" : "TestFailed");
//		Word pword2 = new Word(new char[] {'a'});
//		System.out.println(s.open(pword2) ? "TestFailed" : "TestOK");
//	}
}
