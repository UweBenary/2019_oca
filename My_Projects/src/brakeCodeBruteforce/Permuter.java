package brakeCodeBruteforce;

public abstract class Permuter {
	
	private Test testRef;
	private boolean exitFlag;
	
	void setTestRef(Test ref) {
		this.testRef = ref;
	}
	
	private Test getTestRef() {
		return testRef;
	}
	
	boolean isExitFlag() {
		return exitFlag;
	}

	void setExitFlag(boolean exitFlag) {
		this.exitFlag = exitFlag;
	}

	boolean testWord(Word pw2Try) {
		return getTestRef().isSafeOpen( pw2Try );
	}
	
	abstract void startPermutation();
}
