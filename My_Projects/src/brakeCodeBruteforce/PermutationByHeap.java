package brakeCodeBruteforce;

import java.util.Random;


public class PermutationByHeap extends Permuter {

	@Override
	void startPermutation() {
		 do {
			Random rand = new Random();
			Word w = new Word(new char[] { (char) (rand.nextInt('z'+1 -'a')+'a'), (char) (rand.nextInt('z'+1 -'a')+'a'), (char) (rand.nextInt('z'+1 -'a')+'a') });
			generate(w.length(), w);			
		} while ( !isExitFlag() );
	}
	
	
	private void generate(int k, Word A) {
		if (isExitFlag()) {return;};
		if (k == 1) {
//			System.out.println(A);
			if ( testWord(A) ) {
				/*System.exit(0);*/ 
				setExitFlag(true); 
				return;
			};
		} else {
			// Generate permutations with kth unaltered
			// Initially k == length(A)
			generate(k - 1, A);
	
			// Generate permutations for kth swapped with each k-1 initial
			for (int i = 0; i < k-1; i ++) {
				// Swap choice dependent on parity of k (even or odd)
				if (k%2 == 0) {
//					swap(A[i], A[k-1]); // zero-indexed, the kth is at k-1
					char a = A.getCharAt(i);
					A.setCharAt( i, A.getCharAt(k-1) );
					A.setCharAt( k-1, a );
				} else {
//					swap(A[0], A[k-1]);
					char a = A.getCharAt(0);
					A.setCharAt( 0, A.getCharAt(k-1) );
					A.setCharAt( k-1, a );
				}
				generate(k - 1, A);
			}
		}
	}
}
