package brakeCodeBruteforce;

public class StartBruteForceApproach {

	public static void main(String[] args) {

		Word password = new Word("cac");
		Safe safe = new Safe(password);
		
		Logger logger = new PrintShortInLogFile(); //new PrintShortOnConsoleTime(); // new PrintOnConsole();
		
		Permuter pHeap = new PermutationByHeap();

		bruteForce(safe, pHeap, logger);
		System.out.println("-> Done 1st approach");
		System.out.println();
		
		
		password = new Word("ctc");
		safe = new Safe(password);
		Permuter pGuess = new GuessByLetterFrequency();

		bruteForce(safe, pGuess, logger);
		System.out.println("-> Done 2nd approach");
		
	}
	
	private static void bruteForce(Safe safe, Permuter p, Logger logger) {
		Test test = new Test(safe, logger);
		p.setTestRef(test);
		p.startPermutation();		
	}
}
