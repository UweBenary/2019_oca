package brakeCodeBruteforce;

import java.sql.Timestamp;
import java.util.Date;

public class PrintShortOnConsoleTime extends Logger {

	@Override
	void writeLog(Test test) {
		int testCount = test.getTestCount();
		if ( testCount == 1 || test.isTestResult() ) {
	        Date date = new Date();
	        Timestamp ts = new Timestamp(date.getTime());
			String word = test.getPw2Try().toString();
			String testResult = test.isTestResult() ? "Correct password found." : "Password failed.";
			System.out.printf("Test #%d (%s): %s - %s %n", testCount, ts, word, testResult);
		}
	}
}
