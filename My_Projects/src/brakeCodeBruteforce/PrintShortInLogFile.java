package brakeCodeBruteforce;

//import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
//import java.io.FileReader;
import java.io.FileWriter;
//import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PrintShortInLogFile extends Logger {
	
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");

	@Override
	void writeLog(Test test) {
		
		int testCount = test.getTestCount();
		
		if ( testCount == 1 ) {
			write2File( String.format("*** Start new test%n") );
		}
		
		if ( testCount == 1 || test.isTestResult() ) {
			
	        Date date = new Date();
	        Timestamp ts = new Timestamp(date.getTime());
	        
			String word = test.getPw2Try().toString();
			
			String testResult = test.isTestResult() ? "Correct password found." : "Password failed.";
			
			String outputString = String.format("Test #%d (%s): %s - %s %n", testCount, ts, word, testResult);
			
			write2File(outputString);
		}
		
		if ( test.isTestResult() ) {
			write2File( String.format("%n") );
		}
	}
	
	private void write2File(String content) {
		//https://stackoverflow.com/questions/1625234/how-to-append-text-to-an-existing-file-in-java
		
		try{
            // Create new file
            
            //String path="D:\\a\\hi.txt";
            //File file = new File(path);

            Path absolutePath = Paths.get(System.getProperty("user.dir"));
            
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
    		String fileName = "logfile_" + sdf.format(timestamp) + ".txt";
    		
    		Path path2File = Paths.get(absolutePath.toString(),"bin", this.getClass().getPackageName(), fileName);

    		File file = new File( path2File.toString() );

            // If file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
                
                System.out.println( "Safed to Log-file: " + path2File.toString() );
        		
            }

//            FileWriter fw = new FileWriter(file.getAbsoluteFile());
//            BufferedWriter bw = new BufferedWriter(fw);

            // https://www.baeldung.com/java-append-to-file
            //
//            FileWriter fw = new FileWriter(fileName, true);
//            BufferedWriter bw = new BufferedWriter(fw);
//            bw.write("Spain");
//            bw.newLine();
//            bw.close();
            FileWriter fw = new FileWriter( file.getAbsoluteFile(), true );
            BufferedWriter bw = new BufferedWriter(fw);
            
            // Write in file
            bw.write(content);

            // Close connection
            bw.close();
        }
        catch(Exception e){
            System.out.println(e);
        }
	}
	
	/*
	 * Unit Test
	 */
	
//	public static void main(String[] args) {
//		PrintShortInLogFile x = new PrintShortInLogFile(); 
//		x.write2File( "test lorem ipsum " );	
//		
//		x.write2File( " and again " );	
//		
//        Path absolutePath = Paths.get(System.getProperty("user.dir"));
//		String fileName = "logfile.txt";
//		Path path2File = Paths.get(absolutePath.toString(),"bin", x.getClass().getPackageName(), fileName);
//		System.out.println( loadFromFile( path2File.toString() ) );
//	}
//	
//	static String loadFromFile(String path) {
//		StringBuilder sb = new StringBuilder();
//		
//		try( BufferedReader in = new BufferedReader( new FileReader( path ) ) ) {
//			
//			String line;
//			while( (line = in.readLine()) != null ) {
//				
//				sb.append(line);
//				sb.append(System.lineSeparator());
//			}
//			
//		} catch (IOException e) {
//			System.out.println("Fehler! Kann die Datei nicht lesen");
//			e.printStackTrace();
//		}
//		return sb.toString();
//	}
}
