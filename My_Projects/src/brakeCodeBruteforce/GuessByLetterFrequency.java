package brakeCodeBruteforce;

import java.util.Arrays;
import java.util.Random;

public class GuessByLetterFrequency extends Permuter {
	
	private final double[] LETTER_FREQ = new double[] {11.682, 4.434, 5.238, 3.174, 2.799, 4.027, 1.642, 4.200, 7.294, 
													0.511, 0.456, 2.415, 3.826, 2.284, 7.631, 4.319, 0.222, 2.826,
													6.686, 15.978, 1.183, 0.824, 5.497, 0.045, 0.763, 0.045};	

	private final double[] LETTER_FREQ_CULM;
	
	GuessByLetterFrequency() {
		LETTER_FREQ_CULM = calcLETTER_FREQ_CULM ();
	}
	
	private double[] calcLETTER_FREQ_CULM() {
		double[] result = new double[LETTER_FREQ.length];
		result[0] = LETTER_FREQ[0];
		for (int i = 1; i < result.length; i++) {
			result[i] = result[i-1] + LETTER_FREQ[i];
		}		
		return result; 
	}

	private char chooseLetter() {
		Random rand = new Random();
		double r = rand.nextDouble() * 100 /* % */;
		int position = Arrays.binarySearch(LETTER_FREQ_CULM, r);
		int ch = position < 0 ? Math.abs(position + 1 ) : position; // check for presence of r (position >=0) or absence (position < 0 and reduced by 1)
		return (char) ('a' + ch);
	}
	
	@Override
	void startPermutation() {
		 do {
			Word w = new Word(new char[] { chooseLetter(), chooseLetter(), chooseLetter() });
			if ( testWord(w) ) {
				/*System.exit(0);*/ 
				setExitFlag(true); 
				return;
			};
		} while ( !isExitFlag() );
	}
	
	/*
	 * "UnitTest"
	 */
//	public static void main(String[] args) {
//		GuessByLetterFrequency p = new GuessByLetterFrequency();
//		
//		System.out.println(Arrays.toString(p.LETTER_FREQ_CULM));
//		for (int i = 0; i < 1000; i++) {
//					System.out.println( p.chooseLetter() );
//		}
//
//		for (int i = 0; i < 1000; i++) {
//			char[] chArray = new char[] { p.chooseLetter(), p.chooseLetter(), p.chooseLetter() };
//			System.out.println(chArray);
//			Word w = new Word(chArray); //checks validity of chArray
//		}
//	}
}
