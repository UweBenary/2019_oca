package brakeCodeBruteforce;

public class Test {
	
	private final Safe SAFE;
	private final Logger LOGGER;
	private Word pw2Try;
	private int testCount;
	private boolean testResult;

	public Test(Safe safe, Logger logger) {
		this.SAFE = safe;
		this.LOGGER = logger;
	}

	private Safe getSAFE() {
		return SAFE;
	}
	
	private Logger getLOGGER() {
		return LOGGER;
	}
	
	private void setPw2Try(Word pw2Try) {
		this.pw2Try = pw2Try;
	}
	
	Word getPw2Try() {
		return pw2Try;
	}

	int getTestCount() {
		return testCount;
	}

	private void incTestCount() {
		testCount = testCount + 1;
	}
	
	boolean isTestResult() {
		return testResult;
	}

	private void setTestResult(boolean testResult) {
		this.testResult = testResult;
	}

	boolean isSafeOpen(Word word) {
		setPw2Try(word);
		incTestCount();	
		setTestResult( getSAFE().open( getPw2Try() ) );
		useLogger();
		return isTestResult();
	}
	
	private void useLogger() {
		getLOGGER().writeLog(this);
	}
	
	
	/*
	 * "UnitTests"
	 */
//	public static void main(String[] args) {
//		Word pword = new Word(new char[] {'a','b'});
//		Safe s = new Safe(pword);	
//
//		Logger l = new PrintOnConsole();
//		Test t = new Test(s, l);
//		
//		System.out.println( t.getTestCount() );
//		t.incTestCount();
//		System.out.println( t.getTestCount() );	
//		
//		t.setPw2Try(pword);
//		t.useLogger();
//		
//		t.isSafeOpen(pword);
//	}

}
