package nuPogodi;

import java.awt.Point;

public abstract class Figure {
	
	private Point position;
	
	public void setPosition(Point position) {
		this.position = position;
	}

	public Point getPosition() {
		return position;
	}
	
	public void move(Point location) {
		setPosition(location);
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + " at [" + getPosition().getX() + "," + getPosition().getY() + "]";
	}
}
