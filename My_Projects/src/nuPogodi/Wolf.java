package nuPogodi;

import java.awt.Point;

public class Wolf extends Figure {

	public Wolf() {
	}

	public boolean hasCaugth(Hare hare) {
		double distance = this.getPosition().distance(hare.getPosition());
		return distance == 0 ;
	}
	
	
	/*
	 * UnitTest
	 */
	public static void main(String[] args) {
		Wolf wolf = new Wolf();
		Point location = new Point();
		wolf.setPosition(location);
		System.out.println(wolf);
		
		
		Hare hare = new Hare();
		hare.setPosition(location);
		System.out.println(hare);
		
		System.out.println(wolf.hasCaugth(hare));
	}
}
