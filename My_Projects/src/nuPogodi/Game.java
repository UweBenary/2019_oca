package nuPogodi;

import java.awt.Point;
import java.util.Random;

public class Game {	 
	
	private Field field;
	private Hare hare;
	private Wolf wolf;	 
	
	private Game() {}

	
	private void setupField() {
		this.field = new Field();		
	}
	
	private void placeWolf() {
		Point place = getRandomFreePlace();
		wolf = new Wolf();
		wolf.setPosition(place);	
	}

	private void placeHare() {		
		Point place = getRandomFreePlace();
		hare = new Hare();
		hare.setPosition(place);	
	}

	public Point getRandomFreePlace() {
		Point place = new Point();
		Random rand = new Random();
		int randX, randY;
		int xMin = field.getX_MIN() + 1; // exclusive
		int xMax = field.getX_MAX();
		int yMin = field.getY_MIN() + 1;
		int yMax = field.getY_MAX();
		
		do {
			randX = rand.nextInt(xMax - xMin) +  xMin;
			randY = rand.nextInt(yMax - yMin) +  yMin;
			place.setLocation(randX, randY);
		} while ( isOccupied(place) );
		
		return place;
	}
	
	private boolean isOccupied(Point place) {
		if (hare != null && hare.getPosition() != null && place.equals( hare.getPosition() ) ) {
			return true;
		}
		if (wolf != null && wolf.getPosition() != null && place.equals( wolf.getPosition() ) ) {
			return true;
		}
		return false;
	}
	
	private void display() {
		int xMin = field.getX_MIN();
		int xMax = field.getX_MAX();
		int yMin = field.getY_MIN();
		int yMax = field.getY_MAX();
		int hareX = (int) hare.getPosition().getX();
		int hareY = (int) hare.getPosition().getY();
		int wolfX = (int) wolf.getPosition().getX();
		int wolfY = (int) wolf.getPosition().getY();
		
		for (int y = yMax; y > yMin-1; y--) {
			for (int x = xMin; x < xMax+1; x++) {
				
				if (y == yMax || y == yMin || x == xMin || x == xMax) {
					System.out.print("# ");
					if ( x == xMax && y == yMax ) {
						System.out.print("  Nu pogodi!");
					}					
				} else if (x == hareX && y == hareY ) {
					if (wolfX == hareX && wolfY == hareY ) {
						System.out.print("X ");
					} else {
						System.out.print("H ");
					}					
				} else if (x == wolfX && y == wolfY ) {
					System.out.print("w ");			
					
				} else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println();
		
		try {						// Pause shortly
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void move(Figure figure) {
		Point place = getRandomNextPlace(figure);
		figure.move(place);
	}
	
	private Point getRandomNextPlace(Figure figure) {
		int xOffField = field.getX_MIN() - 1;
		int yOffField = field.getY_MIN() - 1;
		
		Point place = new Point(xOffField, yOffField);
		Random rand = new Random();
		int randX, randY;
		int x = (int) figure.getPosition().getX();
		int y = (int) figure.getPosition().getY();
				
		do {
			randX = rand.nextInt(3) - 1;
			randY = rand.nextInt(3) - 1;
			place.setLocation(x + randX, y + randY);		
			
			// do not stay
			if (randX == 0 && randY == 0) {
				place.setLocation(xOffField, yOffField); continue;
			}
			
			// Hare must not move onto olf
			if (figure instanceof Hare && place.equals( wolf.getPosition() ) ) {
				place.setLocation(xOffField, yOffField); continue;
			}
			
			// Wolf moves towards Hare
			if (figure instanceof Wolf ) {
				double dist1 = place.distance( hare.getPosition() );
				double dist2 = wolf.getPosition().distance( hare.getPosition() );
				if (dist1 > dist2) {
					place.setLocation(xOffField, yOffField); continue;
				}
			}
		} while ( !isOnField(place) );
		
		return place;
	}


	private boolean isOnField(Point place) {
		if (place == null) {
			return false;
		}
		
		int x = (int) place.getX();
		int y = (int) place.getY();
		
		if ( x > field.getX_MIN() && x < field.getX_MAX() && y > field.getY_MIN() && y < field.getY_MAX() ) {
			return true;
		} else {
			return false;
		}
	}


	public static void main(String[] args) {

		Game game = new Game();
		
		game.setupField();
		game.placeWolf();
		game.placeHare();

		
		int count = 0;
		while ( !game.wolf.hasCaugth(game.hare) && count < 100 ) {
			game.move(game.hare);
			game.display();
			game.move(game.wolf);	
			game.display();
			count++;			
		}
		
		System.out.println("--- The End ---");
	}

}
