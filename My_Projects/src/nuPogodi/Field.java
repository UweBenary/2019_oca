package nuPogodi;

public class Field {

    private int X_MIN, X_MAX, Y_MIN, Y_MAX;
	
	public Field() {
		this(0,10,0,10);
	}
	
	public Field(int xMin, int xMax, int yMin, int yMax) {
		if (xMin>xMax-2) {
			throw new IllegalArgumentException("xMax must be larger than xMin +2 .");
		}
		if (yMin>yMax-2) {
			throw new IllegalArgumentException("yMax must be larger than yMin +2 .");
		}
		setX_MIN(xMin);
		setX_MAX(xMax);
		setY_MIN(yMin);
		setY_MAX(yMax);
	}
	
	public int getX_MIN() {
		return X_MIN;
	}

	private void setX_MIN(int x_MIN) {
		X_MIN = x_MIN;
	}

	public int getX_MAX() {
		return X_MAX;
	}

	private void setX_MAX(int x_MAX) {
		X_MAX = x_MAX;
	}

	public int getY_MIN() {
		return Y_MIN;
	}

	private void setY_MIN(int y_MIN) {
		Y_MIN = y_MIN;
	}

	public int getY_MAX() {
		return Y_MAX;
	}

	private void setY_MAX(int y_MAX) {
		Y_MAX = y_MAX;
	}
	
	@Override
	public String toString() {
//		return countOccupiedPlaces + ": " + Arrays.toString( occupiedPlaces );
		return "Field with limits: [" + getX_MIN() + "," + getY_MIN() + "] - [" + getX_MAX() + "," + getY_MAX() + "]";
	}

	
	
	/*
	 * UnitTests
	 */

	public static void main(String[] args) {
		
		Field f1 = new Field();
		System.out.println(f1);
		
		Field f2 = new Field(-1,5,2,11);
		System.out.println(f2);
		
		try {			
			Field f3 = new Field(5,-1,2,11);
			System.out.println(f3);		
		} catch (RuntimeException  e) {
			System.out.println("--> Test passed.");
		}	
		
		try {			
			Field f3 = new Field(-1,5,2,3);
			System.out.println(f3);		
		} catch (RuntimeException  e) {
			System.out.println("--> Test passed.");
		}	
		
//		Field f4 = new Field();
//		Point p = new Point(3, 3);
//		f4.addPlace(p);
//		System.out.println(f4);		
//		System.out.println(	"true: " + f4.isOccupied(p) );
//		Point p2 = new Point(1, 3);
//		System.out.println(	"false: " + f4.isOccupied(p2) );
		
		
		
	}
}
