package mines;

import java.awt.Point;

public class Guess {
	private final Point PLACE;	
	private int value;
	
	Guess(int x, int y) {
		this( new Point(x, y) );
	}
	
	Guess(Point p) {
		PLACE = new Point(p);
	}
	
	Point getPlace() {
		return new Point(PLACE);
	}
	
	int getX() {
		return (int) PLACE.getX();
	}
	
	int getY() {
		return (int) PLACE.getY();
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Guess at [" + getX() + "," + getY() + "] with value: " + getValue();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if ( !(obj instanceof Guess) ) {
			return false;
		} else {
			Guess g = (Guess) obj;
			return getX() == g.getX() & getY() == g.getY();
		}
	}
}
