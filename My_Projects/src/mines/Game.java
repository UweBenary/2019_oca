package mines;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Game {

	private static Area area = new Area();		// different dimensions can be implemented
	private static final int NUMBER_OF_MINES = area.check(10);
	private static final int MAX_NUMBER_OF_GUESSES = area.getNumberOfPlaces() - NUMBER_OF_MINES - 1;
	private static final Mine[] MINES = placeMines(); 	// enum is a better implementation
	private static List<Guess> guesses = new ArrayList<Guess>();
	private static boolean displayMines = false;
	

	private static Mine[] placeMines() {
		int xMin = area.getX_MIN() +1; //exclusive
		int xMax = area.getX_MAX();
		int yMin = area.getY_MIN() +1;
		int yMax = area.getY_MAX();
		
		Random rand = new Random();
		int x, y;
		
		Mine[] mines = new Mine[NUMBER_OF_MINES];
		
		for (int i = 0; i < mines.length; i++) {
			x = rand.nextInt(xMax - xMin) + xMin;
			y = rand.nextInt(yMax - yMin) + yMin;
			mines[i] = new Mine(x, y);
		}
		
		return  Arrays.copyOf(mines, mines.length);
	}

	private static Mine[] getMINES() {
		return Arrays.copyOf(MINES, MINES.length);
	}

	private static String getGuessValue(int x, int y) {
		int idx = guesses.indexOf( new Guess(x, y) );		
		int value =  guesses.get(idx).getValue();		
		return Integer.toString(value);
	}

	private static boolean isMine(int x, int y) {
		return isMine(new Point(x, y));
	}

	private static boolean isMine(Point p) {
		Mine m = new Mine(p);
		return Arrays.asList( getMINES() ).contains(m);
	}

	private static boolean isGuess(int x, int y) {
		return guesses.contains(new Guess(x, y));
	}

	private static boolean isLastGuess(int x, int y) {
		Guess last = guesses.get( guesses.size() - 1 );
		return last.getX() == x & last.getY() == y;
	}

	private static boolean isValid(Point p) {
		int xMin = area.getX_MIN();
		int xMax = area.getX_MAX();
		int yMin = area.getY_MIN();
		int yMax = area.getY_MAX();
		
		int x = (int) p.getX();
		int y = (int) p.getY();
		
		if (x > xMin & x < xMax & y > yMin & y < yMax) {
			return true;
		} else {
			return false;
		}
	}
	
	private static Point getUserInput() {
		int x, y;		
		Point p;
		boolean validPoint, alreadyGuessed;

		do {
			System.out.println("Place your next guess (i.e., a1): ");
			String input = new java.util.Scanner( System.in ).next();
			x = input.charAt( 0 ) - 97 + area.getX_MIN() + 1;
			y = area.getY_MAX() - Integer.valueOf( input.substring( 1 ) ) ;
			
			p = new Point(x, y);
			
			validPoint = isValid(p);
			if ( !validPoint ) {
				System.out.printf("   -> %s does not exist on the field! Please try again... %n", input);
			}
			
			alreadyGuessed = isGuess(x, y);
			if ( alreadyGuessed ) {
				System.out.printf("   -> You have already chosen %s once. Please place a new guess ... %n", input);
			}
			
		} while ( !validPoint || alreadyGuessed);
		
		return p;
	}
	
	private static int countSurroundingMines(Point p) {
		int x = (int) p.getX();
		int y = (int) p.getY();
		
		int count = 0;
		for (int i = x-1; i < x+2; i++) {
			for (int j = y-1; j < y+2; j++) {

				if ( !( i==x && j==y ) && isMine(i, j) ) {
				   count++;
				}
			}
		}
		return count;
	}

/*
 * Display methods
 */
	private static void display() {
		int xMin = area.getX_MIN();
		int xMax = area.getX_MAX();
		int yMin = area.getY_MIN();
		int yMax = area.getY_MAX();
		
		System.out.println();
		
		for (int x = xMin; x < xMax; x++) {				
			if (x > xMin & x < xMax) {
				System.out.print( (char)('a' + (x - xMin -1)) + " " );
			
			} else {
				System.out.print("  ");
			}
		}
		System.out.println();
		
		for (int y = yMax; y > yMin-1; y--) {
			for (int x = xMin; x < xMax+1; x++) {
				
				if ( x == xMax & (y == yMin || y == yMax) ) {
					System.out.print("+ ");
					
				} else if ( x == xMin & (y == yMin || y == yMax) ) {
					System.out.print("+-");
					
				} else if (y == yMax || y == yMin) {
					System.out.print("--");
					
				} else if (x == xMin || x == xMax) {
					System.out.print("| ");
											
				} else if (displayMines & isMine(x,y) ) {
					
					if (isLastGuess(x,y)) {
						System.out.print("X ");	
					
					} else {
						System.out.print("@ ");				
					}
					
				} else if ( isGuess(x,y) ) {
					String s = getGuessValue(x,y);
					System.out.printf("%s ", s.contentEquals("0") ? "." : s);	
	
					
				} else {
					System.out.print("  ");
				}
			}
			if (y > yMin & y < yMax) {
				System.out.println(" " + (yMax - y ) );
			} else {
				System.out.println();
			}
		}
		System.out.println();
	}
	
	private static void displayStartScreen() {
		System.out.println();
		System.out.println("*********************************");
		System.out.println("***        ATTENTION!         ***");
		System.out.println("*********************************");
		System.out.println();
		System.out.printf("  There %s %d MINE%s on the field. %n", 
				NUMBER_OF_MINES > 1 ? "are" : "is", 
				NUMBER_OF_MINES, 
				NUMBER_OF_MINES > 1 ? "S" : "");
		System.out.println("Can you identify all safe places to step on? ");		
	}
	
	private static void displayWinnerScreen() {
		System.out.println("*********************************");
		System.out.println("***         Excellent!        ***");
		System.out.println("*********************************");
		System.out.println();
		System.out.println("      All safe places found.");
	}

	private static void displayLooserScreen() {
		System.out.println("*********************************");
		System.out.println("***           BOOM!           ***");
		System.out.println("*********************************");
		System.out.println();
		System.out.println("   You have stepped on a mine.");
	}
	
/*
 * Main method
 */
	public static void main(String[] args) { 
		
		displayStartScreen();
		
		Point guessedPlace;
		boolean foundMine = false;
		do {
			display();
			guessedPlace = getUserInput();
						
			Guess newGuess = new Guess(guessedPlace);
			newGuess.setValue( countSurroundingMines(guessedPlace) );
			guesses.add(newGuess);
			
			foundMine = isMine(guessedPlace);
			
		} while (!foundMine && guesses.size() <= MAX_NUMBER_OF_GUESSES);
		
		System.out.println();
		if ( !foundMine ) {			
			displayWinnerScreen();
			
		} else {
			displayLooserScreen();
		}
		
		displayMines = true;
		display();
		
	} // end of main
}
