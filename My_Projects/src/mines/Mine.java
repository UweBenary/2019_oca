package mines;

import java.awt.Point;

public class Mine {

	private final Point PLACE;	
	
	Mine(int x, int y) {
		this( new Point(x, y) );
	}
	
	Mine(Point p) {
		PLACE = new Point(p);
	}
	
	Point getPlace() {
		return new Point(PLACE);
	}
	
	int getX() {
		return (int) PLACE.getX();
	}
	
	int getY() {
		return (int) PLACE.getY();
	}

	@Override
	public String toString() {
		return "Mine at [" + getX() + "," + getY() + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (this == obj) {
			return true;
		}
		if ( !(obj instanceof Mine) ) {
			return false;
		} else {	
			Mine m = (Mine) obj;
			return getX() == m.getX() & getY() == m.getY();}
	}
	
	/*
	 * UnitTest
	 */
	public static void main(String[] args) {

		Mine m = new Mine(10, 0);
		
		int result = m.getX();
		int expected = 10;
		System.out.println( result == expected ? "-> Test passed" : "-> Test failed!" );

		Point result2 = m.getPlace();
		Point expected2 = new Point(10,0);
		System.out.println( result2.equals(expected2) ? "-> Test passed" : "-> Test failed!" );
	}
}
