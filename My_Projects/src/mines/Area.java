package mines;

public class Area {

	private final int X_MIN;
	private final int X_MAX;
	private final int Y_MIN;
	private final int Y_MAX;
	
	Area() {
		this(0, 10, 0, 5);
	}
	
	Area(int x_MIN, int x_MAX, int y_MIN, int y_MAX) {
		if ( !(x_MAX > x_MIN + 2) ) {
			throw new IllegalArgumentException("Input failes: x_maxn > x_min + 2");
		}
		if ( !(y_MAX > y_MIN + 2) ) {
			throw new IllegalArgumentException("Input failes: y_max > y_min + 2");
		}
		
		if ( (x_MAX - x_MIN > 27) ) {
			throw new IllegalArgumentException("Input failes: x-Dimension exceeds 28");
		}
		
		X_MIN = Math.min(x_MIN, x_MAX);
		X_MAX = Math.max(x_MIN, x_MAX);
		Y_MIN = Math.min(y_MIN, y_MAX);
		Y_MAX = Math.max(y_MIN, y_MAX);
	}

	int getX_MIN() {
		return X_MIN;
	}

	int getX_MAX() {
		return X_MAX;
	}

	int getY_MIN() {
		return Y_MIN;
	}

	int getY_MAX() {
		return Y_MAX;
	}
	
	int getNumberOfPlaces() {
		int xMin = getX_MIN() +1; //exclusive
		int xMax = getX_MAX() -1; //exclusive
		int yMin = getY_MIN() +1; 
		int yMax = getY_MAX() -1; 
		
		return (xMax - xMin +1) * (yMax - yMin +1);
	}

	int check(int i) {
		return i < getNumberOfPlaces() ? i : Math.floorDiv(getNumberOfPlaces(), 4);
	}
	
	/*
	 * UnitTest
	 */
	public static void main(String[] args) {
		
		Area a = new Area();
		
		int result = a.getX_MIN();
		int expected = 0;
		System.out.println( result == expected ? "-> Test passed" : "-> Test failed!" );

		result = a.getY_MAX();
		expected = 10;
		System.out.println( result == expected ? "-> Test passed" : "-> Test failed!" );
		
		a = new Area(0,7,0,3);
		a.display();
		result = a.getNumberOfPlaces();
		System.out.println(result + " : " + a.check(10));
		expected = 19*9;
		System.out.println( result == expected ? "-> Test passed" : "-> Test failed!" );
	}

	private void display() {
		int xMin = getX_MIN();
		int xMax = getX_MAX();
		int yMin = getY_MIN();
		int yMax = getY_MAX();
		
		for (int x = xMin; x < xMax; x++) {				
			if (x > xMin & x < xMax) {
				System.out.print( (char)('a' + (x - xMin -1)) + " " );
			
			} else {
				System.out.print("  ");
			}
		}
		System.out.println();
		
		for (int y = yMax; y > yMin-1; y--) {
			for (int x = xMin; x < xMax+1; x++) {
				
				if ( x == xMax & (y == yMin || y == yMax) ) {
					System.out.print("+ ");
					
				} else if ( x == xMin & (y == yMin || y == yMax) ) {
					System.out.print("+-");
					
				} else if (y == yMax || y == yMin) {
					System.out.print("--");
					
				} else if (x == xMin || x == xMax) {
					System.out.print("| ");
										
				} else {
					System.out.print("  ");
				}
			}
			if (y > yMin & y < yMax) {
				System.out.println(" " + (yMax - y ) );
			} else {
				System.out.println();
			}

		}
		System.out.println();
	}
}
