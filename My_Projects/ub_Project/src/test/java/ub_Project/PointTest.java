package ub_Project;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


class PointTest {

	int expectedX = 2;
	int expectedY = 5;
    ub_Project.Point p = new ub_Project.Point(expectedX, expectedY);
	
	
	@Test
	void testGetX() {
		int actual = p.getX();
		assertEquals(expectedX, actual);
	}

	@Test
	void testGetY() {
		int actual = p.getY();
		assertEquals(expectedY, actual);
	}
	
	@Test
	void testToString() {
		String expected = "[2,5]";
		String actual = p.toString();
		assertEquals(expected, actual);
	}
	
	@Test
	void testEquals() {
		ub_Project.Point p2 = new ub_Project.Point(expectedX, expectedY);
		assertEquals(p, p2);
	}
	
	@Test
	void testEqualsFails() {
		ub_Project.Point p2 = new ub_Project.Point(expectedY, expectedX);
		assertNotEquals(p, p2);
	}

	@ParameterizedTest(name = "Test # {index}.  Expected: {2}")
	@CsvSource( {
		"2, 5, 0",
		"1, 5, 1",
		"3, 6, 2",
	} )
	void parameterizedTestGetManhattanDistance(int x, int y, int expected) {
		ub_Project.Point other = new ub_Project.Point(x, y);
		int actual = p.getManhattanDistance( other );
		assertEquals(expected, actual);
	}
}
