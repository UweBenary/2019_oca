package ub_Project;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ShipyardTest {

	String name = "Titanic II";
	ShipTypes shipType = ShipTypes.BATTLESHIP;
	ub_Project.Point bow = new Point(1, 2);
	ub_Project.Point stern = new Point(1, 2 - shipType.size() +1 ); // including point
	
		
	Ship ship;
	
	@Test
	void testGetShip() {
		ship = Shipyard.getShip(name, shipType, bow, stern);
		assertNotNull(ship);
	}

	ub_Project.Point stern1 = new Point(1, 3); // including point
	
	@Test
	void testGetShipThrowsIllegalArgumentException1() {
		String expectedMessage = "Ship body (bow: " + bow + " stern: " + stern1 + ") does not match size of ship type " + shipType;
		assertThrows(IllegalArgumentException.class, () -> {Shipyard.getShip(name, shipType, bow, stern1);}, expectedMessage);
	}

	ub_Project.Point stern2 = new Point(1 +1, 2 - shipType.size()); 
	
	@Test
	void testGetShipThrowsIllegalArgumentException2() {
		String expectedMessage = "Ship body is ill defined! Bow: " + bow + " stern: " + stern;
		assertThrows(IllegalArgumentException.class, () -> {Shipyard.getShip(name, shipType, bow, stern2);}, expectedMessage);
	}
}
