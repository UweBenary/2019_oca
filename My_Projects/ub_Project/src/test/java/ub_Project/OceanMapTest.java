package ub_Project;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

/*
 * setField only once if null ??
 * 
 */

class OceanMapTest {

	@ParameterizedTest(name = "Test #{index}.")
	@CsvSource( {
		"0, 1",
		"1, 0",
		"-11, 1",
		"1, -11",
	} )
	void testInstantiationFailsWithNonPositiveArguments(int x, int y) {
		assertThrows(IllegalArgumentException.class, ()-> new OceanMap(-1, 0));
	}
	
	@Test
	void testGetField() {
		OceanMap ocean = new OceanMap(1,1);
		Field actual = ocean.getField(0, 0);
		assertNull(actual);
	}
	
	@ParameterizedTest(name = "Test #{index}.")
	@CsvSource( {
		"1, 1",
		"-1, 0",
		"0, -1",
	} )
	void testGetFieldFailsWithIllegalArgument(int x, int y) {
		OceanMap ocean = new OceanMap(1,1);
		assertThrows(ArrayIndexOutOfBoundsException.class, () -> ocean.getField(x, y));
	}

	@Test
	void testSetField() {
		OceanMap ocean = new OceanMap(1,1);
		Field f = new Field();
		ocean.setField(0, 0, f );
		assertEquals(f, ocean.getField(0, 0) );
	}
	
	@Test
	void testHitField() {
		OceanMap ocean = new OceanMap(1,1);
		Field f = new Field();
		ocean.setField(0, 0, f );
		ocean.hitField(0, 0);
		assertTrue( ocean.getField(0, 0).isHit() );
	}
	
	@Test
	void testHitFieldWithNullField() {
		OceanMap ocean = new OceanMap(1,1);
		ocean.hitField(0, 0);
		assertTrue( ocean.getField(0, 0).isHit() );
		assertFalse( ocean.getField(0, 0).hasShipPart() );
	}
}
