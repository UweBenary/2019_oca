package ub_Project;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FleetTest {

	Fleet testFleet;
	String name = "Test Fleet";
	
	@Test
	void testFleetStringIntIntIntInt() {
		testFleet = new Fleet(name, 2, 0, 0, 0);
		assertNotNull(testFleet);
	}
	
	Fleet testFleet2 = new Fleet(name, 2, 0, 0, 0);
	
	@Test
	void testIsDestroyed() {
		assertFalse( testFleet2.isDestroyed() );
	}

	@Test
	void testToString() {
		String expected = "Fleet " + name;
		assertEquals(expected, testFleet2.toString() );
	}

}
