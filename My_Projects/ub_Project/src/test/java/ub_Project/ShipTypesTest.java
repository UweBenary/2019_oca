package ub_Project;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ShipTypesTest {

	@Test
	void testSize() {
		int expected = 3;
		int actual = ShipTypes.DESTROYER.size();
		assertEquals(expected, actual);
	}

}
