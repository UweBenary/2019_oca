package ub_Project;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class FieldTest {

	
	@Test
	void testSetHasShipPartMustRemainSetTrue() {
		Field f = new Field();
		f.placeShipPart();
		assertTrue( f.hasShipPart() );
	}
	
	@Test
	void testGetCopy() {
		Field f = new Field();
		assertNotNull( f.getCopy() );
	}
	
	@Test
	void testEquals() {
		Field f = new Field();
		assertEquals(f, f.getCopy() );
	}
	
	@Test
	void testGetInstanceWithShipPart() {
		Field f = Field.getInstanceWithShipPart();
		assertTrue( f.hasShipPart() );
		assertFalse( f.isHit() );
	}

}
