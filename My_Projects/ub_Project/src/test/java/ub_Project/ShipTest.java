package ub_Project;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ShipTest {

	String name = "Titanic II";
	String shipType = "Test";
	ub_Project.Point[] shipPartLocations = getPoints();
	
	private Point[] getPoints() {
		ub_Project.Point[] points = new ub_Project.Point[3];
		for (int i = 0; i < points.length; i++) {
			points[i] = new Point(i+1, 2);
		}
		return points;
	}
	
	
	Ship ship;
	
	@Test
	void testGetShip() {
		ship = Ship.getShip(name, shipType, shipPartLocations);
		assertNotNull(ship);
	}
	
	

	Ship ship2 = Ship.getShip(name, shipType, shipPartLocations);

	@Test
	void testIsDestroyed() {
		assertFalse( ship2.isDestroyed() );
	}

	@Test
	void testToString() {
		String expected = "Ship " + name + " (" + shipType + " class). Damage: 0%)";
		assertEquals(expected, ship2.toString() );
	}

}
