package ub_Project;

class Shipyard {

	public static Ship getShip(String name, ShipTypes type, Point bow, Point stern) {
		
		ShipBuilder builder = new Shipyard().new ShipBuilder();
		builder.nameShip(name);
		builder.setShipType(type);
		builder.buildShipParts(type, bow, stern);
		
		return builder.build();

	}

	private class ShipBuilder {
		private String name;
		private String shipType;
		private Point[] shipPartLocations;
		
		private ShipBuilder nameShip(String name) {
				this.name = name;
				return this;
			}
		
		private ShipBuilder setShipType(ShipTypes type) {
			StringBuilder sb = new StringBuilder( type.toString() );
			sb.substring(1).toLowerCase();
			this.shipType = sb.toString();
			return this;
		}
		
		private ShipBuilder buildShipParts(ShipTypes type, Point bow, Point stern) {
			
			try {
				verifyArguments(type, bow, stern);
			} catch (IllegalArgumentException e) {
				throw e;

			}

			int shipPartCount = bow.getManhattanDistance(stern) + 1; // include bow
			shipPartLocations = new ub_Project.Point[shipPartCount];

			int[] xarr;
			if ( bow.getX() < stern.getX() ) {
				xarr = new int[] {bow.getX(), stern.getX()};
			} else {
				xarr = new int[] {stern.getX(), bow.getX()};
			}
			
			int[] yarr;
			if ( bow.getY() < stern.getY() ) {
				yarr = new int[] {bow.getY(), stern.getY()};
			} else {
				yarr = new int[] {stern.getY(), bow.getY()};
			}
	
			for (int i = 0, x = xarr[0]; x < xarr[1] +1 ; x++) { 	 // inclusive xarr[1]
				for (int y = yarr[0]; y < yarr[1] +1 ; y++) {		 // inclusive yarr[1]
					shipPartLocations[i] = new Point(x, y);
					i++;
				}
			}
			
			return this;
		}
		
		private void verifyArguments(ShipTypes type, Point bow, Point stern) throws IllegalArgumentException {
			if ( !( bow.getX() == stern.getX() || bow.getY() == stern.getY() ) ) {
				throw new IllegalArgumentException("Ship body is ill defined! Bow: " + bow + " stern: " + stern);
			}			
			int shipPartCount = bow.getManhattanDistance(stern) + 1; // include bow
			if (shipPartCount != type.size()) {
				throw new IllegalArgumentException("Ship body (bow: " + bow + " stern: " + stern + ") does not match size of ship type " + type);
			}
		}

		private Ship build() {
			return Ship.getShip(name, shipType, shipPartLocations);
		}
	}
}