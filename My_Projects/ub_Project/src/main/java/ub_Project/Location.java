package ub_Project;

public class Location extends Point 
	implements Diplayable {

	private boolean isHit = false;

	Location(int x, int y) {
		super(x, y);
	}

	public void setHit() {
		isHit = true;
	}

	public boolean isHit() {
		return isHit;
	}
}
