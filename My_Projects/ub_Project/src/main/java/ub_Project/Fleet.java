package ub_Project;

import java.util.ArrayList;
import java.util.List;

public class Fleet {
	
	private final int NUMBER_OF_BATTLESHIPS;
	private final int NUMBER_OF_CRUISERS;
	private final int NUMBER_OF_DESTROYERS;
	private final int NUMBER_OF_SUBMARINES;	
	private final String NAME;
	private final Ship[] SHIPS;
	private boolean destroyed;
	
	
	public Fleet(String name) {
		this(name, 1, 1, 3, 4);
	}	
	
	public Fleet(String name, int numberBattleships, int numberCruisers, int numberDestroyers, int numberSubmarines) {
		NAME = name;
		NUMBER_OF_BATTLESHIPS = numberBattleships;
		NUMBER_OF_CRUISERS = numberCruisers;
		NUMBER_OF_DESTROYERS = numberDestroyers;
		NUMBER_OF_SUBMARINES = numberSubmarines;
		destroyed = false;
		SHIPS = buildFleet();
	}

	private Ship[] buildFleet() {  
		String shipName;
		ub_Project.Point[] shipLocation;
		ub_Project.Point bow;
		ub_Project.Point stern;
		
		List<Ship> shipList = new ArrayList<Ship>();		

		for (int i = 0; i < NUMBER_OF_BATTLESHIPS; i++) {
			shipName = createShipName();
			shipLocation = getShipLocation();
			bow = shipLocation[0];
			stern = shipLocation[1];
			shipList.add( Shipyard.getShip( shipName, ShipTypes.BATTLESHIP, bow, stern ) );
		}
		
		for (int i = 0; i < NUMBER_OF_CRUISERS; i++) {
			shipName = createShipName();
			shipLocation = getShipLocation();
			bow = shipLocation[0];
			stern = shipLocation[1];
			shipList.add( Shipyard.getShip( shipName, ShipTypes.CRUISER, bow, stern ) );
		}
		
		for (int i = 0; i < NUMBER_OF_DESTROYERS; i++) {
			shipName = createShipName();
			shipLocation = getShipLocation();
			bow = shipLocation[0];
			stern = shipLocation[1];
			shipList.add( Shipyard.getShip( shipName, ShipTypes.DESTROYER, bow, stern ) );
		}
		
		for (int i = 0; i < NUMBER_OF_SUBMARINES; i++) {
			shipName = createShipName();
			shipLocation = getShipLocation();
			bow = shipLocation[0];
			stern = shipLocation[1];
			shipList.add( Shipyard.getShip( shipName, ShipTypes.SUBMARINE, bow, stern ) );
		}

		return shipList.toArray( new Ship[] {} );
	}


	private Point[] getShipLocation() {
		// TODO Auto-generated method stub
		System.err.println("Auto-generated method stub: getShipLocation() in class Fleet");
		return new Point[] {new Point(0, 0), new Point(0, 0 - ShipTypes.BATTLESHIP.size() + 1)};
	}

	private String createShipName() {
		// TODO Auto-generated method stub
		System.err.println("Auto-generated method stub: createShipName() in class Fleet");
		return "Test Ship";
	}

	/**
	 * evaluates {@code destroyed} using ships 
	 * unless {@code destroyed} has already been evaluated as {@value true} once.
	 * @return {@code destroyed} 
	 */
	boolean isDestroyed() {
		if ( !destroyed ) {
			destroyed = updateDestroyed();
		}
		return destroyed;
	}	
	
	private boolean updateDestroyed() {
		return countDestroyedShips() == SHIPS.length;
	}
	
	private int countDestroyedShips() {
		int destroyedShips = 0;
		for (Ship ship : SHIPS) {
			if ( ship.isDestroyed() ) {
				destroyedShips++;
			}
		}
		return destroyedShips;
	}
	
	@Override
	public String toString() {
		return "Fleet " + NAME;
	}
}
