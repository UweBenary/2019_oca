package ub_Project;

public class Ship {


	/**
	 * Inner Class ShipPart
	 * Defines a part of a Ship, 
	 * providing information of its location ({@code x}, {@code y} and 
	 * whether it has been {@code destroyed} ({@value true}) or not ({@value false}). 
	 */
	private class ShipPart extends ub_Project.Point{

		private boolean destroyed;
		
		private ShipPart(int x, int y) {
			super(x, y);
			destroyed = false;
		}
		
		private boolean isDestroyed() {
			return destroyed;
		}

		private void setDestroyedTrue() {
			destroyed = true;
		}
		
	} // end of class ShipPart
	
	
	
	/*
	 * Instance variables of Ship
	 */
	private String name ;
	private String shipType ;
	private ShipPart[] shipBody ;
	private boolean destroyed ;
	
	/*
	 * Private constructor 
	 */
	private Ship() { }
		
	
	/**
	 * static getShip(...) substitutes for private constructor
	 * @param name
	 * @param shipType
	 * @param shipPartLocations
	 * @return Ship
	 */
	static Ship getShip(String name, String shipType, Point[] shipPartLocations) {
		Ship ship = new Ship();
		ship.name = name;
		ship.shipType = shipType;
		ship.setShipBody(shipPartLocations);
		ship.destroyed = false;
		return ship;
	}
	
	/*
	 * getters and setters
	 */
	private void setShipBody(Point[] locations) {
		shipBody = new ShipPart[locations.length];
		for (int i = 0; i < locations.length; i++) {
			shipBody[i] = new ShipPart( locations[i].getX(), locations[i].getY() );
		}
	}
	
	private String getName() {
		return name;
	}

	private String getShipType() {
		return shipType;
	}
	

	/**
	 * evaluates {@code destroyed} using shipBody 
	 * unless {@code destroyed} has already been evaluated as {@value true} once.
	 * @return {@code destroyed} 
	 */
	boolean isDestroyed() {
		if ( !destroyed ) {
			destroyed = updateDestroyed();
		}
		return destroyed;
	}	
	
	private boolean updateDestroyed() {
		return countDestroyedShipParts() == shipBody.length;
	}
	
	private int countDestroyedShipParts() {
		int destroyedParts = 0;
		for (ShipPart shipPart : shipBody) {
			if ( shipPart.isDestroyed() ) {
				destroyedParts++;
			}
		}
		return destroyedParts;
	}
	
	
	@Override
	public String toString() {
		int percentDestroyed = (100 * countDestroyedShipParts()) / shipBody.length;
		return "Ship " + getName() + " (" + getShipType() + " class). Damage: " + percentDestroyed + "%)";
	}
}
