package ub_Project;

public class Point {
	
	private final int X;
	private final int Y;

	Point(int x, int y) {
		X = x;
		Y = y;
	}
	
	public int getX() {
		return X;
	}

	public int getY() {
		return Y;
	}

	public int getManhattanDistance(Point other) {
		return Math.abs( ( other.getX() - this.getX() ) + ( other.getY() - this.getY() ) );
	}
	
	@Override
	public String toString() {
		return "[" + getX() + "," + getY() + "]";
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( ! (obj instanceof Point) ) {
			return false;
		}
		Point other = (Point) obj;
		return getX() == other.getX() && getY() == other.getY();
	}
}
