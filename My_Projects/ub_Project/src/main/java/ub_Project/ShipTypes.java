package ub_Project;

enum ShipTypes {
	
	BATTLESHIP (5), CRUISER (4), DESTROYER (3), SUBMARINE (2);

	private final int SIZE;
	
	private ShipTypes(int size) {
		SIZE = size;
	}	
	
	public int size() {
		return SIZE;
	}
}
