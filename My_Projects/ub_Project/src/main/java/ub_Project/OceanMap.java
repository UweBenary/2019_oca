package ub_Project;

import java.util.ArrayList;
import java.util.List;

public class OceanMap {
	
	public final int X_MAX ;
	public final int Y_MAX ;
	private Field[][] fields;
	
	OceanMap (int xMax, int yMax) {
		if (xMax < 1 || yMax < 1) {
			throw new IllegalArgumentException("Arguments must be positive int.");
		}
		X_MAX = xMax;
		Y_MAX = yMax;
		fields = new Field[X_MAX][Y_MAX];
	}
	
	public Field getField(int x ,int y) {
		if ( fields[x][y] == null ) {
			return null;
		}
		return fields[x][y].getCopy();
	}
	
	public void setField(int x ,int y, Field field) {
		fields[x][y] = field;
	}

	public void hitField(int x, int y) {
		Field f;
		if ( fields[x][y] == null ) {
			f = new Field();
		} else {
			f = fields[x][y].getCopy();
		}
		f.hit();
		setField(x, y, f);		
	}


//	public List<Point> getHits() {
//		return new ArrayList<Point> (hits);
//	}
//
//	public ArrayList<ShipPart> getShipParts() {
//		return new ArrayList<ShipPart> (shipsParts);
//	}
//
//	public void addHit(Point p) {
//		if ( ! isLegal(p) ) {
//			throw new IllegalArgumentException(p + " out of bounds.");
//		}
//		if ( ! hits.contains(p) ) {
//			hits.add(p);
//		}
//	}
//
//	private boolean isLegal(Point p) {
//		if (p == null) {
//			return false;
//		}
//		if (p.getX() < 1 || p.getX() > X_MAX) {
//			return false;
//		}
//		if (p.getY() < 1 || p.getY() > Y_MAX) {
//			return false;
//		}
//		return true;
//	}
//
//	public void addShipPart(ShipPart shipPart) {
//		if ( ! isLegal(shipPart) ) {
//			throw new IllegalArgumentException(shipPart + " out of bounds.");
//		}
//		if ( shipsParts.contains(shipPart) ) {
//			throw new IllegalArgumentException("Cannot place ship at " + shipPart);
//		}	
//		shipsParts.add(shipPart);
//	}

}
