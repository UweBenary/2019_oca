package ub_Project;

public class MyOceanDisplayer {
	
	private OceanMap map;

	public MyOceanDisplayer(OceanMap map) {
		this.map = map;
	}
	
	public void displayMap() {
		
		for (int x = 0; x < map.X_MAX; x++) {
			System.out.format("%2d", (x + 1) );
		}
		System.out.println();
		
		for (int y = 0; y < map.Y_MAX; y++) {
			for (int x = 0; x < map.X_MAX; x++) {
				Field f = map.getField(x, y);
				displayField(f);
			}
			System.out.println(" " + (y+1) );
		}
	}

	private void displayField(Field field) {
		
		if ( field == null || ! field.hasShipPart() ) {
			System.out.print("  ");
			return;
		}
		if ( field.hasShipPart() ) {
			if ( field.isHit() ) {
				System.out.print("XX");
			} else {
				System.out.print("[]");
			}
		}		
	}
	
	public static void main(String[] args) {
		OceanMap ocean = new OceanMap(3, 5);
		MyOceanDisplayer displayer = new MyOceanDisplayer(ocean);
		ocean.setField(1, 1, Field.getInstanceWithShipPart() );
		ocean.setField(2, 1, Field.getInstanceWithShipPart() );
//		ocean.hitField(1,1);
		displayer.displayMap();
	}

}
