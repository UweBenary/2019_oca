package ub_Project;

public class Field {
	
	private boolean hasShipPart = false;
	private boolean isHit = false;
	
	public Field() {
		this(false, false);
	}

	private Field(boolean hasShipPart, boolean isHit) {
		this.hasShipPart = hasShipPart;
		this.isHit = isHit;
	}

	public boolean hasShipPart() {
		return hasShipPart;
	}

	public void placeShipPart() {
		this.hasShipPart = true;
	}

	public boolean isHit() {
		return isHit;
	}

	public void hit() {
		this.isHit = true;
	}

	public Field getCopy() {
		return new Field(hasShipPart, isHit);
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( ! (obj instanceof Field) ) {
			return false;
		}
		Field other = (Field) obj;
		return hasShipPart() == other.hasShipPart() && isHit() == other.isHit() ;
	}
	
	static Field getInstanceWithShipPart() {
		return new Field(true, false);
	}
	
}
