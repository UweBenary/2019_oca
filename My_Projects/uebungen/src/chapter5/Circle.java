package chapter5;

public class Circle {

	private double radius;
	private boolean filled;
	
	public Circle() {
		this(1,false);
	}
		
	public Circle(double radius) {
		this(radius,false);
	}	
	
	public Circle(double radius, boolean filled) {
		setRadius(radius);
		setFilled(filled);
	}
	
	
	public void setRadius(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return radius;
	}
		
	public boolean isFilled() {
		return filled;
	}

	public void setFilled(boolean filled) {
		this.filled = filled;
	}

	public double getCircumference() {
		return 2 * SomeMaths.PI * radius;
	}
	
	public double getArea() {
		return SomeMaths.PI * SomeMaths.getSquare(radius);
	}

}
