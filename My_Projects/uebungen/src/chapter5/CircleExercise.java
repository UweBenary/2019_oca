package chapter5;

public class CircleExercise {

	public static void main(String[] args) {

//		Circle c1 = new Circle();
//		Circle c2 = new Circle();
//		Circle c3 = new Circle();
//
//		c1.setRadius(3);
//		c2.setRadius(8);
//		c3.setRadius(5);
		
		Circle c1 = new Circle(3, true);
		Circle c2 = new Circle(8);
		Circle c3 = new Circle(5, true);
		
		printCircleData(c1);
		printCircleData(c2);
		printCircleData(c3);
	}
	
	static void printCircleData(Circle circle) {
		System.out.printf("Radius: %g%n", circle.getRadius());
		System.out.printf("Unfang: %g%n", circle.getCircumference());
		System.out.printf("Fläche: %g%n", circle.getArea());
		System.out.println("Der Kreis ist " + (circle.isFilled() ? "gefüllt." : "nicht gefüllt."));
		System.out.println();
	}

	
}
