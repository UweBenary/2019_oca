package ScratchBoard;

class A {
	void m() { System.out.print("A "); }
}

abstract class B extends A {
	abstract void m();
}

class C extends B {
	void m() { System.out.print("C "); }
}

public class Main {
	public static void main(String[] args) {
		A a = new C();
		a.m();
		
		C c = new C();
		c.m();
	}
}