package ScratchBoard;

public class Test3 {

	public static void main(String[] args) {

		int y = 0;
		System.out.println(y++);
		y = 0;
		System.out.println( (y++) );
		
		
		int x = 0;
		x = x++; // x = x; x + 1;
		System.out.print(x);
		x = ++x; // x = (x = (x + 1));
		System.out.print(x);
		x = x;

//		
//		for(;;) throw new Error();
//		System.out.println("");

//		if (true) {		throw new Error();	
//		}
//		System.out.println("");
	}

}
