
public class tset {

	private class subclass {
		int x = 2;
	}
	
	class subclass2 extends subclass {
		
		void dostuff() {
			System.out.println(x);
		}
	}
	
	public static void main(String[] args) {
		
		subclass2 t = new tset().new subclass2();
		t.dostuff();
	}
}
