package chapter8;

public class Main {

	public static void main(String[] args) {


		ZeitFormat zf1, zf2, zf3, zf4, zf5, zf6;
		
		zf1 = new ZeitFormat12();
		zf2 = new ZeitFormat12();
		zf3 = new ZeitFormat12();
		
		zf4 = new ZeitFormat24();
		zf5 = new ZeitFormat24();
		zf6 = new ZeitFormat24();
		
		zf1.setStunden(23);
		zf1.setMinuten(1);
		
		zf2.setStunden(12);
		zf2.setMinuten(00);
		
		zf3.setStunden(0);
		zf3.setMinuten(0);
		
		zf4.setStunden(23);
		zf4.setMinuten(23);

		zf5.setStunden(3);
		zf5.setMinuten(30);
		
		zf6.setStunden(0);
		zf6.setMinuten(0);
		
		zf1.printZeit();
		zf2.printZeit();
		zf3.printZeit();
		zf4.printZeit();
		zf5.printZeit();
		zf6.printZeit();
		
	}

}
