package chapter8;

public class ZeitFormat24 extends ZeitFormat {

	@Override
	public void printZeit() {
		System.out.println( this.zeit2String(this.getStunden()) + ":" + this.zeit2String(this.getMinuten()) );
	}

}
