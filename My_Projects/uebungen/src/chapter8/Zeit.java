package chapter8;

public class Zeit {

	private int stunden, minuten;

	public int getStunden() {
		return stunden;
	}

	public void setStunden(int stunden) {
		if (stunden<0 || stunden>23) {
			throw new IllegalArgumentException("Stunden müssen im Intervall [0 ... 23] liegen");
		}			
		this.stunden = stunden;
	}

	public int getMinuten() {
		return minuten;
	}

	public void setMinuten(int minuten) {
		if (minuten<0 || minuten>59) {
			throw new IllegalArgumentException("Minuten müssen im Intervall [0 ... 59] liegen");
		}			

		this.minuten = minuten;
	}
	
	
}
