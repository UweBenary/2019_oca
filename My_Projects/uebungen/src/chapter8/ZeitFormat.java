package chapter8;

public abstract class ZeitFormat extends Zeit {

	public abstract void printZeit();
	
	public String zeit2String(int x) {
		return x<10 ? "0" + Integer.toString(x) : Integer.toString(x);
	}

}
