package chapter8;

public class ZeitFormat12 extends ZeitFormat {

	@Override
	public void printZeit() {
	
		String amPm  = this.getStunden()>11 ? "PM " : "AM ";
		int h = this.getStunden()%12;
		System.out.println(amPm + this.zeit2String(h) + ":" + this.zeit2String(this.getMinuten()) );
		
	}
	
	

}
