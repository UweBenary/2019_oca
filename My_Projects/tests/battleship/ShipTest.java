package battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ShipTest {
		
	class DummyShip extends Ship {

		@Override
		String nameShip() {
			return null;
		}

		@Override
		ShipBody createShipBody() {
			return new ShipBody(0, 2, 1, 1);
		}

		
	}
	
	Ship dummyShip = new DummyShip();
	
	
	
	@Test
	void testIsDestroyed() {
		assertTrue( ! dummyShip.isDestroyed() );
	}
	
	@Test
	void testToString() {
		String expected = "Anonymous Ship (Damage: 0%)";
		String actual = dummyShip.toString();
		assertEquals(expected, actual);	}
		
	}
