package battleship;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class ShipTest2 {
		
	class DummyShip2 extends Ship {

		@Override
		String nameShip() {
			return "Dummy Ship";
		}

		@Override
		ShipBody createShipBody() {
			return new ShipBody(0, 2, 1, 1);
		}

	}
	
	Ship dummyShip2 = new DummyShip2();
	
	
	@Test
	void testIsDestroyed2() {
		dummyShip2.getSHIP_PART(0, 1).destroy();
		dummyShip2.getSHIP_PART(1, 1).destroy();
		dummyShip2.getSHIP_PART(2, 1).destroy();
		assertTrue( dummyShip2.isDestroyed() );
	}


	@Test
	void testToString2() {
		dummyShip2.getSHIP_PART(0, 1).destroy();
		dummyShip2.getSHIP_PART(1, 1).destroy();
		dummyShip2.getSHIP_PART(2, 1).destroy();
		String expected = "Dummy Ship (Damage: 100%)";
		String actual = dummyShip2.toString();
		assertEquals(expected, actual);	}

	
	@Test
	void testCountDestroyedShipParts() {
		dummyShip2.getSHIP_PART(0, 1).destroy();
		dummyShip2.getSHIP_PART(1, 1).destroy();
		dummyShip2.getSHIP_PART(2, 1).destroy();
		int expected = 3;
		int actual = dummyShip2.countDestroyedShipParts();
		assertEquals(expected, actual);
	}

}
