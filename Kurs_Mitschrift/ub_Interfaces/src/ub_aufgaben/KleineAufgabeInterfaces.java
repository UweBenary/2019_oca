package ub_aufgaben;

public class KleineAufgabeInterfaces {

	
	
	public static void main(String[] args) {

		/*
		 * A1
		 */
		Size s1 = new Size(20);
		System.out.println("s1: " + s1); // Size = 20          <- notwendige Ausgabe
		
		
		/*
		 * 
		 */
		Kreis k1 = new Kreis(3);
		System.out.println("k1: " + k1);
		
		/*
		 * 
		 */
		Size sA = new Size(12);
		Size sB = new Size(7);
		System.out.println( getMax(sA,sB) );

		Kreis kA = new Kreis(4);
		Kreis kB = new Kreis(77);
		System.out.println( getMax(kA,kB) );
	}
	
	static Object getMax(Sortable s1, Sortable s2) {
		if (s1!=null && s2!=null && s1.getClass() == s2.getClass() ) {
			int result = s1.compareWith(s2);
			if (result > 0) {
				return s1;
			} else if (result == 0) {
				return null;
			} else {
				return s2;
			}
		} else {
			return null;
		}
	}
	
//	static Object getMax(Object s1, Object s2) {
//		if (s1 instanceof Size && s2 instanceof Size) {
//			Size s1size = (Size) s1;
//			Size s2size = (Size) s2;
//			return ( s1size.getSize() > s2size.getSize() ) ? s1size : s2size;
//			
//		} else if (s1 instanceof Kreis && s2 instanceof Kreis) {
//			Kreis s1kreis = (Kreis) s1;
//			Kreis s2kreis = (Kreis) s2;
//			return ( s1kreis.getRadius() > s2kreis.getRadius() ) ? s1kreis : s2kreis;
//			
//		} else {
//			return null;
//		}		
//	}

}
