package ub_aufgaben;

public interface Sortable {
	
	int compareWith(Sortable s1);

}
