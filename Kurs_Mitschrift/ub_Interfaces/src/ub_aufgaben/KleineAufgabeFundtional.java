package ub_aufgaben;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

class Astronaut {
	String name;

	public Astronaut(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Astronaut " + name;
	}
}


public class KleineAufgabeFundtional {

	public static void main(String[] args) {


		String[] namen = {"Tom", "Jerry","Peter", "Paul", "Mary", "Ute"};
		

		List<Astronaut> listA = buildMannschaft(namen, n-> new Astronaut(n) );

		System.out.println(listA);
		
	}

	private static List<Astronaut> buildMannschaft(String[] namen, Function<String,Astronaut> fun) {
		List<Astronaut> list = new ArrayList<Astronaut>();
		for (String name : namen) {
			list.add( fun.apply(name) );
		}
		return list;
	}



}
