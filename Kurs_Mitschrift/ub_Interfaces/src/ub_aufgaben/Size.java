package ub_aufgaben;

public class Size 
	implements Sortable{
	
	private int size;
	
	public Size(int size) {
		setSize(size);
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Size = " + getSize();
	}

	@Override
	public int compareWith(Sortable s1) {
		return this.getSize() - ((Size) s1).getSize() ;			
	}
}
