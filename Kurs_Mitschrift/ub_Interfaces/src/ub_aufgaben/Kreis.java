package ub_aufgaben;

public class Kreis 
	implements Sortable {

	private int radius;

	public Kreis(int radius) {
		setRadius(radius);
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}
	
	@Override
	public String toString() {
		return "Kreis. Radius = " + getRadius();
	}
	
	@Override
	public int compareWith(Sortable s1) {
		return this.getRadius() - ((Kreis) s1).getRadius() ;			
	}
}
