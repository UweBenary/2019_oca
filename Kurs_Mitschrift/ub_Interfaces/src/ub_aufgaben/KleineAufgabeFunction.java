package ub_aufgaben;

import java.util.function.Function;

public class KleineAufgabeFunction {

	public static void main(String[] args) {

		Function<Integer, Integer> f = x -> x + x;
		
		int result = f.apply(12);
		System.out.println( result );

	}

}
