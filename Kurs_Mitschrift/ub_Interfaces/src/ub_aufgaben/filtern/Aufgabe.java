package ub_aufgaben.filtern;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class Aufgabe {

	public static void main(String[] args) {
		
		List<Integer> list = new ArrayList<Integer>();
		
		list.add(12);
		list.add(13);
		list.add(14);
		list.add(15);
		list.add(-4);
		list.add(77);
		
		System.out.println(list);
		
		ArrayList<Integer> listGerade = filtern(list);
		System.out.println(listGerade);
		System.out.println(list);
		
		Predicate<Integer> pred =  n -> n % 2 == 1;  
		listGerade = filtern(list, pred);
		System.out.println(listGerade);
		System.out.println(list);
	}

	private static ArrayList<Integer> filtern(List<Integer> list) {
		ArrayList<Integer> returnList = new ArrayList<Integer>(list);
		returnList.removeIf(n -> n % 2 == 1);
		return returnList;
	}
	
	private static ArrayList<Integer> filtern(List<Integer> list, Predicate<Integer> filter) {
		ArrayList<Integer> returnList = new ArrayList<Integer>(list);
		returnList.removeIf(filter);
		return returnList;
	}

}
