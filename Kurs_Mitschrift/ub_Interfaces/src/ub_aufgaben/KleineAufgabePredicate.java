package ub_aufgaben;

import java.util.ArrayList;
import java.util.function.Predicate;

class Kreis2 {
	private int radius;

	public Kreis2(int radius) {
		this.radius = radius;
	}

	public int getRadius() {
		return radius;
	}
	
	@Override
	public String toString() {
		return "Kreis. R = " + radius;
	}
	
}

public class KleineAufgabePredicate {

	public static void main(String[] args) {


		ArrayList<Kreis2> list = new ArrayList<Kreis2>();
		list.add(new Kreis2(3) );
		list.add(new Kreis2(17) );
		list.add(new Kreis2(2) );
		list.add(new Kreis2(22) );
		list.add(new Kreis2(8) );
		
		Predicate<Kreis2> p = k -> k.getRadius() > 10;
		
		for (Kreis2 kreis : list) {
			if (p.test(kreis)) {
				System.out.println(kreis);
			}
		}
		
	}
}
