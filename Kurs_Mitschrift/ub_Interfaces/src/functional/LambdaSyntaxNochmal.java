package functional;

public class LambdaSyntaxNochmal {

	interface Combiner {
		int combine(int a, int b);
	}
	
	static class Adder implements Combiner {

		@Override
		public int combine(int a, int b) {
			return a + b;
		}
		
	}
	public static void main(String[] args) {

		Combiner c = new Adder();
		
		System.out.println( c.combine(2, 3) );

		Combiner c2 = (x, y) -> y + x;
		
		System.out.println( c2.combine(2, 3) );
	}

}
