package ub3_Arrays;

class Rechteck {
	int breite;
	int hoehe;
}

public class UB01_ArraysMitPrimitiven {
	
	public static void main(String[] args) {
		
//		int[] array = new int[3];
//		
//				
//		Rechteck r1 = new Rechteck();
//		r1.breite = 3;
//		System.out.println(r1.breite);
		
		/*
		 * Aufgabe:
		 * boolean-Array Länge 200
		 * jede 2. Position true
		 * Array ausgeben
		 */
		
		boolean[] b = new boolean[200];
		
		for (int i = 0; i < b.length; i++) {
			if ((i%2)==0) {
				b[i] = true;
			} else {
				b[i] = false;
			}
		}
		for(boolean var : b) {
			System.out.println(var);
		}
	}

}
