package ub3_Arrays;

public class UB02_KleineAufgabeEPi {
	
	public static void main(String[] args) {
		
		double[] arr1 = new double[2];
		
		arr1[0] = Math.PI;
		arr1[1] = Math.E;
		
		for (double var : arr1) {
			System.out.println(var);
		}
	}

}
