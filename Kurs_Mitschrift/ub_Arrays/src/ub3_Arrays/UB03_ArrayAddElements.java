package ub3_Arrays;

public class UB03_ArrayAddElements {
	
	public static void main(String[] args) {
		
		int[] arr1 = { 1, 2, 3 };
		int[] arr2 = { 11, 12, 13};
		int[] arr3 = { 11, 12, 13, 0};
		
		for (int i : addArrays(arr1, arr2)) {
			System.out.println( i );
		}
		
		for (int i : addArrays(arr1, arr3)) {
			System.out.println( i );
		}
		
	}

	public static int[] addArrays(int[] a1, int[] a2) {
		
		if(a1.length == a2.length) {
			
			int[] result = new int[a1.length];
			
			for (int i = 0; i < a1.length; i++) {
				result[i] = a1[i] + a2[i];
			}
			return result;
		
		} else {
			System.out.println("Error: Arrays must have same dimensions!");
			return new int[0];
		}
	}
}
