package ub_strings;

public class KleineAufgabeStringIndexOf {

	public static void main(String[] args) {

		String s = "Die Note Do ist nicht dasselbe wie Do, wenn Do für Donnerstag steht";
		
		/*
		 * A1.
		 * 
		 * Suchen Sie bitte alle Positionen im String s, wo der Unterstring 'Do' steht.
		 * geben Sie die Positionen aus: 
		 */
		int index = 0;
		do {
			index = s.indexOf("Do", index);
			if (index >= 0) {
				System.out.println(index);
			}
			index++;
		} while (index > 0);
		
	}

}
