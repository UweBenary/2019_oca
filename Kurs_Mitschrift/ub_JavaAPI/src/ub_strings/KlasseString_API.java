package ub_strings;

public class KlasseString_API {

	public static void main(String[] args) {

//		konstruktoren();
		
		length_und_charAt();
		
	} // end of main

	static void length_und_charAt() {
		System.out.println("*** int length()");
		
		String s1 = "Java";
		System.out.println( "s1.length(): " + s1.length() ); // 4 
		
		System.out.println( "*** char charAt(int)"  );
		System.out.println( "s1.charAt(2): " + s1.charAt(2) ); // v
	
		a1();
	}
	
	static void konstruktoren() { 
		System.out.println("*** Konstruktoren");
		String s1 = new String();
		String s2 = new String("Java");
	}
	
	
	/*
	 * A1
	 * 
	 *  Bitte geben Sie den String rückwärts aus
	 */
	static void a1() {
		String s = "Heute ist Mittwoch";
		
//		char[] sChar = s.toCharArray();
//		char[] sChar2 = new char[sChar.length];
//		for (int i = 0; i < sChar.length; i++) {
//			sChar2[sChar2.length-1 - i] = sChar[i];
//		}
//		String sRev = new String(sChar2);
//		System.out.println(sRev);
		
		for (int i = s.length()-1; i >=0; i--) {
			System.out.print(s.charAt(i));
		}
	}
}
