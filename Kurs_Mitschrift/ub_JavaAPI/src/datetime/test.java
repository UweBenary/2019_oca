package datetime;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class test {

	public static void main(String[] args) {


		LocalDate date = LocalDate.of(2000, Month.FEBRUARY, 28+1);
		System.out.println(date);
		

		LocalDate date2 = LocalDate.of(-2000, Month.FEBRUARY, 28+1);
		System.out.println(date2);
		

		System.out.println(LocalDate.now().minus(2, ChronoUnit.YEARS));
		
		System.out.println(	date.withYear(1000).isLeapYear() );
	
		DateTimeFormatter fmt = DateTimeFormatter.ofPattern("y yy yyy yyyyyy MMM MMMMM");
		System.out.println( date.withYear(1987).format(fmt) );
	
	}

}
