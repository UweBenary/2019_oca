package datetime;

import java.time.LocalDate;

public class KleineAufgabe {

	public static void main(String[] args) {
		LocalDate date = LocalDate.now().withYear(2000);
		boolean isLeap = date.isLeapYear();
		System.out.println( isLeap );

		
		for (int i = 1890; i < 2021; i+=2) {
			
			LocalDate date2 = LocalDate.now().withYear(i);
			if ( date2.getYear() % 4 == 0 && ! date2.isLeapYear() ) {
				System.out.println( date2.getYear() );
			}
		}
	}

}
