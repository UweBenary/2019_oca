package format;

import java.util.Arrays;
import java.util.Random;

public class KleineAufgabeFormat {

	public static void main(String[] args) {


		/*
		 * A1
		 * 
		 * |    Nr.|    Wert  |
		 * |    01 |   121314 |
		 * |    02 |      -22 |
		 */
		int[] array = randomArray(); // Array Länge 10 mit Zufallszahlen aus Bereich [-1000 ... 1000]
//		System.out.println(Arrays.toString(array));
		print(array); 

	}

	private static void print(int[] array) {

		System.out.println(" |    Nr.|    Wert  |");
		for (int i = 0; i < array.length; i++) {
					System.out.printf( " |    %02d |%9d |%n", i+1, array[i]);
		}		
	}

	private static int[] randomArray() {
		Random rand = new Random();
		int[] array = new int[10];
		int min = -1000;
		int max = 1000;
		for (int i = 0; i < array.length; i++) {
			array[i] = rand.nextInt( (max+1) - min) + min;
		}
		return array;
	}

}
