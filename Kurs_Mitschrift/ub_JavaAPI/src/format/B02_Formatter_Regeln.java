package format;

public class B02_Formatter_Regeln {

	public static void main(String[] args) {

		System.out.format("Ausgabe 1 ");
		System.out.format("Ausgabe 2 \n");
		
		/*
		 * %n - line separator
		 */
		System.out.format("Ausgabe 3 %n Ausgabe 4 %n");
		
		
		
	}

}
