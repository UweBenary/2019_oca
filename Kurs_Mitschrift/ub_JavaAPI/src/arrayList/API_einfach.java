package arrayList;

import java.util.ArrayList;

public class API_einfach {

	public static void main(String[] args) {

		ArrayList<String> list = new ArrayList<String>();
		list.add("mo");
		list.add("fr");
		list.add("di");
		list.add("fr");
		list.add("mi");
		list.add("fr");
		list.add("do");
		list.add("fr");
		
		System.out.println(list);
		
		boolean result;
		do {
			result = list.remove("fr");
		} while (result);

		System.out.println(list);
		
	}

}
