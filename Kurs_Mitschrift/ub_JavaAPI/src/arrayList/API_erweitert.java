package arrayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;


class StringComperator implements Comparator<String> {

	@Override
	public int compare(String o1, String o2) {
		return o1.length() - o2.length();
	}
}

public class API_erweitert {

	public static void main(String[] args) {



	/*
	 * Kleine Aufgabe:
	 * 
	 * - Erzeugen Sie eine ArrayList mit "montag", "di", "mittw.", "freitagabend", "samstag"
	 * - Verwenden Sie bitte die sort-Methode der Klasse 'ArrayList' 
	 *   um die Liste nach Länge der Strings zu sortieren
	 * 
	 * 
	 */
	String[] arr = {"montag", "di", "mittw.", "freitagabend", "samstag"};
	List<String> list = new ArrayList<String>( Arrays.asList(arr) );
	System.out.println(list);
	
	Comparator<String> cmp = new StringComperator();
	list.sort(cmp);
	System.out.println(list);
	
	
	} // end of main
	
}
















