package ub_konsole;

import java.util.Scanner;

public class KlasseScanner {

	public static void main(String[] args) {
		String stringSource = "a b, . hallo d";
		Scanner sc = new Scanner(stringSource);
		
		String element;
		while (sc.hasNext()) {
			element = sc.next();
			System.out.println(element);
		}
		
		sc.close();
	}
}
