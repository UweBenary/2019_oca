package ub_kleineAufgabeExceptions;

public class KleineAufgabe1 {

	public static void main(String[] args) {


		String name = "Jerry";
		
		name = formatName(name);
		
		System.out.println("Name: " + name);

		name = null;
		
		try {
			name = formatName(name);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		formatName("Do");
		
		System.out.println("end of main");
	
		
	}

	private static String formatName(String name) {
		if (name.length() < 3) {
				throw new IllegalArgumentException("Der Name muss mindestens 3 Zeichen haben");
		}
		
		return name.toUpperCase();
	}

}
