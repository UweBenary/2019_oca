package ub_kleineAufgabeExceptions;

import java.util.Random;

public class KleineAufgabe3 {

	public static void main(String[] args) {

		boolean isException;
		
		do {
			try {
				maybeException();
				isException = false;
		
			} catch (Exception e) {
				System.out.println("Exception: " + e.getMessage());
				isException = true;
			}
		
		} while (isException);
		
		System.out.println("end of main");
		
	}

	static void maybeException() {
		if ( new Random().nextBoolean() ) {
			throw new NullPointerException();
		}
	}
}
