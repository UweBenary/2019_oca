package ub_kleineAufgabeExceptions;

public class KleineAufgabe2 {

	public static void main(String[] args) {


//		int Integer.parseInt(String);
		
		System.out.println( isInteger("10") ); //true
		System.out.println( isInteger("Hallo Welt") ); //false

//		System.out.println( isInteger("10 Welt") ); //false
//		System.out.println( isInteger("-10") ); //true
//		System.out.println( isInteger("+10") ); //true
//		System.out.println( isInteger("10.2") ); //false
	}

	private static boolean isInteger(String string) {
		try {
			Integer.parseInt(string);
			return true;
			
		} catch (NumberFormatException e) {
			return false;
		}
	}

}
