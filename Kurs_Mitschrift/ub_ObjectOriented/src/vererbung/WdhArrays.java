package vererbung;

import java.util.Arrays;

class Wochentag {
	String wochentag;

	public Wochentag(String wochentage) {
		this.wochentag = wochentage;
	}
	
	@Override
	public String toString() {
		return wochentag;
	}
}


public class WdhArrays {

	public static void main(String[] args) {
		
		String[] days = createDaysArray();
		System.out.println( Arrays.toString(days) );
		
		Wochentag t1 = new Wochentag( days[0] );
		System.out.println(t1);
		
		Wochentag[] wochentagArray = convertieren(days);
		System.out.println( Arrays.toString( wochentagArray ));
		
	}
	

	static String[] createDaysArray() {
		return new String[] {"mo", "di", "mi", "do", "fr", "sa", "so"};
	}
	
	static Wochentag[] convertieren(String[] days) {
		Wochentag[] wochentage = new Wochentag[ days.length ];
		for (int i = 0; i < wochentage.length; i++) {
			wochentage[i] = new Wochentag( days[i] );			
		}
		return wochentage;
	}
}
