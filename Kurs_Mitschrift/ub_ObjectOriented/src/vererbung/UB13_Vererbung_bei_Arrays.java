package vererbung;

import java.util.Arrays;

import vererbung.B13_Vererbung_bei_Arrays.Person;

public class UB13_Vererbung_bei_Arrays {

		static class Person {	}
		static class Dozent {	}
	
	public static void main(String[] args) {

		/*
		 * Arrays mit primitiven Elementen sind Geschwisterklassen
		 */
		
		int[] a1 = { 1, 2, 3 };
		
		// long[] a2 = a1; 			// Compilerfehler: keine IS-A-Beziehung
		
		// Object[] arrObj = a1;	// Compilerfehler: keine IS-A-Beziehung
		
		float[] a3 = { 1F, 0.5F, 7.7F };
		
		print(a3);
		
		double[] a4 = { 3.3, 4.4, 5.5 };
		
		print(a4);
		
		Object[] objArr = { new Person(), new Dozent()	};
		
		print(objArr);
		print(new String[] {"mo", "di"});
		print(new Person[2]);
		
	} // end of main
	
	
	/*
	 * Aufgabe A1.
	 * 
	 * Definieren Sie bitte eine (!) Methode print, die als Argument
	 * sowohl float[] als auch double[] akzeptiert. Die print soll alle Elemente des Arrays
	 * untereinander ausgeben.
	 * 
	 *  Die print muss keine anderen Arrays mit primitiven unterstützen.
	 */
	
	static void print(Object arr) {
		if (arr instanceof double[]) {
			System.out.println("print(x) has x instanceof double[] :");
			double[] arr2 ;
			arr2 = (double[]) arr;
			for (double d : arr2) {
				System.out.println(Double.toString(d));
			}	
		} else if (arr instanceof float[]) {
			System.out.println("print(x) has x instanceof float[] :");
			float[] arr2 ;
			arr2 = (float[]) arr;
			for (float f : arr2) {
				System.out.println(Float.toString(f));
			}	
//		} else {
//			System.out.println( Arrays.toString( (Object[]) arr) );
		}
	}
	
	static void print(Object[] arr) {
		for (Object object : arr) {
			System.out.println( object );
		}
	}
	

	
}
