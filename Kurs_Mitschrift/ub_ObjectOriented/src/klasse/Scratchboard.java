package klasse;

public class Scratchboard {

	public static void main(String[] args) {
		
		int a = 2;
		int b = a;
		a = 1;
		System.out.println(b);
		
		int[] arr = new int[1];
		arr[0] = 2;
		int[] brr = arr;
		arr[0] = 1;
		System.out.println(brr[0]);
		
		
		int[] ar = new int[1];
		ar[0] = 2;
		int[] br = new int[1];
		br[0] = ar[0];
		ar[0] = 1;
		System.out.println(br[0]);
		
	}
}
