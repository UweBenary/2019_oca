package klasse;

public class UB02_PlanetenOhneKlassen {
	
	public static void main(String[] args) {
		
		/*
		 * 1. Planet anlegen: 
		 * 		Name: Erde, Koordinaten x= 12, y=13
		 */
		
		String name1 = "Erde";
		int x1 = 12;
		int y1 = 13;
		
		/*
		 * Infos zu Planeten ausgeben
		 */
		
//		System.out.println("Planet " + name1 + ", Koordinaten: " + x1 + ", " + y1);
		printPlanet ( name1, x1, y1);
		
		
		/*
		 * Planet bewegen
		 */
		
		x1 = 33;
		y1 = 40;
		
//		System.out.println("Planet " + name1 + ", Koordinaten: " + x1 + ", " + y1); // get rid of duplicated code
		printPlanet ( name1, x1, y1);
		
	}
	
	static void printPlanet ( String name, int x, int y) {
		System.out.println("Planet: " + name + ", Koordinaten: " + x + ", " + y);
	}

}
