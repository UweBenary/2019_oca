package klasse;

public class UB05_Wiederholung {

	class Wdh {
		
		int x;
		
//		void Wdh() { // compiliert aber heißt wie constructor!
//			
//		}
		
//		Wdh() {  // Compiler fehler, weil cycling call of constructor
//			this(2);
//			}
//		Wdh(int x) { 
//			this();
//			}
		
		void m1() {
			m2();
		}
		
		void m2() {
			m1();
		}
	}
	
	public static void main(String[] args) {
		
//		Wdh.m1(); // Compiler keine static


//		new Wdh().m1(); // Runtime Stack overflow
	}
}
