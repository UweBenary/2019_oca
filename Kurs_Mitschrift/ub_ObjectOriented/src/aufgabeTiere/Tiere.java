package aufgabeTiere;

class Hund {
	private String name;
	private int age;
	private String owner;
	
	public Hund(String name, int age){
		this(name, age, "");
	}
	
	public Hund(String name, int age, String owner){
		this.name = name;
		this.age = age;
		this.owner = owner;
	}
	
	public String toString() {
		return "Der Hund " + this.name + " ist " + this.age + " Jahre alt und gehört " + this.owner +".";
	}
	
	public boolean equals(Object param) {
		Hund hParam = (Hund) param;
		if (this.name.equals(hParam.name) &&
				this.age == hParam.age &&
				this.owner.equals(hParam.owner)) {
			return true;
		} else {
			return false;
		}			
	}
	
	public void setHund(String name, int age, String owner) {
		this.name = name;
		this.age = age;
		this.owner = owner;
	}

	
}

public class Tiere {
	static public void main(String[] args) {
		
		Hund h1 = new Hund("Rex", 6);
		h1.setHund( "Rex", 6, "ich");
		
		Hund h2 = new Hund("Lassie", 7);
		System.out.println(h2);
		h2.setHund( "Rex", 6, "ich");
		System.out.println(	h1.equals(h2) );		
		
		Hund h3 = new Hund("Lassie", 7, "ich");
		System.out.println(	h1.equals(h3) );		
	}	
}
