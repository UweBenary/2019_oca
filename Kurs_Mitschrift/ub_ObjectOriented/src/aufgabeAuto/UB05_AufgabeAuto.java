package aufgabeAuto;

/*
 * definieren Sie eine Klasse 'Auto' mit den Attributen 'modell' und 'speed'.
 * 
 * 1. Erzeugen Sie ein Auto-Objekt.
 * 2. Setzen Sie sinnvolle Werte für das 'modell' und 'speed'
 *
 * 3. Versuchen Sie eine statische Methode printAuto zu definieren, die alle Auto-Infos ausgibt.   
 * 4. Versuchen Sie eine Instanz-Methode printAuto zu definieren, die alle Auto-Infos ausgibt.   
 * 
 */

class Auto {
	String modell;
	int speed;
	
	/*
	 * @ 3.
	 */
	static void printAuto(Auto a) {
		System.out.println("Modell: " + a.modell + " Speed: " + a.speed);
	}
	
	/*
	 * @ 4.
	 */
	void printAuto() {
		System.out.println("Modell: " + modell + " Speed: " + speed);
	}
}

public class UB05_AufgabeAuto {
	
	public static void main(String[] args) {
			
		/*
		 * @ 1.
		 */
		Auto auto1 = new Auto();
		/*
		 * @ 2.
		 */		auto1.speed = 200;
		auto1.modell ="Opel";
			
		/*
		 * @ 3.
		 */
		Auto.printAuto(auto1); 
		
		/*
		 * @ 4.
		 */
		auto1.printAuto(); 
		
	}



	
	
	

}
