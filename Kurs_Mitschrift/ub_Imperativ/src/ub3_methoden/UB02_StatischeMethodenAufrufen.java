package ub3_methoden;

public class UB02_StatischeMethodenAufrufen {

	public static void main(String[] args) {


		UB01_StatischeMethodenDefinieren.sayHello();
		sayBye();
		
	}
	
	static void sayBye() {
		System.out.println("ByeBye");
	}

}
