package ub3_methoden;

public class UB01_StatischeMethodenDefinieren {
	
	public static void main(String[] args) {
		System.out.println( 123456789 + 5432L < Integer.MAX_VALUE);
	}
	
	static void sayHello() {
	
		System.out.println("Hello World!");
	}
}
