package ub1_variablen;

public class UB1_PrimitiveDatentypen {

	public static void main(String[] args) {
		
		System.out.println( "Max. int: " + Integer.MAX_VALUE);
		System.out.println( "Min. int: " + Integer.MIN_VALUE);
		System.out.println( "Max. float: " + Float.MAX_VALUE);
		
		int a = 10;
		int b = 11;
		int c = 12;
		int d = 13;
		int e = 14;
		int f = 15;
		
		double deadcafe = d * Math.pow(16, 7) + e * Math.pow(16, 6) + a * Math.pow(16, 5) + d * Math.pow(16, 4) + c * Math.pow(16, 3) + a * Math.pow(16, 2) + f * Math.pow(16, 1) + e * Math.pow(16, 0); 
		
		System.out.println( deadcafe );
		
		System.out.println( 0XDeadCafe );
		
		System.out.println( 0XDeadCafe > Integer.MIN_VALUE );
		
		System.out.println( deadcafe > Integer.MAX_VALUE );
		
		
		System.out.println("Float: " + 2_000_000_0000F == 2_000_000_0000F+1);
		System.out.println("Double: " + 2_000_000_0000D == 2_000_000_0000D+1);
		
	}
}
