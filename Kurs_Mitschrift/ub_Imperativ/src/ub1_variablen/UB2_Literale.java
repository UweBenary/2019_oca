package ub1_variablen;

public class UB2_Literale {

	public static void main(String[] args) {
	
		System.out.println( 1 == 1. );
		System.out.println( 1 + 1. + 1.  == 3. );
		
		System.out.println( .1 + .1 + .1 );
		System.out.println( .1 + .1 + .1 == .3 );
	
		System.out.println( .001 + .001 == .002 );
		
		System.out.println( ".3f * 1E11f = " + .3f * 1E11f );
		System.out.println( ".3f * 1E11 = " + .3f * 1E11 );
		System.out.println( ".3 * 1E11 = " + .3 * 1E11 );
		
		
	}
}
