package ub2_operatoren;

public class UB1_Zuweisungsoperator {

	public static void main(String[] args) {
		
		/*
		 * int x;
		 * System.out.println( x );
		 */
		
		
		long var6 = Long.MAX_VALUE;
		
		int var7 = (int) var6;
		
		System.out.println(var6);		
		System.out.println(var7);
		
		
//		float f = 2e3; // Compilerfehler, weil rechts typ double
		float f = 2000;// Compilt, weil Ausnahmeregelung int-Literal->Es wird nach Wert geprüft
		
	
		
		
//				int hallo = 1_000_000;  // unterstrich geht weil eingesetzt zur besseren lesbarkeit (nicht vorn nicht zuletzt)
		
		
	}
}
