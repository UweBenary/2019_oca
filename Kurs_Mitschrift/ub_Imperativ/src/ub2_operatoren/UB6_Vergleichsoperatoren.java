package ub2_operatoren;

public class UB6_Vergleichsoperatoren {
 
	public static void main(String[] args) {
	
		double x = 3. / 10;
		System.out.println(x);
		
		char ch = 97;
		int i = 97;
		
		System.out.println(ch == i);
		
		ch = 'a';
		
		System.out.println(ch == i);
		
	}
}
