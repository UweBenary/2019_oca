package ub2_operatoren;

public class UB3_ZuweisungMitOperation {

	public static void main(String[] arg) {
		

		int a = 2;
		int b = 3;
		
		a += b;
		/*
		 * fast dasselbe wie a = a + b;
		 */
		
		System.out.println(a);
		
		
		byte b1 = 2;
		byte b2 = 3;
		
//		b1 = b1 + b2; // Compilerfehler, weil ergebnis int
		
		b1 += b2; // geht weil impliziertes casting auf Typ von b1: b1 = (byte) (b1 + b2);
		System.out.println(b1); 
		
		b1 += 1000000; // geht weil impliziertes casting auf Typ von b1: b1 = (byte) (b1 + b2);
		System.out.println(b1); 
		
	
//		
		
//		a += b - 2 ;
//		
//		System.out.println(a);
//		
		
		
		
	}
	
}
