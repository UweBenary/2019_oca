package ub2_operatoren;

public class UB5_Logische {

	public static void main(String[] args) {
		
		boolean b1 = true;
		System.out.println("!b1 = " + !b1);
		
		
		int x = 0;
		int y = 0;
		
		boolean result = x==1 & ++y==1;
		
		System.out.println("& result " + result);
		System.out.println("& y = " + y);	
		
		
		x = 0;
		y = 0;
		
		result = x==1 && ++y==1;
		
		System.out.println("&& result " + result);
		System.out.println("&& y = " + y);	
		
	}
}
