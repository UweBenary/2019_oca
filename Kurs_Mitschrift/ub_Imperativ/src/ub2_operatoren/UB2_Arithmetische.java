package ub2_operatoren;

public class UB2_Arithmetische {

	public static void main(String[] arg) {
		
		System.out.println(1 + 1);
		System.out.println(1 + 1 + .0);
		
		
		byte var1 = 1;
		byte var2 = 2;
		
		byte var3 = 1 + 2; // Compiled weil Int-Literal impliziert gecastet wird und 3<=127
//		byte var3 = var1 + var2; // Compilerfehler, weil Oprator-ergebnis mind Typ int ist
		
		
//		System.out.println("Test: " + 5 / 0); // nicht kompiliert
		int x = 0;
//		System.out.println("Test: " + 5 / x); // kompiliert aber mit exception
		System.out.println("Test2: " + 5. / x); // Infinity
	
		double d = Double.POSITIVE_INFINITY;
	
		double x2 = 0.;
		System.out.println("Test2: " + 0 / x2); // Infinity
		System.out.println("Test2: " + x2 / x); // Infinity
	}
}
