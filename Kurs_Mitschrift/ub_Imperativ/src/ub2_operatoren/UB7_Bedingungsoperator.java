package ub2_operatoren;

public class UB7_Bedingungsoperator {

	public static void main(String[] args) {
		
		int x = true ? 22 : 0;
		
		System.out.println("x = " + x);
		
		boolean result = x==1000 ? true : false; // geht aber sinnlos kompliziert
		result = x==1000;
		
		
		double z = x > 0 ? 77 : 5.3; // geht für typ double
		
		boolean y = x > 0 ? x==22 : false; // geht für typ double
	}
}
