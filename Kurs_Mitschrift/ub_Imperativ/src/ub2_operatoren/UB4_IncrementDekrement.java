package ub2_operatoren;

public class UB4_IncrementDekrement {

	public static void main(String[] args) {
	
		int x = 0;
		
//		x++; // x = x + 1;
//		
//		System.out.println(x);
//		
//		++x; // x = x + 1;
//		
//		System.out.println(x);
//		
//		x = 0;
		
		System.out.println(x++);// zuerst ausgeben, dann erhöhen
		System.out.println(x);
		
		x = 0;
		
		System.out.println(++x);// zuerst erhöhen, dann ausgeben
		System.out.println(x);
	
		
		byte b = 3;
		byte c = ++b; // casting
		System.out.println("c = " + c);
		
		x = 0;
		System.out.println(++x + x);
		x = 0;
		System.out.println(x++ + x);
		x = 0;
		System.out.println(x + x++);
		x = 0;
		System.out.println(x + ++x);
		
		//
		int hallo = 0;
		hallo = hallo++; // Postinkrement: 1. Hallo-Wert in Register schreiben; 2. Postdekrement; 3. Register-Wert in Hallo abspeichern -> hallo bleibt 0
		System.out.println("hallo: " + hallo); // 
		
		hallo = 0;
		hallo = ++hallo; // Präinkrement: 1. Präimkrement 2. neuer Hallo-Wert in Register schreiben; 3. Register-Wert in Hallo abspeichern -> hallo ist 1
		System.out.println("hallo: " + hallo); // 
		
		System.out.println("---------------");
		int i = 1;
		System.out.println( Math.max(i++, --i));
		
	}
}
