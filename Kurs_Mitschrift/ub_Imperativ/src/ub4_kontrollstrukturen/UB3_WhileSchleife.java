package ub4_kontrollstrukturen;

public class UB3_WhileSchleife {

	public static void main(String[] args) {
		
		for(int i=0; i<3; i++) {
			
			do {
				break; 
//				continue;
			} while(false);
			
			System.out.println("i: " + i);
		
		}
		
	}
}
