package ub4_kontrollstrukturen;

public class UB2_ForSchleife {

	public static void main(String[] args) {

		for(int x = 5; x >= 1; x--) {
			System.out.println("x: " + x);
		}
		
		
		for(int x = 0; x < 4; ) {
			System.out.println( x++ );
		}

	}

}
