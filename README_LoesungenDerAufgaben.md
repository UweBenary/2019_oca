## Code solutions to tasks of the Java course


###### Tasks:

[https://github.com/ccjavadozent/oca_162982/tree/master/Aufgaben/](https://github.com/ccjavadozent/oca_162982/tree/master/Aufgaben/ "Hyperlink to tasks @ github.com")

###### My code solutions:

[https://bitbucket.org/UweBenary/2019_oca/src/master/Loesungen_der_Aufgaben/src/](https://bitbucket.org/UweBenary/2019_oca/src/master/Loesungen_der_Aufgaben/src/ "Hyperlink to code solutions @ bitbucket.org")

#### 11Nov2019

###### Task 'DateAndTime'
- Done.
- Code in: ub54_dateAndTime


#### 08Nov2019

###### Task 'Deutsche Städte'
- Done.
- Code in: ub53_germanCities

###### Task 'Calculator'
- Done.
- Code in: ub52_calculator


#### 06Nov2019

###### Task 'Exceptions - ParseDate'
- Done.
- Code in: ub51_exceptionsParseDate


#### 04Nov2019

###### Task 'Enums-Hunderassen'
- Done.
- Code in: ub50_enumDogs


#### 30Oct2019

###### Task 'Lambdas'
- Done.
- Code in: ub49_lambdasPredicateJavaTipp


#### 29Oct2019

###### Task 'Lambdas'
- Done.
- Code in: ub48_lambdas


#### 28Oct2019

###### Task 'Innere Klassen - Autos.md'
- Done.
- Code in: ub47_innerClassCars

###### Small task 'Innere Klasse'
- Done.
- Code in: ub46_kleineAufgabeInnereKlasse


#### 25Oct2019

###### Task 'String - StringBuilder.md'
- Done.
- Code in: ub45_stringBuilder

###### Task 'Wrapper - Zahlen laden.md'
- Done.
- Code in: ub44_wrapperLoadNumbers


#### 24Oct2019

###### Task 'ArrayList-erweitert.md'
- Done.
- Code in: ub43_arrayListExtended


#### 23Oct2019

###### Task 'ArrayList.md'
- Done.
- Code in: ub42_arrayList


#### 21Oct2019

###### Task 'String-Silben.md'
- Done.
- Code in: ub41_personenSortFormat


#### 18Oct2019

###### Task 'String-Silben.md'
- Done.
- Code in: ub40_lesenAusEinerTextdatei


#### 17Oct2019

###### Task 'String-Silben.md'
- Done.
- Code in: ub39_stringSilben

###### Task 'String-API - Methoden.md'
- Done. 
- Code in: ub38_stringAPImethodes 


#### 16Oct2019

###### Task 'Interfaces - default.md'
- Done. 
- Code in: ub37_interfacesDefault  


#### 15Oct2019

###### Task 'Interfaces - Comparable/Comparator - Person'
- Done. 
- Code in: ub36_interfacesCompPerson  

###### Small task 'kleineAufgabeCompareable'
- Done. 
- Code in: ub35_kleineAufgabeCompareable  


#### 14Oct2019

###### Task 'Vererbung - IS-A'
- Done. 
- Code in: ub34_vererbungISA  

###### Task 'Interfaces - Geometrie.md'
- Done. 
- Code in: ub33_interfacesGeometrie  

###### Task 'Polymorphismus - Bruteforce.md'
- Done. 
- Code in: ub32_polymorphieBruteforce  


#### 11Oct2019

###### Task 'Polymorphismus - Bruteforce.md'
- Work in progress... 
- Code in: ub32_polymorphieBruteforce  


#### 10Oct2019

###### Task 'Polymorphie - Tiere.md'
- Done. 
- Code in: ub31_polymorphieTiere  

###### Small task 'KleineAufgabePolimorphie'
- Done. 
- Code in: ub30_kleineAufgabePolimorphie  


#### 09Oct2019

###### Task 'Vererbung - Dozent-Teilnehmer'
- Done. 
- Code in: ub29_polymorphieGeometrie 


#### 08Oct2019

###### Task 'Vererbung - Dozent-Teilnehmer'
- Done. 
- Code in: ub28_vererbungDozentTeilnehmer 

###### Task 'GC.md'
- Done. 
- Slides in: ub27_gc 

###### Task 'Vererbung - Geometrie'
- Done. 
- Code in: ub26_vererbungGeometrie 


#### 04Oct2019

###### Task 'Lotto'
- Done. 
- Code in: ub25_lotto 
- This code should be optimized in that 'LottoTipp' extends 'LottoSpiel' but I do not yet know how to do it...  


#### 02Oct2019

###### Task 'Lotto'
- Work in progress... 
- Code in: ub25_lotto 


#### 01Oct2019

###### Task 'Arrays - Kampfarena' (Kampfarena.md)
- Work in progress... 
- Code in: ub24_fightClub 

###### Small task 'Klassenobjekt'
- Done. 
- Code in: ub23_classObject 

###### Small task 'varargs'
- Done. 
- Code in: ub22_varargs 


#### 30Sept2019

###### Task 'IntMatrix.md'
- Done. 
- Code in: ub21_intMatrix 


#### 27Sept2019

###### Task 'Arrays - Hafen'
- Done. 
- Code in: ub20_arraysHafen 

###### Task 'Arrays - 2D'
- Done. 
- Code in: ub19_arrays2D 

###### Small task 'AufgabeTagesnamen'
- Done. 
- Code in: ub18_tagesnamen 

###### Small task 'MehrDimArrays'
- Done. 
- Code in: ub17_kleineAufgabeArrays 


#### 26Sept2019

###### Task 'Arrays - test equals.md'
- Done. 
- Code in: ub16_arraysEquals 

###### Task 'Array - einfach'
- Done. 
- Code in: ub15_arrays 


#### 25Sept2019

###### Task 'Klassen - Personen.md'
- Done. 
- Code in: ub14_klassenPersonen 


#### 24Sept2019

###### Task 'Klassen - Geometrie.md'
- Done.
- Code in: ub13_klassenGeometrie

###### Small task 'Aufgabe Auto'
- Done.
- Code in: ub12_aufgabeAuto


#### 23Sept2019

###### Task 'KleineAufgabenKlassen.java'
- Done.
- Code in: ub11_kleineAufgabeKlassen 

###### Small task 'move planet'  
- Done.
- Code in: ub10_movePlanet 


#### 20Sept2019

###### Task 'Methoden - Zeitspanne.md'  
- Done.  
- Code in: ub09_methodenZeitspanne  

###### Task 'Methoden - Fakultät.md'  
- Done.  
- Code in: ub08_methodenFakultaet  


#### 19Sept2019

###### Tasks 1-6 in 'Methoden.md'  
- Done.  
- Code in: ub07_methoden  

###### Task 'KleineAufgabeZurPause'  
- Done.  
- Code in: ub06_KleineAufgabeZurPause  


#### 18Sept2019

###### Task 'switch/if - Ampelfarben'  
- Done.  
- Code in: ub03_switchIfAmpelfarben  
	   
###### Task 'switch' (kleinen englischen Buchstaben)  
- Done.  
- Code in: ub04_switchKleinbuchstaben 

###### Task 'Kontrollstrukturen - while-do-while'   
- Done.  
- Code in: ub05_whileDowhile 


#### 17Sept2019

###### Task 'Kontrollstrukturen - for'
- Done. 
- Code in: ub01\_Kontrollstrukturen\_for

