## Some random programming projects


###### Source:

[https://bitbucket.org/UweBenary/2019_oca/src/master/My_Projects/src/](https://bitbucket.org/UweBenary/2019_oca/src/master/My_Projects/src/ "Hyperlink to my projects @ bitbucket.org")

#### 17Dec2019

###### Project 'BattleShip'
- Work in progress...
- Code in: My\_Projects/ub\_Project/src/LotkaVolterra

#### 30Oct2019

###### Project 'LotkaVolterra'
- First running version.
- Comment: random walk leads to cluster of animals
- Code in: My\_Projects/src/LotkaVolterra


#### 24Oct2019

###### Project 'Mines'
- First running version.
- Code in: My\_Projects/src/mines


#### 21Oct2019

###### Project 'Nu Pogodi'
- First running version.
- Code in: My\_Projects/src/nuPogodi


#### 27Sept2019

###### Project 'Cow On Meadow'
- First running version.
- Addressed issue: cow instance depends on meadow instance's dimensions (solution unsatisfying)
- Open issues: how to interrupt while-loop by user
- Code in: My\_Projects/src/cowOnMeadow


#### 26Sept2019

###### Project 'Cow On Meadow'
- Work in progress...
- Code in: My\_Projects/src/cowOnMeadow


#### 23Sept2019

###### Project 'pong'
- Work in progress...
- Comment: paddles missing
- Addressed issue: reflection of ball at board boundaries
- Open issues: ball instance depends on board instance's dimension (new project Cow on Meadow)
- Future problems: how to handle user inputs
- Code in: My\_Projects/src/pong


